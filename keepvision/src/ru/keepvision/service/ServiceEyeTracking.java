package com.keepvisionapp.child.service;

import ru.keepvision.measurement.MeasurementDistance;
import ru.keepvision.measurement.MeasurementDistance.IMeasurement;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.Utils;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class ServiceEyeTracking extends Service implements IMeasurement {

	private MeasurementDistance measurement;
	private PendingIntent pendingIntent;
	private volatile boolean isStopTracking;
	
	@Override
	public void onCreate() {
		super.onCreate();
		measurement = new MeasurementDistance(this, this);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//ensure existing one service
		if (startId != 1) { 
			while (startId != 1) {
				--startId;
				stopSelf(startId);
			}
			stopSelf(1);
		}
		
		isStopTracking = intent.getBooleanExtra(Constants.EYE_TRACKING_IS_STOP, true);
		pendingIntent = intent.getParcelableExtra(Constants.EYE_TRACKING_PARAM_PINTENT);
		
		if (isStopTracking) {
			measurement.stopMeasurement();
			stopSelf();
		} 
		else {
			measurement.startMeasurement();
		}
		return START_NOT_STICKY;
	}
	
	public void onDestroy() {
		super.onDestroy();
		measurement.stopMeasurement();
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void getDistance(float distance) {
		Intent iResult = new Intent();
		iResult.putExtra(Constants.EYE_TRACKING_RESULT_DISTANCE, distance);
		try {
			pendingIntent.send(this, Constants.ON_ACTIVITY_RESULT_EYE_TRACKING, iResult);
		} catch (CanceledException e1) {
			Utils.log("������ ��� �������� ���������� ������� RUNNING" + e1.getMessage());
		}
	}
}