package com.keepvisionapp.child.tools;

public class Constants {
	public static final int COUNT_FRAMES_THREASHOLD = 10;
	public static final String EYE_TRACKING_IS_STOP = "IS_STOP_EYE_TRACKING";
	public static final String EYE_TRACKING_PARAM_PINTENT = "PINTENT_FOR_EYE_TRACKING";
	public static final String EYE_TRACKING_RESULT_DISTANCE = "RESULT_OF_EYE_TRACKING";
	public static final int ON_ACTIVITY_RESULT_EYE_TRACKING = 1;
	
	public enum StateEyeTracking {
		STOP_TRACKING, START_TRACKING;
	}
}
