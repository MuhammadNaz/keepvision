package ru.keepvision;

import com.keepvisionapp.child.service.ServiceEyeTracking;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.Utils;
import com.keepvisionapp.child.tools.Constants.StateEyeTracking;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

	private Intent serviceEyeTracking;
	private Button btnStartService;
	private TextView tvDistance;
	private PendingIntent pendingIntent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	//	int x = 9/0;
		btnStartService = (Button) findViewById(R.id.btnStartService);
		tvDistance = (TextView) findViewById(R.id.tvDistance);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onClkBtnControlService(View v) {
		Utils.log("ServiceEyeTracking = " + Utils.isServiceRunning(this, ServiceEyeTracking.class));
		if (Utils.isServiceRunning(this, ServiceEyeTracking.class)) 
			controlServiceEyeTrack(StateEyeTracking.STOP_TRACKING);
		else 
			controlServiceEyeTrack(StateEyeTracking.START_TRACKING);
	}
	
	public void controlServiceEyeTrack(StateEyeTracking stateEyeTrack) {
		pendingIntent = createPendingResult(Constants.ON_ACTIVITY_RESULT_EYE_TRACKING, new Intent(), 0);
		if(stateEyeTrack == StateEyeTracking.STOP_TRACKING) {
			serviceEyeTracking = new Intent(this, ServiceEyeTracking.class).putExtra(Constants.EYE_TRACKING_IS_STOP, true).
					putExtra(Constants.EYE_TRACKING_PARAM_PINTENT, pendingIntent);
			btnStartService.setText("Start Service");
		}
			
		else {
			serviceEyeTracking = new Intent(this, ServiceEyeTracking.class).putExtra(Constants.EYE_TRACKING_IS_STOP, false).
					putExtra(Constants.EYE_TRACKING_PARAM_PINTENT, pendingIntent);
			btnStartService.setText("Stop Service");
		}
		startService(serviceEyeTracking);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Constants.ON_ACTIVITY_RESULT_EYE_TRACKING) {
			tvDistance.setText(String.valueOf(data.getFloatExtra(Constants.EYE_TRACKING_RESULT_DISTANCE, 0)));
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		controlServiceEyeTrack(StateEyeTracking.STOP_TRACKING);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
