package com.keepvisionapp.child.eye_detection;

import com.keepvisionapp.child.R;
import com.keepvisionapp.child.tools.Utils;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

public class HideEyeDetection implements PreviewCallback, FaceDetectionListener, SurfaceHolder.Callback {

	private final SurfaceHolder mHolder;
	private Camera mCamera;
	private final Context context;
	
	private WindowManager wm;
	private LinearLayout sv_layout;
	private Size previewSize;
	private IEyeDetection iEyeDetection;
	private WindowManager.LayoutParams paramsOfWindowView;
	private int rotate;
	
	public interface IEyeDetection {
		public void getDataImage(byte[] data, Size previewSize, int rotate);
	}
	
	@SuppressLint("InflateParams")
	public HideEyeDetection(final Context context, IEyeDetection iEyeDetection) {
		this.context = context;
		this.iEyeDetection = iEyeDetection;
		paramsOfWindowView = new WindowManager.LayoutParams(
				WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
				WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSPARENT);
		wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		LayoutInflater inflater = (LayoutInflater) context.getApplicationContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		View view = inflater.inflate(R.layout.sv_layout, null);
		sv_layout = (LinearLayout) view.findViewById(R.id.sv_layout);
		SurfaceView sv = (SurfaceView) view.findViewById(R.id.surf_view);
		mHolder = sv.getHolder();
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Utils.log("in surfaceDestroyed");
		if (mCamera == null)
			return;
		mCamera.stopPreview();
		mCamera.release();
		mCamera = null;
		Log.d("TrackingFlow", "Surface destroyed");
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Utils.log("in surfaceCreated");
		try {
			Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
			int cameraId = 0;
			int camerasCount = Camera.getNumberOfCameras();
			for (int camIndex = 0; camIndex < camerasCount; camIndex++) {
				Camera.getCameraInfo(camIndex, cameraInfo);
				if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
					cameraId = camIndex;
					break;
				}
			}
			if (cameraId == 0) 
				throw new Exception();
			mCamera = Camera.open(cameraId);
			mCamera.setPreviewDisplay(holder);
			Parameters parameters = mCamera.getParameters();
			previewSize = parameters.getPreviewSize();
			mCamera.setParameters(parameters);
			mCamera.startPreview();
			mCamera.setFaceDetectionListener(HideEyeDetection.this);
			mCamera.startFaceDetection();
		} catch (NoFrontCamera e) {
			Toast.makeText(context, "You have no front camera", Toast.LENGTH_SHORT).show();
			if (mCamera == null)
				return;
			mCamera.release();
			mCamera = null;
		}
			catch (Exception e) {
			Utils.log("Exception in surfaceCreated " + e.getMessage());
			if (mCamera == null)
				return;
			mCamera.release();
			mCamera = null;
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		Utils.log("surfaceChanged called");
	}
	
	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		int rotation = wm.getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0: rotate= 90; break;
            case Surface.ROTATION_90: rotate = 0; break;
            case Surface.ROTATION_180: rotate = 270; break;
            case Surface.ROTATION_270: rotate = 180; break;
        }
		iEyeDetection.getDataImage(data, previewSize, rotate);
		restartFaceDetection(); //need restart facedetection after set previewcallback
	}

	@Override
	public void onFaceDetection(Face[] faces, Camera camera) {
		//autostop facedetectioncamera 
		shotOnPreviewFrame();
	}
	
	public void restartFaceDetection() {
		Utils.log("in restartFaceDetection");
		try {
			mCamera.stopFaceDetection();
		} catch(Exception e) {
			Utils.log("error in stopFaceDetection");
		} 
		mCamera.setPreviewCallback(null);
		try {
			mCamera.startFaceDetection();
		} catch (Exception e) {
			Utils.log("error in stopFaceDetection");
		}
	}
	
	public void shotOnPreviewFrame() {
		Utils.log("in shotOnPreviewFrame");
	    mCamera.setOneShotPreviewCallback(this);
	}
	
	public void startFaceDetection() {
		Utils.log("in startFaceDetection");
		wm.addView(sv_layout, paramsOfWindowView);
		mHolder.addCallback(this);
	}
	
	public void stopFaceDetection() {
		Utils.log("in stopFaceDetection");
		mHolder.removeCallback(this);
		if (mCamera == null)
			return;
		mCamera.stopPreview();
		mCamera.release();
		mCamera = null;
	}
	
	private static class NoFrontCamera extends Exception {

		/**
		 * 
		 */
		private static final long serialVersionUID = -3438398978770552431L;
		
	}
}
