package ru.keepvision.measurement;

import java.util.ArrayList;
import java.util.List;

public class MeasureData {
	
	private List<Distance> distances = new ArrayList<Distance>(); //all distances from traking for finding averageDistance
	private float averageDistance; //distance is operated of system 

	public float getAverageDistance() {
		if (distances.size() == 0)
			return -1; //wrong during take averageEyeDistance; 
		float sum = 0;
		for (Distance x : distances) 
			sum += x.getDeviceDistance();
		averageDistance = sum / distances.size();
		return averageDistance;
	}
	
	public void addDistance(float eyeDistance) {
		float deviceDistance = 18000/eyeDistance; //18000 it's ratio
		Distance distance = new Distance(deviceDistance);
		distances.add(distance);
	}

	public void setAverageDistance(float averageEyeDistance) {
		this.averageDistance = averageEyeDistance;
	}
	
	public int size() {
		return distances.size();
	}
	
	public void removeFirst() {
		if (distances.size() > 0)
			distances.remove(0);
	}

	public static class Distance {
		
		private final float deviceDistance;

		public Distance(final float deviceDistance) {
			this.deviceDistance = deviceDistance;
		}
		
		public float getDeviceDistance() {
			return deviceDistance;
		}
	}
}
