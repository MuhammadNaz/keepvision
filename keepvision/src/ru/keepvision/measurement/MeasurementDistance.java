package ru.keepvision.measurement;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

import com.keepvisionapp.child.R;
import com.keepvisionapp.child.eye_detection.HideEyeDetection;
import com.keepvisionapp.child.eye_detection.HideEyeDetection.IEyeDetection;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.Utils;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera.Size;

public class MeasurementDistance implements IEyeDetection {

	public interface IMeasurement {
		public void getDistance(float distance);
	}
	
	private MeasureData measureData;
	private HideEyeDetection eyeDetection;
	private IMeasurement iMeasurment;
	private float currentAvgDistance = -1;
	private File mCascadeFile;
	private DetectionBasedTracker mNativeDetector;
	private Context context;
	private CascadeClassifier mClassifierDetectorEye;
	
	public MeasurementDistance(final Context context, final IMeasurement iMeasurement) {
		measureData = new MeasureData();
		this.iMeasurment = iMeasurement;
		this.context = context;
		eyeDetection = new HideEyeDetection(context, this);
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, context, mLoaderCallback);
	}
	
	private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(context) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Utils.log("OpenCV loaded successfully");

                    // Load native library after(!) OpenCV initialization
                    System.loadLibrary("detection_based_tracker");

                    try {
                        // load cascade file from application resources
                        InputStream is = context.getResources().openRawResource(R.raw.lbpcascade_frontaleye);
                        File cascadeDir = context.getDir("cascade", Context.MODE_PRIVATE);
                        mCascadeFile = new File(cascadeDir, "lbpcascade_frontaleye.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);
                        
                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();
                        mClassifierDetectorEye = new CascadeClassifier(mCascadeFile.getAbsolutePath());
                        mNativeDetector = new DetectionBasedTracker(mCascadeFile.getAbsolutePath(), 0);
                        cascadeDir.delete();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Utils.log("Failed to load cascade. Exception thrown: " + e);
                    }
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
	
	
	public void startMeasurement() {
		eyeDetection.startFaceDetection();
	}
	
	public void stopMeasurement() {
		eyeDetection.stopFaceDetection();
	}

	/*@Override
	public void getDataImage(byte[] data, Size previewSize, int rotate) {
		Bitmap currentFrame;
		long t = System.currentTimeMillis();
		YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21,
				previewSize.width, previewSize.height, null);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if (!yuvimage.compressToJpeg(new Rect(0, 0, previewSize.width, previewSize.height), 100, baos)) {
			Utils.log("compressToJpeg failed");
		}

		Utils.log("Compression finished: " + (System.currentTimeMillis() - t));
		t = System.currentTimeMillis();

		BitmapFactory.Options bfo = new BitmapFactory.Options();
		bfo.inPreferredConfig = Bitmap.Config.RGB_565;
		currentFrame = BitmapFactory.decodeStream(new ByteArrayInputStream(
				baos.toByteArray()), null, bfo);

		Utils.log("Decode Finished: " + (System.currentTimeMillis() - t));
		t = System.currentTimeMillis();
		
		currentFrame = rotatedBitmap(currentFrame, rotate);
		
		Utils.log("Rotate, Create finished: " + (System.currentTimeMillis() - t));
		t = System.currentTimeMillis();

		if (currentFrame == null) {
			Utils.log("Could not decode Image");
			return;
		}
		
		FaceDetector d = new FaceDetector(currentFrame.getWidth(),
				currentFrame.getHeight(), 1);
		Face[] faces = new Face[1];
		d.findFaces(currentFrame, faces);

		Utils.log("FaceDetection finished: " + (System.currentTimeMillis() - t));
		t = System.currentTimeMillis();
		
		Face currentFace = faces[0];
		
		if (currentFace != null) {
			measureData.addDistance(currentFace.eyesDistance());
			while (measureData.size() > Constants.COUNT_FRAMES_THREASHOLD)
				measureData.removeFirst();
			currentAvgDistance = measureData.getAverageDistance();
			if (currentAvgDistance != -1)
				iMeasurment.getDistance(currentAvgDistance);
		} 
		else 
			Utils.log("currentFace = null");
	}*/
	
	private static Bitmap rotatedBitmap(Bitmap source, float angle) {
	      Matrix matrix = new Matrix();
	      matrix.postRotate(angle);
	      return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, false);
	}
	
	public static Mat convertBitmapToMat(Bitmap bmp) {
		Mat imageMat = new Mat (bmp.getHeight(), bmp.getWidth(), CvType.CV_8UC1); //������ ���� gray
		Bitmap myBitmap32 = bmp.copy(Bitmap.Config.ARGB_8888, true);
		org.opencv.android.Utils.bitmapToMat(myBitmap32, imageMat);
		
//		org.opencv.android.Utils.bitmapToMat(bmp, imageMat);
		//Do smth.
		Imgproc.cvtColor(imageMat, imageMat, Imgproc.COLOR_RGB2GRAY);
		
		return imageMat;
	}

/*	@Override
	public void getDataImage(byte[] data, Size previewSize, int rotate) {
		Bitmap currentFrame;
		long t = System.currentTimeMillis();
		YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21,
				previewSize.width, previewSize.height, null);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if (!yuvimage.compressToJpeg(new Rect(0, 0, previewSize.width, previewSize.height), 100, baos)) {
			Utils.log("compressToJpeg failed");
		}

		Utils.log("Compression finished: " + (System.currentTimeMillis() - t));
		t = System.currentTimeMillis();

		BitmapFactory.Options bfo = new BitmapFactory.Options();
		bfo.inPreferredConfig = Bitmap.Config.RGB_565;
	//	bfo.inPreferredConfig = Bitmap.Config.ARGB_8888;
		currentFrame = BitmapFactory.decodeStream(new ByteArrayInputStream(
				baos.toByteArray()), null, bfo);

		Utils.log("Decode Finished: " + (System.currentTimeMillis() - t));
		t = System.currentTimeMillis();
		
		currentFrame = rotatedBitmap(currentFrame, rotate);
		
		Utils.log("Rotate, Create finished: " + (System.currentTimeMillis() - t));
		t = System.currentTimeMillis();
		double eyeDistance = getEyeDistance(currentFrame);
		if (eyeDistance != -1) {
			measureData.addDistance((float) eyeDistance);
			while (measureData.size() > Constants.COUNT_FRAMES_THREASHOLD)
				measureData.removeFirst();
			currentAvgDistance = measureData.getAverageDistance();
			if (currentAvgDistance != -1)
				iMeasurment.getDistance(currentAvgDistance);
		} 
		else 
			Utils.log("eyeDistance = null");
	}*/
	
	
	
	private double getEyeDistance(Bitmap currentFrame) {
		Mat currentFrameInMat = convertBitmapToMat(currentFrame);
		MatOfRect faces = new MatOfRect();
		if (mNativeDetector != null)
            mNativeDetector.detect(currentFrameInMat, faces);
		else
			return -1;
		if (faces.toArray().length == 0) {
			Utils.log("faces didn't find");
			return -1;
		}
		org.opencv.core.Rect face = faces.toArray()[0];
		org.opencv.core.Rect eyearea_right;
		org.opencv.core.Rect eyearea_left;
		if (face != null) {
			eyearea_right = new org.opencv.core.Rect(face.x + face.width / 16,
					(int) (face.y + (face.height / 4.5)),
					(face.width - 2 * face.width / 16) / 2, (int) (face.height / 3.0));
			eyearea_left = new org.opencv.core.Rect(face.x + face.width / 16
					+ (face.width - 2 * face.width / 16) / 2,
					(int) (face.y + (face.height / 4.5)),
					(face.width - 2 * face.width / 16) / 2, (int) (face.height / 3.0));
			Point leftEye = getIrisPoint(eyearea_left, currentFrameInMat);
			Point rightEye = getIrisPoint(eyearea_right, currentFrameInMat);
			if (leftEye == null || rightEye == null)
				return -1;
			return Math.sqrt(Math.pow(rightEye.x - leftEye.x, 2) - Math.pow(rightEye.y - leftEye.y, 2));
		}
		else 
			return -1;
	}
	private Point getIrisPoint(org.opencv.core.Rect area, Mat mGray) {
		Mat mROI = mGray.submat(area);
		MatOfRect eye = new MatOfRect();
		Point iris = new Point();
		mClassifierDetectorEye.detectMultiScale(mROI, eye, 1.15, 2,
				Objdetect.CASCADE_FIND_BIGGEST_OBJECT
						| Objdetect.CASCADE_SCALE_IMAGE, new org.opencv.core.Size(30, 30),
				new org.opencv.core.Size());
		if (eye.toArray().length == 0)
			return null;
		org.opencv.core.Rect eye = eye.toArray()[0];
		if (eye != null) {
			eye.x = area.x + eye.x;
			eye.y = area.y + eye.y;
			org.opencv.core.Rect eye_only_rectangle = new org.opencv.core.Rect((int) eye.tl().x,
					(int) (eye.tl().y + eye.height * 0.4), (int) eye.width,
					(int) (eye.height * 0.6));
			mROI = mGray.submat(eye_only_rectangle);
			Core.MinMaxLocResult mmG = Core.minMaxLoc(mROI);
			iris.x = mmG.minLoc.x + eye_only_rectangle.x;
			iris.y = mmG.minLoc.y + eye_only_rectangle.y;
			return iris;
		}
		return null;
	}

	@Override
	public void getDataImage(byte[] data, Size previewSize, int rotate) {
		Mat mat = new Mat(previewSize.height, previewSize.width, CvType.CV_8UC1);
		mat.put(0, 0, data);
		
	}
}
