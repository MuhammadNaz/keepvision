/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.keepvisionapp.child;

public final class R {
    public static final class attr {
        /** <p>May be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
<p>May be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>any</code></td><td>-1</td><td></td></tr>
<tr><td><code>back</code></td><td>99</td><td></td></tr>
<tr><td><code>front</code></td><td>98</td><td></td></tr>
</table>
         */
        public static final int camera_id=0x7f010001;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int circleCrop=0x7f010004;
        /** <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int imageAspectRatio=0x7f010003;
        /** <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>none</code></td><td>0</td><td></td></tr>
<tr><td><code>adjust_width</code></td><td>1</td><td></td></tr>
<tr><td><code>adjust_height</code></td><td>2</td><td></td></tr>
</table>
         */
        public static final int imageAspectRatioAdjust=0x7f010002;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int show_fps=0x7f010000;
    }
    public static final class bool {
        public static final int isTablet=0x7f060000;
    }
    public static final class color {
        public static final int blue=0x7f070000;
        public static final int common_action_bar_splitter=0x7f070001;
        public static final int common_signin_btn_dark_text_default=0x7f070002;
        public static final int common_signin_btn_dark_text_disabled=0x7f070003;
        public static final int common_signin_btn_dark_text_focused=0x7f070004;
        public static final int common_signin_btn_dark_text_pressed=0x7f070005;
        public static final int common_signin_btn_default_background=0x7f070006;
        public static final int common_signin_btn_light_text_default=0x7f070007;
        public static final int common_signin_btn_light_text_disabled=0x7f070008;
        public static final int common_signin_btn_light_text_focused=0x7f070009;
        public static final int common_signin_btn_light_text_pressed=0x7f07000a;
        public static final int common_signin_btn_text_dark=0x7f070010;
        public static final int common_signin_btn_text_light=0x7f070011;
        public static final int dark_blue=0x7f07000b;
        public static final int oragne=0x7f07000c;
        public static final int pink=0x7f07000d;
        public static final int red=0x7f07000e;
        public static final int white=0x7f07000f;
    }
    public static final class dimen {
        public static final int activity_horizontal_margin=0x7f080000;
        public static final int activity_vertical_margin=0x7f080001;
    }
    public static final class drawable {
        public static final int btn_game=0x7f020000;
        public static final int child_list=0x7f020001;
        public static final int common_full_open_on_phone=0x7f020002;
        public static final int common_ic_googleplayservices=0x7f020003;
        public static final int common_signin_btn_icon_dark=0x7f020004;
        public static final int common_signin_btn_icon_disabled_dark=0x7f020005;
        public static final int common_signin_btn_icon_disabled_focus_dark=0x7f020006;
        public static final int common_signin_btn_icon_disabled_focus_light=0x7f020007;
        public static final int common_signin_btn_icon_disabled_light=0x7f020008;
        public static final int common_signin_btn_icon_focus_dark=0x7f020009;
        public static final int common_signin_btn_icon_focus_light=0x7f02000a;
        public static final int common_signin_btn_icon_light=0x7f02000b;
        public static final int common_signin_btn_icon_normal_dark=0x7f02000c;
        public static final int common_signin_btn_icon_normal_light=0x7f02000d;
        public static final int common_signin_btn_icon_pressed_dark=0x7f02000e;
        public static final int common_signin_btn_icon_pressed_light=0x7f02000f;
        public static final int common_signin_btn_text_dark=0x7f020010;
        public static final int common_signin_btn_text_disabled_dark=0x7f020011;
        public static final int common_signin_btn_text_disabled_focus_dark=0x7f020012;
        public static final int common_signin_btn_text_disabled_focus_light=0x7f020013;
        public static final int common_signin_btn_text_disabled_light=0x7f020014;
        public static final int common_signin_btn_text_focus_dark=0x7f020015;
        public static final int common_signin_btn_text_focus_light=0x7f020016;
        public static final int common_signin_btn_text_light=0x7f020017;
        public static final int common_signin_btn_text_normal_dark=0x7f020018;
        public static final int common_signin_btn_text_normal_light=0x7f020019;
        public static final int common_signin_btn_text_pressed_dark=0x7f02001a;
        public static final int common_signin_btn_text_pressed_light=0x7f02001b;
        public static final int ic_launcher=0x7f02001c;
        public static final int ic_plusone_medium_off_client=0x7f02001d;
        public static final int ic_plusone_small_off_client=0x7f02001e;
        public static final int ic_plusone_standard_off_client=0x7f02001f;
        public static final int ic_plusone_tall_off_client=0x7f020020;
        public static final int ok=0x7f020021;
        public static final int shape_btn_game_pressed=0x7f020022;
        public static final int shape_btn_game_unpressed=0x7f020023;
        public static final int start=0x7f020024;
        public static final int vse_khorosho=0x7f020025;
        public static final int vse_ochen_plokho=0x7f020026;
        public static final int window_for_ok=0x7f020027;
    }
    public static final class id {
        public static final int adjust_height=0x7f0d0003;
        public static final int adjust_width=0x7f0d0004;
        public static final int any=0x7f0d0000;
        public static final int back=0x7f0d0001;
        public static final int btnGetDistance=0x7f0d0022;
        public static final int btnLogin=0x7f0d0007;
        public static final int btnRepeatGetChildren=0x7f0d0013;
        public static final int btnSetBottom=0x7f0d0021;
        public static final int btnToChildrenList=0x7f0d0016;
        public static final int button=0x7f0d0017;
        public static final int etMail=0x7f0d000b;
        public static final int etPassword=0x7f0d000a;
        public static final int front=0x7f0d0002;
        public static final int ibItemChildOk=0x7f0d001d;
        public static final int imageView=0x7f0d001e;
        public static final int imageView1=0x7f0d000d;
        public static final int lvChildren=0x7f0d0012;
        public static final int menu_log_out=0x7f0d0025;
        public static final int none=0x7f0d0005;
        public static final int paint_layout=0x7f0d001f;
        public static final int paint_surface=0x7f0d0020;
        public static final int parent_of_sv_camera=0x7f0d0018;
        public static final int progressBar=0x7f0d001b;
        public static final int surf_view=0x7f0d0024;
        public static final int sv_camera=0x7f0d0019;
        public static final int sv_layout=0x7f0d0023;
        public static final int textView=0x7f0d001a;
        public static final int textView2=0x7f0d000e;
        public static final int textView3=0x7f0d0011;
        public static final int textView5=0x7f0d0009;
        public static final int toGame=0x7f0d0015;
        public static final int tvDOZ=0x7f0d000f;
        public static final int tvDec=0x7f0d0014;
        public static final int tvForgot=0x7f0d0008;
        public static final int tvFunt=0x7f0d0010;
        public static final int tvItemChildName=0x7f0d001c;
        public static final int tvPer=0x7f0d000c;
        public static final int tvRegister=0x7f0d0006;
    }
    public static final class integer {
        public static final int google_play_services_version=0x7f090000;
    }
    public static final class layout {
        public static final int acitvity_login=0x7f030000;
        public static final int activity_bad=0x7f030001;
        public static final int activity_children_list=0x7f030002;
        public static final int activity_good=0x7f030003;
        public static final int activity_main=0x7f030004;
        public static final int camera_evaluate=0x7f030005;
        public static final int dialog_wait=0x7f030006;
        public static final int item_list_chlidren=0x7f030007;
        public static final int paint_layout=0x7f030008;
        public static final int sv_layout=0x7f030009;
    }
    public static final class menu {
        public static final int main=0x7f0c0000;
    }
    public static final class raw {
        public static final int bl_video=0x7f050000;
        public static final int cs0_eyes=0x7f050001;
        public static final int cs0_eyesnosemouth=0x7f050002;
        public static final int cs0_eyeswithnose=0x7f050003;
        public static final int cs0_mouth=0x7f050004;
        public static final int cs0_nose=0x7f050005;
        public static final int dfl_video=0x7f050006;
        public static final int eye_left=0x7f050007;
        public static final int eye_right=0x7f050008;
        public static final int eyes=0x7f050009;
        public static final int eyes_weak=0x7f05000a;
        public static final int face=0x7f05000b;
        public static final int gtm_analytics=0x7f05000c;
        public static final int haarcascade_eye=0x7f05000d;
        public static final int lbpcascade_frontaleye=0x7f05000e;
        public static final int letters_ascii=0x7f05000f;
        public static final int msl_video=0x7f050010;
        public static final int sfl_video=0x7f050011;
        public static final int tl_video=0x7f050012;
        public static final int vdlphotoapp=0x7f050013;
        public static final int vdlphotodb=0x7f050014;
        public static final int viewdle=0x7f050015;
        public static final int viewdle_default=0x7f050016;
        public static final int viewdle_photo=0x7f050017;
        public static final int viewdle_video=0x7f050018;
    }
    public static final class string {
        public static final int app_name=0x7f0a0000;
        public static final int close_button_text=0x7f0a0001;
        public static final int common_android_wear_notification_needs_update_text=0x7f0a0002;
        public static final int common_android_wear_update_text=0x7f0a0003;
        public static final int common_android_wear_update_title=0x7f0a0004;
        public static final int common_google_play_services_enable_button=0x7f0a0005;
        public static final int common_google_play_services_enable_text=0x7f0a0006;
        public static final int common_google_play_services_enable_title=0x7f0a0007;
        public static final int common_google_play_services_error_notification_requested_by_msg=0x7f0a0008;
        public static final int common_google_play_services_install_button=0x7f0a0009;
        public static final int common_google_play_services_install_text_phone=0x7f0a000a;
        public static final int common_google_play_services_install_text_tablet=0x7f0a000b;
        public static final int common_google_play_services_install_title=0x7f0a000c;
        public static final int common_google_play_services_invalid_account_text=0x7f0a000d;
        public static final int common_google_play_services_invalid_account_title=0x7f0a000e;
        public static final int common_google_play_services_needs_enabling_title=0x7f0a000f;
        public static final int common_google_play_services_network_error_text=0x7f0a0010;
        public static final int common_google_play_services_network_error_title=0x7f0a0011;
        public static final int common_google_play_services_notification_needs_installation_title=0x7f0a0012;
        public static final int common_google_play_services_notification_needs_update_title=0x7f0a0013;
        public static final int common_google_play_services_notification_ticker=0x7f0a0014;
        public static final int common_google_play_services_sign_in_failed_text=0x7f0a0015;
        public static final int common_google_play_services_sign_in_failed_title=0x7f0a0016;
        public static final int common_google_play_services_unknown_issue=0x7f0a0017;
        public static final int common_google_play_services_unsupported_text=0x7f0a0018;
        public static final int common_google_play_services_unsupported_title=0x7f0a0019;
        public static final int common_google_play_services_update_button=0x7f0a001a;
        public static final int common_google_play_services_update_text=0x7f0a001b;
        public static final int common_google_play_services_update_title=0x7f0a001c;
        public static final int common_open_on_phone=0x7f0a001d;
        public static final int common_signin_button_text=0x7f0a001e;
        public static final int common_signin_button_text_long=0x7f0a001f;
        public static final int commono_google_play_services_api_unavailable_text=0x7f0a0020;
        public static final int hello_world=0x7f0a0021;
        public static final int log_out=0x7f0a0022;
        public static final int no_sd_card_message=0x7f0a0023;
    }
    public static final class style {
        /** 
            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can runningGame here.
        
 API 11 theme customizations can runningGame here. 
 API 14 theme customizations can runningGame here. 
         */
        public static final int AppBaseTheme=0x7f0b0000;
        /**  All customizations that are NOT specific to a particular API-level can runningGame here. 
         */
        public static final int AppTheme=0x7f0b0001;
    }
    public static final class xml {
        public static final int global_tracker=0x7f040000;
    }
    public static final class styleable {
        /** Attributes that can be used with a CameraBridgeViewBase.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #CameraBridgeViewBase_camera_id com.keepvisionapp.child:camera_id}</code></td><td></td></tr>
           <tr><td><code>{@link #CameraBridgeViewBase_show_fps com.keepvisionapp.child:show_fps}</code></td><td></td></tr>
           </table>
           @see #CameraBridgeViewBase_camera_id
           @see #CameraBridgeViewBase_show_fps
         */
        public static final int[] CameraBridgeViewBase = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link com.keepvisionapp.child.R.attr#camera_id}
          attribute's value can be found in the {@link #CameraBridgeViewBase} array.


          <p>May be an integer value, such as "<code>100</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
<p>May be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>any</code></td><td>-1</td><td></td></tr>
<tr><td><code>back</code></td><td>99</td><td></td></tr>
<tr><td><code>front</code></td><td>98</td><td></td></tr>
</table>
          @attr name com.keepvisionapp.child:camera_id
        */
        public static final int CameraBridgeViewBase_camera_id = 1;
        /**
          <p>This symbol is the offset where the {@link com.keepvisionapp.child.R.attr#show_fps}
          attribute's value can be found in the {@link #CameraBridgeViewBase} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.keepvisionapp.child:show_fps
        */
        public static final int CameraBridgeViewBase_show_fps = 0;
        /** Attributes that can be used with a LoadingImageView.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #LoadingImageView_circleCrop com.keepvisionapp.child:circleCrop}</code></td><td></td></tr>
           <tr><td><code>{@link #LoadingImageView_imageAspectRatio com.keepvisionapp.child:imageAspectRatio}</code></td><td></td></tr>
           <tr><td><code>{@link #LoadingImageView_imageAspectRatioAdjust com.keepvisionapp.child:imageAspectRatioAdjust}</code></td><td></td></tr>
           </table>
           @see #LoadingImageView_circleCrop
           @see #LoadingImageView_imageAspectRatio
           @see #LoadingImageView_imageAspectRatioAdjust
         */
        public static final int[] LoadingImageView = {
            0x7f010002, 0x7f010003, 0x7f010004
        };
        /**
          <p>This symbol is the offset where the {@link com.keepvisionapp.child.R.attr#circleCrop}
          attribute's value can be found in the {@link #LoadingImageView} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.keepvisionapp.child:circleCrop
        */
        public static final int LoadingImageView_circleCrop = 2;
        /**
          <p>This symbol is the offset where the {@link com.keepvisionapp.child.R.attr#imageAspectRatio}
          attribute's value can be found in the {@link #LoadingImageView} array.


          <p>Must be a floating point value, such as "<code>1.2</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.keepvisionapp.child:imageAspectRatio
        */
        public static final int LoadingImageView_imageAspectRatio = 1;
        /**
          <p>This symbol is the offset where the {@link com.keepvisionapp.child.R.attr#imageAspectRatioAdjust}
          attribute's value can be found in the {@link #LoadingImageView} array.


          <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>none</code></td><td>0</td><td></td></tr>
<tr><td><code>adjust_width</code></td><td>1</td><td></td></tr>
<tr><td><code>adjust_height</code></td><td>2</td><td></td></tr>
</table>
          @attr name com.keepvisionapp.child:imageAspectRatioAdjust
        */
        public static final int LoadingImageView_imageAspectRatioAdjust = 0;
    };
}
