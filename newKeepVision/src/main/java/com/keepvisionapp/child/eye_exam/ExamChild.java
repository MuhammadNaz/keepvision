package com.keepvisionapp.child.eye_exam;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;


import com.keepvision.eyeexamfar.eye_exam.MsgKV;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import com.keepvisionapp.child.KVApp;
import com.keepvisionapp.child.activities.BadEyes;
import com.keepvisionapp.child.activities.GoodEyes;
import com.keepvisionapp.child.game.framework.Game;
import com.keepvisionapp.child.game.framework.Graphics;
import com.keepvisionapp.child.game.framework.Input;
import com.keepvisionapp.child.game.framework.Screen;
import com.keepvisionapp.child.game.framework.impl.AnimationGL;
import com.keepvisionapp.child.game.framework.impl.GLDrawer;
import com.keepvisionapp.child.game.framework.impl.GLGame;
import com.keepvisionapp.child.game.framework.impl.GLGraphics;
import com.keepvisionapp.child.game.framework.impl.PixmapGL;
import com.keepvisionapp.child.game.logic.Optotype;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 09.05.2015.
 */
public class ExamChild extends GLGame {

    public final String TAG = "ExamChild";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (((KVApp) getApplication()).getBluetoothSocket() != null)
            if (!ConnectionThread.isConnected) {
                Toast.makeText(this, "Not connected", Toast.LENGTH_SHORT).show();
                finish();
                return;
            }
    }
    @Override
    public Screen getStartScreen() {
        return new MainScreen(this);
    }

    private class MainScreen extends Screen {
        GLGraphics glGraphics;
        PixmapGL pArrow, pPause, pHand;;
        AnimationGL animHand;
        int STATE_INIT = 0;
        int STATE_RUNNING = 1;
        int STATE_PAUSE = 2;
        int STATE_EXAM_OVER = 3;
        int state = STATE_INIT;
        ExamMechanic examMechanic;
        volatile boolean isSwiped = false;
        boolean isShowExplain = false;
        int stateExplain = -1;

        public MainScreen(Game game) {
            super(game);
            glGraphics = ((GLGame) game).getGLGraphics();
            Graphics g = game.getGraphics();
            pArrow = new PixmapGL(glGraphics, g, g.newPixmap("line_for_explain.png", Graphics.PixmapFormat.RGB565));
            pPause = new PixmapGL(glGraphics, g, g.newPixmap("pause.png", Graphics.PixmapFormat.RGB565));
            pPause.setPosition(glGraphics.getWidth() / 2, glGraphics.getHeight() / 2);

            pHand = new PixmapGL(glGraphics, g, g.newPixmap("hand.png", Graphics.PixmapFormat.RGB565));
            animHand = new AnimationGL(glGraphics);
            animHand.create(pHand, glGraphics.getWidth() / 2, glGraphics.getHeight() / 2);
            animHand.setRepeat(true);

            examMechanic = new ExamMechanic();
        }

        @Override
        public void update(float deltaTime) {
            List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
            if (state == STATE_RUNNING && !isSwiped) {

                for (int i = 0; i < touchEvents.size(); i++) {
                    if (touchEvents.get(i).type == Input.TouchEvent.TOUCH_SWIPE_DOWN
                            || touchEvents.get(i).type == Input.TouchEvent.TOUCH_SWIPE_LEFT
                            || touchEvents.get(i).type == Input.TouchEvent.TOUCH_SWIPE_RIGHT
                            || touchEvents.get(i).type == Input.TouchEvent.TOUCH_SWIPE_UP) {
                        try {
                            examMechanic.swiped(touchEvents.get(i).type);
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    isSwiped = true;
                                    try {
                                        Thread.sleep(200);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    isSwiped = false;
                                }
                            }).start();
                        } catch (Exception e) {
                        }
                    }
                }
            }

           // examMechanic.update();
        }

        @Override
        public void present(float deltaTime) {
            GL10 gl10 = glGraphics.getGL();
            gl10.glClear(GL10.GL_COLOR_BUFFER_BIT);
            if (state == STATE_RUNNING) {
                pArrow.setRotation(0, PixmapGL.ROTATE_LEFT);
                pArrow.setPosition(glGraphics.getWidth() / 2 + 80, glGraphics.getHeight() / 2);
                GLDrawer.draw(pArrow);
                pArrow.setPosition(glGraphics.getWidth() / 2 - 80, glGraphics.getHeight() / 2);
                pArrow.setRotation(180, PixmapGL.ROTATE_LEFT);
                GLDrawer.draw(pArrow);
                pArrow.setPosition(glGraphics.getWidth() / 2, glGraphics.getHeight() / 2 + 80);
                pArrow.setRotation(90, PixmapGL.ROTATE_LEFT);
                GLDrawer.draw(pArrow);
                pArrow.setPosition(glGraphics.getWidth() / 2, glGraphics.getHeight() / 2 - 80);
                pArrow.setRotation(90, PixmapGL.ROTATE_RIGHT);
                GLDrawer.draw(pArrow);
                if (isShowExplain) {
                    if (!animHand.isShow) {
                        animHand.create(pHand, glGraphics.getWidth() / 2, glGraphics.getHeight()/2);
                        switch (stateExplain) {
                            case Optotype.RIGHT:
                                animHand.setPosition(glGraphics.getWidth()/2 + pArrow.getWidth(), glGraphics.getHeight()/2);
                                break;
                            case Optotype.LEFT:
                                animHand.setPosition(glGraphics.getWidth()/2 - pArrow.getWidth(), glGraphics.getHeight()/2);
                                break;
                            case Optotype.UP:
                                animHand.setPosition(glGraphics.getWidth()/2 + 15, glGraphics.getHeight()/2 + pArrow.getWidth());
                                break;
                            case Optotype.DOWN:
                                animHand.setPosition(glGraphics.getWidth()/2 + 15, glGraphics.getHeight()/2 - pArrow.getWidth());
                                break;
                        }
                        animHand.init(0.8f);
                        animHand.show();
                    }
                    animHand.draw(deltaTime);
                }
            }
            if (state == STATE_PAUSE)
                GLDrawer.draw(pPause);


        }

        @Override
        public void pause() {

        }

        @Override
        public void resume() {
            GL10 gl = glGraphics.getGL();
            gl.glClearColor(1, 1, 1, 0);
            gl.glViewport(0, 0, glGraphics.getWidth(), glGraphics.getHeight());
            gl.glMatrixMode(GL10.GL_PROJECTION);
            gl.glLoadIdentity();
            gl.glOrthof(0, glGraphics.getWidth(), 0, glGraphics.getHeight(), 1, -1);
            gl.glEnable(GL10.GL_TEXTURE_2D);
            gl.glEnable(GL10.GL_BLEND);
            gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
            gl.glMatrixMode(GL10.GL_MODELVIEW);

            pArrow.reload();
            pPause.reload();
            pHand.reload();
        }

        @Override
        public void dispose() {
            Utils.log(TAG, "dispose");
            state = STATE_EXAM_OVER;
            examMechanic.dispose();
            pArrow.dispose();
            pPause.dispose();
            pHand.dispose();
        }

        class ExamMechanic implements ConnectionThread.IConnectionThread{

            ConnectionThread connectionThread = ((KVApp) getApplication()).getConnectionThread();

            public ExamMechanic() {
                connectionThread.setIConnectionThread(this);
                startExam();
            }

            public void update() {
                if (state == STATE_EXAM_OVER) {
                    return;
                }
            }

            @Override
            public void getData(MsgKV msgKV) {
                Utils.log(TAG, "getData type = " + msgKV.type);
                if (msgKV.type == MsgKV.TYPE_STATE_GAME) {
                    Integer stateGame = Integer.valueOf(msgKV.object);
                    Utils.log(TAG, "state = " + stateGame);
                    if (stateGame == STATE_EXAM_OVER) {
                        MainScreen.this.dispose();
                        return;
                    }
                    if (stateGame == STATE_PAUSE)
                        MainScreen.this.state = STATE_PAUSE;
                    if (stateGame == STATE_RUNNING) {
                        MainScreen.this.state = STATE_RUNNING;
                        connectionThread.write(new MsgKV()); //check
                    }
                }

                if (msgKV.type == MsgKV.TYPE_RESULT) {
                    if (Float.parseFloat(msgKV.object) != 100f) {
                        Intent toBad = new Intent((GLGame) game, BadEyes.class);
                        toBad.putExtra(Constants.EYE_TRACKING_RESULT_VALUE, Float.parseFloat(msgKV.object));
                        startActivity(toBad);
                    }
                    else {
                        startActivity(new Intent((GLGame) game, GoodEyes.class));
                    }
                }
                if (msgKV.type == MsgKV.TYPE_SHOW_EXPLAIN) {
                    stateExplain = Integer.parseInt(msgKV.object);
                    isShowExplain = true;
                }
            }

            @Override
            public void disconnected() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ExamChild.this, "Connection lost", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
            }

            private void success() {

            }

            private void wrong() {

            }

            public void dispose() {
                //connectionThread.close();
            }

            private void startExam() {
                Utils.log(TAG, "startExam");
                state = STATE_RUNNING;
            }

            public void swiped(int type) {
                Utils.log(TAG, "swiped type = " + type);
                MsgKV msgKV = new MsgKV(MsgKV.TYPE_SWIPE);
                switch (type) {
                    case Input.TouchEvent.TOUCH_SWIPE_LEFT:
                        msgKV.object = String.valueOf(Optotype.LEFT);
                        break;
                    case Input.TouchEvent.TOUCH_SWIPE_DOWN:
                        msgKV.object = String.valueOf(Optotype.DOWN);
                        break;
                    case Input.TouchEvent.TOUCH_SWIPE_RIGHT:
                        msgKV.object = String.valueOf(Optotype.RIGHT);
                        break;
                    case Input.TouchEvent.TOUCH_SWIPE_UP:
                        msgKV.object = String.valueOf(Optotype.UP);
                        break;
                }
                connectionThread.write(msgKV);

                if (animHand.isShow) {
                    isShowExplain = false;
                    animHand.stop();
                }
            }
        }
    }
}

