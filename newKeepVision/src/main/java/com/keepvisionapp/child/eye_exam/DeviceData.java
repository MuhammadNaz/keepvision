package com.keepvisionapp.child.eye_exam;

import android.os.ParcelUuid;

/**
 * Created by AbduLlah on 08.05.2015.
 */
public class DeviceData {

    public static final int ERROR_TYPE = -1;
    public String name;
    public String mac;
    public int type;
    public int bondState;
    public ParcelUuid[] uuids;
}
