package com.keepvisionapp.child.eye_exam;

import android.app.Activity;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.keepvisionapp.child.KVApp;
import com.keepvisionapp.child.R;
import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 08.05.2015.
 */
public class BluetoothActivity extends Activity implements BluetoothController.IBluetoothController{

    private final static String TAG = "BluetoothActivity";

    private Button btnShowMe;
    private FrameLayout layout;
    private BluetoothController btController;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (((KVApp) getApplication()).getBluetoothSocket() != null)
            if (ConnectionThread.isConnected) {
                Intent toExam = new Intent(this, ExamChild.class);
                //bluetoothController.stop();
                startActivity(toExam);
                finish();
                return;
            }

        btController = new BluetoothController(this, this);
        layout = new FrameLayout(this);
        layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        btnShowMe = new Button(this);
        btnShowMe.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        btnShowMe.setText("ShowMe");
        if(Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            btnShowMe.setBackgroundDrawable( getResources().getDrawable(R.drawable.btn_game) );
        } else {
            btnShowMe.setBackground( getResources().getDrawable(R.drawable.btn_game));
        }
        btnShowMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btController.showMe();
            }
        });
        btnShowMe.setGravity(Gravity.CENTER);
        layout.addView(btnShowMe);
        setContentView(layout);
        btController.start();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        btController.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        btController.stop();
    }

    @Override
    public void notAvailable() {
        Toast.makeText(this, "Your cell doesn't support bluetooth", Toast.LENGTH_LONG).show();
        onBackPressed();
    }

    @Override
    public void connected(BluetoothSocket bluetoothSocket) {
        Utils.log(TAG, "Connected");
        Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
        ((KVApp) getApplication()).setBluetoothSocketAndStartConnection(bluetoothSocket);
        Intent toExam = new Intent(this, ExamChild.class);
        startActivity(toExam);
        finish();
    }

    @Override
    public void disconnected() {
        Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startAction() {
        Utils.log(BluetoothController.TAG, "startAction");
        btnShowMe.setEnabled(false);
    }

    @Override
    public void stopAction() {
        Utils.log(BluetoothController.TAG, "stopAction");
        btnShowMe.setEnabled(true);
    }
}
