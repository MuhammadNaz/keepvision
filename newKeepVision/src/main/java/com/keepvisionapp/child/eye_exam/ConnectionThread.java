
package com.keepvisionapp.child.eye_exam;

import android.bluetooth.BluetoothSocket;

import com.keepvision.eyeexamfar.eye_exam.MsgKV;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 17.05.2015.
 */
public class ConnectionThread extends Thread {

    public static String TAG = "ConnectionThread";

    private InputStream inputStream = null;
    private OutputStream outputStream = null;
    private int dataLength;
    private BluetoothSocket bluetoothSocket;
    private IConnectionThread iConnectionThread;
    public static volatile boolean isConnected = false;

    public interface IConnectionThread {
        void getData(MsgKV msgKV);
        void disconnected();
    }

    public ConnectionThread(BluetoothSocket bluetoothSocket) {
        this.bluetoothSocket = bluetoothSocket;
        Utils.log(TAG, "isConnected = " + bluetoothSocket.isConnected());
        try {
            inputStream = bluetoothSocket.getInputStream();
            outputStream = bluetoothSocket.getOutputStream();
        } catch (IOException e) {
            Utils.log(TAG, "error construct connection thread");
            e.printStackTrace();
        }
    }

    public void setIConnectionThread(IConnectionThread iConnectionThread) {
        this.iConnectionThread = iConnectionThread;
    }


    @Override
    public void run() {
        ByteArrayInputStream inputByteStream = null;
        ObjectInputStream in = null;
        byte[] buffer = new byte[1024];
        while (true) {
            try {
                dataLength = inputStream.read(buffer);
                if (dataLength == -1)
                    continue;
                Utils.log(TAG, "read");
                inputByteStream = new ByteArrayInputStream(buffer);
                in = new ObjectInputStream(inputByteStream);
                MsgKV msgKV = null;
                msgKV = (MsgKV) in.readObject();
                if (iConnectionThread != null)
                    iConnectionThread.getData(msgKV);
                in.close();
                inputByteStream.close();
            } catch (IOException e) {
                Utils.log(TAG, "Connection lost");
                close();
                if (iConnectionThread != null)
                    iConnectionThread.disconnected();
                e.printStackTrace();
                break;
            } catch (ClassNotFoundException e) {
                Utils.log(TAG, "ClassNotFound");
                if (iConnectionThread != null)
                    iConnectionThread.disconnected();
                e.printStackTrace();
                break;
            }
        }
    }


    public void write(MsgKV msgKV) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        Utils.log(TAG, "write");
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(msgKV);
            byte[] buffer = bos.toByteArray();
            outputStream.write(buffer);
            out.close();
            bos.close();
        } catch (IOException e) {
            Utils.log(TAG, "error write e " + e.getMessage());
            e.printStackTrace();
        }

    }

    public void close() {
        Utils.log(TAG, "close");
        isConnected = false;
        try {
            bluetoothSocket.close();
        } catch (IOException e) {
            Utils.log(TAG, "error close");
            e.printStackTrace();
        }
    }
}
