package com.keepvisionapp.child.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.keepvisionapp.child.R;
public class DialogWait {
    AlertDialog.Builder builder;
    public DialogWait(Activity activity) {
        builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.dialog_wait, null);
        builder.setCancelable(false).setView(view).setTitle("Please, wait");
    }
    public AlertDialog getDialog() {
        return builder.create();
    }
}
