package com.keepvisionapp.child.game.framework.impl;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;


import java.io.IOException;
import java.io.InputStream;

import com.keepvisionapp.child.game.framework.Graphics;
import com.keepvisionapp.child.game.framework.Pixmap;

public class AndroidGraphics implements Graphics {

    private final String TAG = "AndroidGraphics";
    AssetManager assets;
    Bitmap frameBuffer;
    Canvas canvas, canvasMini;
    Paint paint;
    Rect srcRect = new Rect();
    Rect dstRect = new Rect();
    Matrix rotator = new Matrix();
    ColorMatrix colorMatrix = new ColorMatrix();
    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);
    public AndroidGraphics(AssetManager assets, Bitmap frameBuffer) {
        this.assets = assets;
        this.frameBuffer = frameBuffer;
        this.canvas = new Canvas(frameBuffer);
        this.canvasMini = new Canvas();
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG | Paint.LINEAR_TEXT_FLAG);
    }

    public AndroidGraphics(AssetManager assets) {
        this.assets = assets;
        this.canvasMini = new Canvas();
        this.paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG | Paint.LINEAR_TEXT_FLAG);
        this.paint.setStrokeMiter(0);
        this.paint.setStrokeJoin(Paint.Join.BEVEL);
    }

    @Override
    public Pixmap newPixmap(String fileName, Graphics.PixmapFormat format) {
        Config config = null;
        if (format == PixmapFormat.RGB565)
            config = Config.RGB_565;
        else if (format == PixmapFormat.ARGB4444)
            config = Config.ARGB_4444;
        else
            config = Config.ARGB_8888;

        Options options = new Options();
        options.inPreferredConfig = config;
        options.inMutable = true;
        InputStream in = null;
        Bitmap bitmap = null;
        try {
            in = assets.open(fileName);
            bitmap = BitmapFactory.decodeStream(in, null, options);
            if (bitmap == null)
                throw new RuntimeException("Couldn't load bitmap from asset '"
                        + fileName + "'");
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load bitmap from asset '"
                    + fileName + "'");
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
        if (bitmap.getConfig() == Config.RGB_565)
            format = PixmapFormat.RGB565;
        else if (bitmap.getConfig() == Config.ARGB_4444)
            format = PixmapFormat.ARGB4444;
        else
            format = PixmapFormat.ARGB8888;
        return new AndroidPixmap(bitmap, format);
    }

    @Override
    public Pixmap newPixmapFromText(String text, String fontName, float size, int color, Graphics.PixmapFormat format) {
        Config config = null;
        if (format == PixmapFormat.RGB565)
            config = Config.RGB_565;
        else if (format == PixmapFormat.ARGB4444)
            config = Config.ARGB_4444;
        else
            config = Config.ARGB_8888;
        paint.setLinearText(true);
        paint.setColor(color);
        paint.setTypeface(Typeface.createFromAsset(assets, "fonts/" + fontName));
        paint.setTextSize(size);
        int width = (int) (paint.measureText(text) + 0.5f); // round
        float baseline = (int) (-paint.ascent() + 0.5f); // ascent() is negative
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap image = Bitmap.createBitmap(width, height, config);
        canvasMini.setBitmap(image);
        canvasMini.drawText(text, 0, baseline, paint);
        return new AndroidPixmap(image, format);
    }

    @Override
    public Pixmap newPixmapFromRect(int width, int height, int color, Style style, float strokeWidth, Graphics.PixmapFormat format) {
        Config config = null;
        if (format == PixmapFormat.RGB565)
            config = Config.RGB_565;
        else if (format == PixmapFormat.ARGB4444)
            config = Config.ARGB_4444;
        else
            config = Config.ARGB_8888;
        Paint.Style stylePaint = null;
        if (style == Style.FILL)
            stylePaint = Paint.Style.FILL;
        if (style == Style.FILL_AND_STROKE)
            stylePaint = Paint.Style.FILL_AND_STROKE;
        if (style == Style.STROKE || style == null) {
            stylePaint = Paint.Style.STROKE;
            paint.setStrokeWidth(strokeWidth);
        }
        if (style != Style.STROKE && style != Style.FILL_AND_STROKE)
            strokeWidth = 0;
        paint.setColor(color);
        paint.setStyle(stylePaint);
        Bitmap image = Bitmap.createBitmap(width + (int) (strokeWidth + 0.5f), height + (int) (strokeWidth + 0.5f), config);
        canvasMini.setBitmap(image);
        canvasMini.drawRect(0, 0, width, height, paint);
        return new AndroidPixmap(image, format);
    }

    @Override
    public Pixmap newPixmapFromCircle(float radius, int color, Style style, float strokeWidth, Graphics.PixmapFormat format) {
        Config config = null;
        if (format == PixmapFormat.RGB565)
            config = Config.RGB_565;
        else if (format == PixmapFormat.ARGB4444)
            config = Config.ARGB_4444;
        else
            config = Config.ARGB_8888;
        Paint.Style stylePaint = null;
        if (style == Style.FILL)
            stylePaint = Paint.Style.FILL;
        if (style == Style.FILL_AND_STROKE)
            stylePaint = Paint.Style.FILL_AND_STROKE;
        if (style == Style.STROKE || style == null) {
            stylePaint = Paint.Style.STROKE;
            paint.setStrokeWidth(strokeWidth);
        }
        if (style != Style.STROKE && style != Style.FILL_AND_STROKE)
            strokeWidth = 0;

        paint.setColor(color);
        paint.setStyle(stylePaint);
        Bitmap image = Bitmap.createBitmap((int)(2 * radius + strokeWidth + 0.5f), (int)(2 * radius + strokeWidth + 0.5f), config);
        canvasMini.setBitmap(image);
        canvasMini.drawCircle(radius + strokeWidth / 2, radius + strokeWidth / 2, radius, paint);
        return new AndroidPixmap(image, format);
    }

    @Override
    public Pixmap newPixmapFromPixmaps(Pixmap backPixmap, Pixmap frontPixmap, float x, float y) {
        canvasMini.setBitmap(((AndroidPixmap) backPixmap).bitmap);
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        canvasMini.drawBitmap(((AndroidPixmap) frontPixmap).bitmap, x, y, paint);
        return backPixmap;
    }

    @Override
    public void drawRotatePixmap(Pixmap pixmap, float degree) {
        pixmap.degree = degree;
        rotator.setTranslate(pixmap.x, pixmap.y);
        rotator.postRotate(degree, pixmap.px, pixmap.py);
        canvas.drawBitmap(((AndroidPixmap) pixmap).bitmap, rotator, paint);
    }

    @Override
    public void drawRotatePixmap(Pixmap pixmap, float degree, float px, float py) {
        pixmap.degree = degree;
        pixmap.px = px;
        pixmap.py = py;
        rotator.setTranslate(pixmap.x, pixmap.y);
        rotator.postRotate(degree, px, py);
        canvas.drawBitmap(((AndroidPixmap) pixmap).bitmap, rotator, paint);
    }

    @Override
    public void drawTransparentPixmap(Pixmap pixmap, float x, float y, int alpha) {
        if (alpha > 255)
            return;
        paint.setAlpha(255 - alpha);
        pixmap.alpha = alpha;
        pixmap.x = x;
        pixmap.y = y;
        canvas.drawBitmap(((AndroidPixmap)pixmap).bitmap, pixmap.x, pixmap.y, paint);
        paint.setAlpha(255);
    }

    @Override
    public void clear(int color) {
        canvas.drawRGB((color & 0xff0000) >> 16, (color & 0xff00) >> 8,
                (color & 0xff));
    }

    @Override
    public void drawPixel(float x, float y, int color) {
        paint.setColor(color);
        canvas.drawPoint(x, y, paint);
    }

    @Override
    public void drawLine(float x, float y, float x2, float y2, int color) {
        paint.setColor(color);
        canvas.drawLine(x, y, x2, y2, paint);
    }

    @Override
    public void drawRect(float x, float y, int width, int height, int color) {
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(x, y, x + width - 1, y + width - 1, paint);
    }

    @Override
    public void drawPixmap(Pixmap pixmap, float x, float y, int srcX, int srcY, int srcWidth, int srcHeight) {
        srcRect.left = srcX;
        srcRect.top = srcY;
        srcRect.right = srcX + srcWidth - 1;
        srcRect.bottom = srcY + srcHeight - 1;

        dstRect.left = (int) x;
        dstRect.top = (int) y;
        dstRect.right =(int) x + srcWidth - 1;
        dstRect.bottom =(int) y + srcHeight - 1;
        pixmap.x = x;
        pixmap.y = y;
        canvas.drawBitmap(((AndroidPixmap) pixmap).bitmap, srcRect, dstRect,
                paint);
    }

    @Override
    public void drawPixmap(Pixmap pixmap, float x, float y) {
        pixmap.x = x;
        pixmap.y = y;
        canvas.drawBitmap(((AndroidPixmap)pixmap).bitmap, x, y, paint);
    }

    @Override
    public void drawPixmapSat(Pixmap pixmap, float x, float y, float sat) {
        pixmap.x = x;
        pixmap.y = y;
        colorMatrix.setSaturation(sat);
        paint.setColorFilter(filter);
        canvas.drawBitmap(((AndroidPixmap)pixmap).bitmap, x, y, paint);
    }

    @Override
    public void drawRotatePixmap(Pixmap pixmap, float x, float y, float degree, float px, float py) {
        pixmap.x = x;
        pixmap.y = y;
        pixmap.px = px;
        pixmap.py = py;
        pixmap.degree = degree;
        rotator.setTranslate(x, y);
        rotator.postRotate(degree, px, py);
        canvas.drawBitmap(((AndroidPixmap) pixmap).bitmap, rotator, paint);
    }

    @Override
    public void drawRotateAndTransparentPixmap(Pixmap pixmap, float x, float y, float degree, float px, float py, int alpha) {
        if (alpha > 255)
            return;
        pixmap.x = x;
        pixmap.y = y;
        pixmap.px = px;
        pixmap.py = py;
        pixmap.degree = degree;
        pixmap.alpha = alpha;
        rotator.setTranslate(x, y);
        rotator.postRotate(degree, px, py);
        paint.setAlpha(255 - alpha);
        canvas.drawBitmap(((AndroidPixmap) pixmap).bitmap, rotator, paint);
        paint.setAlpha(255);
    }

    @Override
    public void drawText(String text, String fontName, float size, int color, float x, float y) {
        paint.setColor(color);
        paint.setTypeface(Typeface.createFromAsset(assets, "fonts/" + fontName));
        paint.setTextSize(size);
        canvas.drawText(text, x, y, paint);
    }

    @Override
    public int getWidth() {
        return frameBuffer.getWidth();
    }

    @Override
    public int getHeight() {
        return frameBuffer.getHeight();
    }
}
