package com.keepvisionapp.child.game.activities;

import android.content.Intent;

import com.keepvisionapp.child.activities.MainActivity;
import com.keepvisionapp.child.game.framework.Screen;
import com.keepvisionapp.child.game.framework.impl.GLGame;
import com.keepvisionapp.child.game.screens.LoadingScreen;

/**
 * Created by AbduLlah on 28.03.2015.
 */
public class KeepVisionGame extends GLGame {

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public Screen getStartScreen() {
        return new LoadingScreen(this);
    }
}
