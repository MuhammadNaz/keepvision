package com.keepvisionapp.child.game.framework;

/**
 * Created by AbduLlah on 30.03.2015.
 */
public interface Audio {
    public Music newMusic(String fileName);
    public Sound newSound(String fileName);
}
