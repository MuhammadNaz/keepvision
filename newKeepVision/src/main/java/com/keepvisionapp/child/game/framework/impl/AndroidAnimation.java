package com.keepvisionapp.child.game.framework.impl;

import android.util.Log;

import com.keepvisionapp.child.game.framework.Animation;
import com.keepvisionapp.child.game.framework.Graphics;
import com.keepvisionapp.child.game.framework.Pixmap;

/**
 * Created by AbduLlah on 29.03.2015.
 */
public class AndroidAnimation extends Animation implements Cloneable {

    Graphics g;
    public Pixmap pixmap;
    boolean isRepeat, isRotate, isAlpha, isMove;
    float degree;
    float rotX, rotY;
    float dstX, dstY;
    float duration;
    float velocityX, velocityY, velocityRotate, velocityAlpha;
    int alpha;
    public volatile boolean isShow;
    private volatile boolean isInit;
    public AndroidAnimation(Graphics g) {
        this.g = g;
    }
    private IAnimOnStop iAnimOnStop;

    public interface IAnimOnStop {
        public void onStop(Pixmap pixmap);
    }

    @Override
    public void draw(float deltaTime) {
        if (isRotate || isAlpha) {
            if (isRotate && isAlpha) {
                g.drawRotateAndTransparentPixmap(pixmap, pixmap.x + (int) (velocityX * deltaTime), pixmap.y + (int) (velocityY * deltaTime), pixmap.degree + velocityRotate * deltaTime,pixmap.px + (int) (velocityX * deltaTime), pixmap.py + (int) (velocityY * deltaTime), (int) (pixmap.alpha + velocityAlpha * deltaTime ));
                return;
            }
            if (isAlpha) {
                g.drawTransparentPixmap(pixmap, pixmap.x + (int) (velocityX * deltaTime), pixmap.y + (int) (velocityY * deltaTime), (int) (pixmap.alpha + velocityAlpha * deltaTime));
                return;
            }
            if (isRotate) {
                g.drawRotatePixmap(pixmap, pixmap.x + (int) (velocityX * deltaTime), pixmap.y + (int) (velocityY * deltaTime), pixmap.degree + velocityRotate * deltaTime, pixmap.px + (int) (velocityX * deltaTime), pixmap.py + (int) (velocityY * deltaTime));
            }
        }
        else
            g.drawPixmap(pixmap, pixmap.x + (int) (velocityX * deltaTime), pixmap.y + (int) (velocityY * deltaTime));
    }

    @Override
    public void create(Pixmap pixmap, float x, float y) {
        this.pixmap = pixmap;
        this.pixmap.x = x;
        this.pixmap.y = y;
    }

    public void setIAnimOnStop(IAnimOnStop iAnimOnStop) {
        this.iAnimOnStop = iAnimOnStop;
    }

    public IAnimOnStop getIAnimOnStop() {
        return iAnimOnStop;
    }

    @Override
    public void init(float duration) {
        if (pixmap == null)
            try {
                throw new Exception();
            } catch (Exception e) {
                Log.e("Exception", "Pixmap must not be null");
            }
        this.duration = duration;
        if (isMove) {
            velocityX = (Math.abs(pixmap.x - dstX)/duration);
            velocityY = (Math.abs(pixmap.y - dstY)/duration);
            if (dstX <  pixmap.x)
                velocityX *= -1;
            if (dstY < pixmap.y)
                velocityY *= -1;
        }
        if (isRotate) {
            velocityRotate = degree / duration;
            pixmap.px = rotX;
            pixmap.py = rotY;
        }
        if (isAlpha)
            velocityAlpha = alpha/duration;
        isInit = true;
    }
//TODO improve init() with show() according animationPlayer
    @Override
    public void show() {
        if (isInit) {
            isShow = true;
            if (!isRepeat)
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep((long) (duration * 1000f));
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        stop();
                        isShow = false;
                    }
                }).start();
        }
        else
            try {
                throw new Exception();
            } catch (Exception e) {
                Log.e("AnimException", "Before show should init");
                e.printStackTrace();
            }
    }

    @Override
    public void stop() {
        if (iAnimOnStop != null)
            iAnimOnStop.onStop(pixmap);
        isShow = false;
    }

    @Override
    public void setRepeat(boolean isRepeat) {
        this.isRepeat = isRepeat;
    }

    @Override
    public void setRotate(float degree, float x, float y, int dirRotate) {
        isRotate = true;
        this.degree = degree;
        this.rotX = x;
        this.rotY = y;
    }

    @Override
    public void setPosition(float x, float y) {
        isMove = true;
        this.dstX = x;
        this.dstY = y;
    }

    @Override
    public void setAlpha(int alpha) {
        isAlpha = true;
        this.alpha = alpha;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
