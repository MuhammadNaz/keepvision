package com.keepvisionapp.child.game.framework.impl;

import android.util.Log;

import com.keepvisionapp.child.game.framework.Animation;
import com.keepvisionapp.child.game.framework.Pixmap;
import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 01.05.2015.
 */
public class AnimationGL extends Animation implements Cloneable {

    public PixmapGL pixmapGL;
    PixmapGL priorPixmapGL;
    GLGraphics glGraphics;
    boolean isRepeat, isRotate, isAlpha, isMove;
    float degree;
    float rotX, rotY;
    float dstX, dstY;
    float duration, runningTime;
    float velocityX, velocityY, velocityRotate, velocityAlpha;
    int alpha, dirRot;
    public volatile boolean isShow;
    private volatile boolean isInit;
    private IAnimOnStop iAnimOnStop;


    public interface IAnimOnStop {
       public void onStop(PixmapGL pixmapGL);
    }


    public AnimationGL(GLGraphics glGraphics) {
        this.glGraphics = glGraphics;
    }

    @Override
    public void setRepeat(boolean isRepeat) {
        this.isRepeat = isRepeat;
        if (this.pixmapGL.isLog)
            Utils.log("gl", "setRepeat = " + isRepeat);
    }

    @Override
    public void setRotate(float degree, float x, float y, int dirRotate) {
        this.isRotate = true;
        this.degree = degree;
        this.rotX = x;
        this.rotY = y;
        this.dirRot = dirRotate;
        if (this.pixmapGL.isLog)
            Utils.log("gl", "setRotate x = " + degree + " rotX = " + rotX + " rotY = " + rotY + " dirRot = " + dirRotate);
    }

    @Override
    public void setPosition(float x, float y) {
        this.isMove = true;
        this.dstX = x;
        this.dstY = y;
        if (this.pixmapGL.isLog)
            Utils.log("gl", "setPosition x = " + x + " y = " + y);
    }

    @Override
    public void setAlpha(int alpha) {
        this.isAlpha = true;
        this.alpha = alpha;
        if (this.pixmapGL.isLog)
            Utils.log("gl", "setAlpha alpha = " + alpha);
    }

    @Override
    public void init(float duration) {
        if (pixmapGL == null)
            try {
                throw new Exception();
            } catch (Exception e) {
                Log.e("Exception", "PixmapGL must not be null");
            }
        this.duration = duration;
        if (isMove) {
            velocityX = (Math.abs(pixmapGL.x - dstX)/duration);
            velocityY = (Math.abs(pixmapGL.y - dstY)/duration);
            if (dstX <  pixmapGL.x)
                velocityX *= -1;
            if (dstY < pixmapGL.y)
                velocityY *= -1;
        }
        if (isRotate) {
            velocityRotate = degree / duration;
            pixmapGL.px = rotX;
            pixmapGL.py = rotY;
            pixmapGL.dirRotate = dirRot;
        }
        if (isAlpha)
            velocityAlpha = alpha/duration;
        try {
            priorPixmapGL = (PixmapGL) pixmapGL.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        isInit = true;
    }

    @Override
    public void show() {
        if (isInit) {
            isShow = true;
            runningTime = 0;
            if (this.pixmapGL.isLog)
                Utils.log("gl", "show anim");
        }
        else
            try {
                throw new Exception();
            } catch (Exception e) {
                Log.e("AnimationGL", "Before showing you should init");
                e.printStackTrace();
            }
    }

    public void setIAnimOnStop(IAnimOnStop iAnimOnStop) {
        this.iAnimOnStop = iAnimOnStop;
    }

    @Override
    public void stop() {
        if (this.pixmapGL.isLog)
            Utils.log("gl", "stop anim");
        if (iAnimOnStop != null)
            iAnimOnStop.onStop(pixmapGL);
        isShow = false;
        try {
            pixmapGL = (PixmapGL) priorPixmapGL.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void draw(float deltaTime) {
        if (isInit && isShow) {
            runningTime += deltaTime;
            if (runningTime > duration) {
                stop();
                if (isRepeat)
                    show();
                return;
            }
            if (isAlpha)
                pixmapGL.addAlpha(deltaTime * velocityAlpha);
            if (isRotate)
                pixmapGL.addRotate(deltaTime * velocityRotate);
            if (isMove)
                pixmapGL.addPosition(deltaTime * velocityX, deltaTime * velocityY);
            GLDrawer.draw(pixmapGL);
        }
    }

    @Override
    public void create(Pixmap pixmap, float x, float y) {
        pixmapGL = (PixmapGL) pixmap;
        pixmapGL.setPosition(x, y);
    }
}
