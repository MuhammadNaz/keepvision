package com.keepvisionapp.child.game.framework.impl;

import android.graphics.Bitmap;

import com.keepvisionapp.child.game.framework.Graphics;
import com.keepvisionapp.child.game.framework.Pixmap;


public class AndroidPixmap extends Pixmap {
    public Bitmap bitmap;
    public AndroidPixmap(Bitmap bitmap, Graphics.PixmapFormat format) {
        this.bitmap = bitmap;
        this.format = format;
    }

    @Override
    public int getWidth() {
        return bitmap.getWidth();
    }

    @Override
    public int getHeight() {
        return bitmap.getHeight();
    }

    @Override
    public Graphics.PixmapFormat getFormat() {
        return format;
    }

    @Override
    public void dispose() {
        bitmap.recycle();
    }
}
