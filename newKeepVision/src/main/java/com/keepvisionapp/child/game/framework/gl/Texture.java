package com.keepvisionapp.child.game.framework.gl;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.opengl.GLUtils;

import com.keepvisionapp.child.game.framework.Graphics;
import com.keepvisionapp.child.game.framework.Pixmap;
import com.keepvisionapp.child.game.framework.impl.AndroidPixmap;
import com.keepvisionapp.child.game.framework.impl.GLGraphics;
import com.keepvisionapp.child.tools.Utils;

public class Texture {

    public final String TAG = "Texture";

    GLGraphics glGraphics;
    int textureId;
    public Pixmap texPixmap;
    int minFilter;
    int magFilter;    
    int width, height;
    Graphics.PixmapFormat format;
    Graphics g;

    public Texture(GLGraphics glGraphics, Graphics g, AndroidPixmap pixmap) {
        this.glGraphics = glGraphics;
        this.texPixmap = pixmap;
        width = pixmap.getWidth();
        height = pixmap.getHeight();
        format = pixmap.getFormat();
        this.g = g;
        load();
    }

    private void load() {
        GL10 gl = glGraphics.getGL();
        int[] textureIds = new int[1];
        gl.glGenTextures(1, textureIds, 0);
        textureId = textureIds[0];
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureId);
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, getRelevantBitmap(), 0);
        setFilters(GL10.GL_NEAREST, GL10.GL_NEAREST);
        gl.glBindTexture(GL10.GL_TEXTURE_2D, 0);
    }

    private Bitmap getRelevantBitmap() {
        long time = System.currentTimeMillis();
        int w = width;
        int h = height;
        boolean isAvailable = false;
        while (w % 2 == 0) w = w / 2;
        if (w != 1) {
            w = findOptimSize(texPixmap.getWidth());
        } else {
            w = width;
            isAvailable = true;
        }
        while (h % 2 == 0) h = h / 2;
        if (h != 1) {
            h = findOptimSize(texPixmap.getHeight());
            isAvailable = false;
        } else {
            h = height;
        }
        if (!isAvailable) {
            width = w;
            height = h;
            Pixmap back = g.newPixmapFromRect(w, h, Color.TRANSPARENT, Graphics.Style.FILL, 0, Graphics.PixmapFormat.ARGB8888);
            texPixmap = g.newPixmapFromPixmaps(back, texPixmap, 0, 0);
        }
        Utils.log(TAG, "getRelevantSize = " + (float) (System.currentTimeMillis() - time));
        return ((AndroidPixmap) texPixmap).bitmap;
    }

    public int findOptimSize(int x) {
        int i = 2;
        while (true) {
            if (x > i && x < (i = i*2)) {
                x = i;
                break;
            }
            if (i > 2048)
                throw new IllegalArgumentException();
        }
        return x;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Graphics.PixmapFormat getFormat() {
        return format;
    }
    
    public void reload() {
        load();
        bind();
        setFilters(minFilter, magFilter);        
        glGraphics.getGL().glBindTexture(GL10.GL_TEXTURE_2D, 0);
    }
    
    public void setFilters(int minFilter, int magFilter) {
        this.minFilter = minFilter;
        this.magFilter = magFilter;
        GL10 gl = glGraphics.getGL();
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, minFilter);
        gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, magFilter);
    }    
    
    public void bind() {
        GL10 gl = glGraphics.getGL();
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureId);
    }
    
    public void dispose() {
        GL10 gl = glGraphics.getGL();
        gl.glBindTexture(GL10.GL_TEXTURE_2D, textureId);
        int[] textureIds = { textureId };
        gl.glDeleteTextures(1, textureIds, 0);
        texPixmap.dispose();
    }
}