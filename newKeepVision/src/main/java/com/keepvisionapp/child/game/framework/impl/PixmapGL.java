package com.keepvisionapp.child.game.framework.impl;

import com.keepvisionapp.child.game.framework.Graphics;
import com.keepvisionapp.child.game.framework.Pixmap;
import com.keepvisionapp.child.game.framework.gl.Texture;
import com.keepvisionapp.child.game.framework.gl.Vertices;
import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 01.05.2015.
 */
public class PixmapGL extends Pixmap{

    public String TAG = "PixmapGL";
    public Vertices vertices;
    public Texture texture;
    public GLGraphics glGraphics;
    public static final int ROTATE_RIGHT = -1;
    public static final int ROTATE_LEFT = 1;
    private int width, height;

    public PixmapGL(GLGraphics glGraphics, Graphics g, Pixmap pixmap) {
        this.glGraphics = glGraphics;
        this.width = pixmap.getWidth();
        this.height = pixmap.getHeight();
        init(g, (AndroidPixmap) pixmap);
    }


    private void init(Graphics g, AndroidPixmap pixmap) {
        vertices = new Vertices(glGraphics, 4, 6, true, true);
        texture = new Texture(glGraphics, g, pixmap);
        vertices.setVertices(new float[]{
                -width/2f, height/2f, 1, 1, 1,  1 - pixmap.alpha, 0, 0,
                width/2f, height/2f, 1, 1, 1, 1 - pixmap.alpha, (float) width/texture.getWidth(), 0,
                width/2f, -height/2f, 1, 1, 1, 1 - pixmap.alpha, (float) width/texture.getWidth(), (float) height/texture.getHeight(),
                -width/2f, -height/2f, 1, 1, 1, 1 - pixmap.alpha, 0, (float) height/texture.getHeight()}, 0, 32);
        vertices.setIndices(new short[]{
                0, 1, 2, 2, 3, 0
        }, 0, 6);
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;
        if (isLog)
            Utils.log("gl", "setPosition x = " + this.x + " y = " + this.y);
    }

    public void setRotation(float degree, int dirRotate) {
        hasRotation = true;
        this.dirRotate = dirRotate;
        this.degree = Math.abs(degree - (((int) degree / 360) * 360));
        if (isLog)
            Utils.log("gl", "setRotation rotation = " + this.degree + " dirRotate = " + dirRotate);
    }

    public void setAlpha(float alpha) {
        hasAlpha = true;
        this.alpha = alpha;
        vertices.setVertices(new float[]{
                -width/2f, height/2f, 1, 1, 1,  1 - this.alpha, 0, 0,
                width/2f, height/2f, 1, 1, 1, 1 - this.alpha, (float) width/texture.getWidth(), 0,
                width/2f, -height/2f, 1, 1, 1, 1 - this.alpha, (float) width/texture.getWidth(), (float) height/texture.getHeight(),
                -width/2f, -height/2f, 1, 1, 1, 1 - this.alpha, 0, (float) height/texture.getHeight()}, 0, 32);
        if (isLog)
            Utils.log("gl", "setAlpha alpha = " + this.alpha);
    }

    public void setScaling(float scaleX, float scaleY, float scaleZ) {
        hasScaling = true;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.scaleZ = scaleZ;
        if (isLog)
            Utils.log("gl", "setScaling scaleX = " + this.scaleX + " scaleY = " + this.scaleY + " scaleZ = " + this.scaleZ);
    }

    public void addPosition(float x, float y) {
        this.x += x;
        this.y += y;
        if (isLog)
            Utils.log("gl", "addPosition x = " + this.x + " y = " + this.y);
    }

    public void addAlpha(float alpha) {
        hasAlpha = true;
        this.alpha += alpha;
        /* vertices.setVertices(new float[]{
                -getWidth()/2f, getHeight()/2f, 1, 1, 1,  1 - this.alpha, 0, 0,
                this.getWidth()/2f, this.getHeight()/2f, 1, 1, 1, 1 - this.alpha, 1, 0,
                this.getWidth()/2f, -this.getHeight()/2f, 1, 1, 1, 1 - this.alpha, 1, 1,
                -this.getWidth()/2f, -this.getHeight()/2f, 1, 1, 1, 1 - this.alpha, 0, 1}, 0, 32);*/
        vertices.setVertices(new float[]{
                -width/2f, height/2f, 1, 1, 1,  1 - this.alpha, 0, 0,
                width/2f, height/2f, 1, 1, 1, 1 - this.alpha, (float) width/texture.getWidth(), 0,
                width/2f, -height/2f, 1, 1, 1, 1 - this.alpha, (float) width/texture.getWidth(), (float) height/texture.getHeight(),
                -width/2f, -height/2f, 1, 1, 1, 1 - this.alpha, 0, (float) height/texture.getHeight()}, 0, 32);
        if (isLog)
            Utils.log("gl", "addAlpha alpha = " + this.alpha);
    }

    public void addRotate(float degree) {
        hasRotation = true;
        if (this.degree > 360)
            this.degree = 0;
        this.degree += degree;
        if (isLog)
            Utils.log("gl", "addRotate degree = " + this.degree);
    }

    public void reload() {
        if (isLog)
            Utils.log("gl", "reload");
        texture.reload();
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public Graphics.PixmapFormat getFormat() {
        return texture.getFormat();
    }

    @Override
    public void dispose() {
        if (isLog)
            Utils.log("gl", "dispose");
        texture.dispose();
    }
}
