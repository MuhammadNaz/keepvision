package com.keepvisionapp.child.game.framework;


public interface Graphics {
    public static enum PixmapFormat {
        ARGB8888, ARGB4444, RGB565
    }

    public static enum Style {
        FILL, STROKE, FILL_AND_STROKE
    }

    public Pixmap newPixmap(String fileName, PixmapFormat format);

    public Pixmap newPixmapFromText(String text, String fontName, float size, int color, PixmapFormat format);

    public Pixmap newPixmapFromRect(int width, int height, int color, Style style, float strokeWidth, PixmapFormat format);

    public Pixmap newPixmapFromCircle(float radius, int color, Style style, float strokeWidth, PixmapFormat format);

    public Pixmap newPixmapFromPixmaps(Pixmap backPixmap, Pixmap frontPixmap, float x, float y);

    public void clear(int color);

    public void drawPixel(float x, float y, int color);

    public void drawLine(float x, float y, float x2, float y2, int color);

    public void drawRect(float x, float y, int width, int height, int color);

    public void drawPixmap(Pixmap pixmap, float x, float y, int srcX, int srcY,
                           int srcWidth, int srcHeight);

    public void drawPixmap(Pixmap pixmap, float x, float y);

    public void drawPixmapSat(Pixmap pixmap, float x, float y, float sat);

    public void drawRotatePixmap(Pixmap pixmap, float x, float y, float degree, float px, float py);

    public void drawRotateAndTransparentPixmap(Pixmap pixmap, float x, float y, float degree, float px, float py, int alpha);

    public void drawText(String text, String fontName, float size, int color, float x, float y);

    public void drawRotatePixmap(Pixmap pixmap, float degree);

    public void drawRotatePixmap(Pixmap pixmap, float degree, float px, float py);

    public void drawTransparentPixmap(Pixmap pixmap, float x, float y, int alpha);

    public int getWidth();

    public int getHeight();
}
