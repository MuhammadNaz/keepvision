package com.keepvisionapp.child.game.framework;

/**
 * Created by AbduLlah on 29.03.2015.
 */
public interface AnimationFrame {
    public void draw(float deltaTime);
    public void create(Pixmap pixmap, float x, float y);
}
