package com.keepvisionapp.child.game.screens;

import com.keepvisionapp.child.game.framework.Game;
import com.keepvisionapp.child.game.framework.Graphics;
import com.keepvisionapp.child.game.framework.Screen;
import com.keepvisionapp.child.game.logic.Assets;

/**
 * Created by AbduLlah on 30.03.2015.
 */
public class LoadingScreen extends Screen {

    Graphics g;
    public LoadingScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        g = game.getGraphics();
        Assets.loading = g.newPixmap("loading.png", Graphics.PixmapFormat.RGB565);
        Assets.errorFront = g.newPixmap("error_swipe.png", Graphics.PixmapFormat.RGB565);
        Assets.buttonStartActive = g.newPixmap("start_active.png", Graphics.PixmapFormat.RGB565);
        Assets.buttonStartNotActive = g.newPixmap("start_not_active.png", Graphics.PixmapFormat.RGB565);
        Assets.heart = g.newPixmap("heart.png", Graphics.PixmapFormat.RGB565);
        Assets.textScore =  g.newPixmap("text_score.png", Graphics.PixmapFormat.RGB565);
        Assets.textGameOver = g.newPixmap("text_gameover.png", Graphics.PixmapFormat.RGB565);
        Assets.textPause = g.newPixmap("pause.png", Graphics.PixmapFormat.RGB565);
        Assets.textDontJerk = g.newPixmap("dont_shake.png", Graphics.PixmapFormat.RGB565);
        Assets.textMotivation = g.newPixmap("text_motivation.png", Graphics.PixmapFormat.RGB565);
        Assets.lineForExplain = g.newPixmap("line_for_explain.png", Graphics.PixmapFormat.RGB565);
        Assets.textCheckVision = g.newPixmap("text_check_vision.png", Graphics.PixmapFormat.RGB565);
        Assets.textEyesNotDetected =  g.newPixmap("text_eyes_not.png", Graphics.PixmapFormat.RGB565);
        Assets.textCloser = g.newPixmap("text_closer.png", Graphics.PixmapFormat.RGB565);
        Assets.textForth = g.newPixmap("text_forth.png", Graphics.PixmapFormat.RGB565);
        Assets.progressInitItem = g.newPixmap("progress_item.png", Graphics.PixmapFormat.RGB565);
        Assets.progressInitUnder = g.newPixmap("progress_under.png", Graphics.PixmapFormat.RGB565);
        Assets.hand = g.newPixmap("hand.png", Graphics.PixmapFormat.RGB565);
        Assets.phone = g.newPixmap("phone.png", Graphics.PixmapFormat.RGB565);
        Assets.phone_obv = g.newPixmap("phone_adapt.png", Graphics.PixmapFormat.RGB565);
        for (int i = 0; i < 9; i++) {
            Assets.numbs[i] = g.newPixmap((i + 1) + ".png", Graphics.PixmapFormat.RGB565);
        }
        game.setScreen(new GameScreen(game));
    }

    @Override
    public void present(float deltaTime) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
