package com.keepvisionapp.child.game.framework.impl;

import java.util.ArrayList;
import java.util.List;

import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 30.03.2015.
 */
public class AnimationPlayer  {
    public List<AndroidAnimation> animationList = new ArrayList<AndroidAnimation>();
    private final String TAG = "AnimPlayer";
    private long startTime, delay;
    public void play(AndroidAnimation animation, final float delay, final float duration) {
        //if (!animationList.contains(animation))
            new Thread(new ThreadPlay(animation, delay, duration)).start();
    }
    public void stop(AndroidAnimation animation, boolean playOnStop) {
        if (animation.isShow) {
            animation.isShow = false;
            if (animation.getIAnimOnStop() != null && playOnStop)
                animation.getIAnimOnStop().onStop(animation.pixmap);
            animationList.remove(animation);
        }
    }

    private class ThreadPlay implements Runnable {
        float delay, duration;
        AndroidAnimation animation;
        float startX, startY, startPX, startPY;
        float startAlpha;
        long startTime;
        float startDegree;
        private ThreadPlay(AndroidAnimation animation, float delay, float duration) {
            this.delay = delay ;
            this.duration = duration;
            this.animation = animation;
        }
        @Override
        public void run() {
            startTime = System.currentTimeMillis();
            try {
                Thread.sleep((long)(delay * 1000f));
            } catch (InterruptedException e) {
                Utils.log("Error in thread");
                e.printStackTrace();
            }
            try {
                animation.init(duration);
                startX = animation.pixmap.x;
                startY = animation.pixmap.y;
                startPX = animation.pixmap.px;
                startPY = animation.pixmap.py;
                startAlpha = animation.pixmap.alpha;
                startDegree = animation.pixmap.degree;

                animationList.add(animation);
                animation.isShow = true;
                do {
                    animation.pixmap.x = startX;
                    animation.pixmap.y = startY;
                    animation.pixmap.px = startPX;
                    animation.pixmap.py = startPY;
                    animation.pixmap.alpha = startAlpha;
                    animation.pixmap.degree = startDegree;
                    try {
                        Thread.sleep((long)(duration * 1000f)) ;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                while (animation.isRepeat && animation.isShow);
                if (animation.isShow) {
                    if (animation.getIAnimOnStop() != null)
                        animation.getIAnimOnStop().onStop(animation.pixmap);
                    animationList.remove(animation);
                    animation.isShow = false;
                }
            } catch (Exception e) {
                if (animation.getIAnimOnStop() != null)
                    animation.getIAnimOnStop().onStop(animation.pixmap);
                animationList.remove(animation);
                animation.isShow = false;
                Utils.log("Exception since AnimationPlay" + e.getMessage());
            }
        }
    }
}
