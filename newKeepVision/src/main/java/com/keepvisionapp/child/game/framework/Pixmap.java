package com.keepvisionapp.child.game.framework;


public abstract class Pixmap implements Cloneable{
    public Graphics.PixmapFormat format;
    public float x, y, px, py;
    public float degree;
    public float alpha;
    public float scaleX, scaleY, scaleZ;
    public boolean hasRotation, hasScaling, hasAlpha;
    public int dirRotate;
    public boolean isLog;
    public abstract int getWidth();

    public abstract int getHeight();

    public abstract Graphics.PixmapFormat getFormat();

    public abstract void dispose();


    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
