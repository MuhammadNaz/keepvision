package com.keepvisionapp.child.game.logic;

import com.keepvisionapp.child.game.framework.math.Vector2;

/**
 * Created by AbduLlah on 29.03.2015.
 */
public class Optotype {
    public static final int LEFT = 0;
    public static final int RIGHT = 1;
    public static final int DOWN = 2;
    public static final int UP = 3;

    public static final int FROM_TOP = 0;
    public static final int FROM_BOTTOM = 1;
    public static final int FROM_LEFT = 2;
    public static final int FROM_RIGHT = 3;

    public int state;
    public float size;
    public int color;
    public int from;
    public float velocityX, velocityY, saturation;
    public Vector2 vector = new Vector2();
    public int index;
    public long lifeTime;
}
