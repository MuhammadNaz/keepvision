package com.keepvisionapp.child.game.framework;

import android.graphics.Color;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;

import com.keepvisionapp.child.tools.Utils;

public class TestScreen extends Screen {
    long startTime = System.nanoTime();
    int frames;
    Pixmap bob;
    Pixmap bobAlpha;
    Sound sound;

    public TestScreen(Game game) {
        super(game);    
        bob = game.getGraphics().newPixmap("bobrgb888.png", Graphics.PixmapFormat.RGB565);
        bobAlpha = game.getGraphics().newPixmap("bobargb8888.png", Graphics.PixmapFormat.ARGB4444);
        sound = game.getAudio().newSound("music.ogg");
        
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(game.getFileIO().readAsset("test.txt")));
            String text = in.readLine();
            in.close();
            
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(game.getFileIO().writeFile("test.txt")));
            out.write("This is a freaking test");
            out.close();
            
            in = new BufferedReader(new InputStreamReader(game.getFileIO().readFile("test.txt")));
            String text2 = in.readLine();
            in.close();
            
            Utils.log(text + ", " + text2);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void update(float deltaTime) {

    }

    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();
        Input inp = game.getInput();
        g.clear(Color.RED);
        g.drawLine(0,0,g.getWidth(), g.getHeight(), Color.BLUE);
        g.drawRect(20,20,100,100, Color.GREEN);
        g.drawPixmap(bob, (int) (g.getWidth()/4f), (int) (g.getHeight()/5f));
        g.drawPixmap(bobAlpha, (int) (g.getWidth()/3f), (int) (g.getHeight()/2f));
        g.drawPixmap(bob, 200, 200, 0, 0, 64, 64);
        for(int i=0; i < 2; i++) {
            if(inp.isTouchDown(i)) {
                g.drawPixmap(bob, inp.getTouchX(i), inp.getTouchY(i), 0, 0, 64, 64);
            }
        }
        
        g.drawPixmap(bob, (int)(inp.getAccelX() * 10) + 160 - 16, (int)(inp.getAccelY() * 10) + 240 - 16, 0, 0, 32, 32 );
        List<Input.KeyEvent> keyEvents = inp.getKeyEvents();
        int len = keyEvents.size();
        for(int i = 0; i < len; i++) {
            Utils.log(keyEvents.get(i).toString());
        }
        
        List<Input.TouchEvent> touchEvents = inp.getTouchEvents();
        len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            Utils.log(touchEvents.get(i).toString());
            if(touchEvents.get(i).type == Input.TouchEvent.TOUCH_SWIPE_LEFT)
                sound.play(1);
            if(touchEvents.get(i).type == Input.TouchEvent.TOUCH_SWIPE_RIGHT)
                sound.play(1);
        }
        frames++;
        if(System.nanoTime() - startTime > 1000000000l) {
            Utils.log("fps: " + frames + ", delta: " + deltaTime);
            frames = 0;
            startTime = System.nanoTime();
        }
    }

    @Override
    public void pause() {
        Utils.log("pause");
    }

    @Override
    public void resume() {
        Utils.log("resume");
    }

    @Override
    public void dispose() {
        Utils.log("dispose");
    }
}
