package com.keepvisionapp.child.game.framework;

/**
 * Created by AbduLlah on 30.03.2015.
 */
public abstract class Animation implements AnimationFrame{
    public abstract void setRepeat(boolean isRepeat);
    public abstract void setRotate(float degree, float x, float y, int dirRotate);
    public abstract void setPosition(float x, float y);
    public abstract void setAlpha(int alpha);
    public abstract void init(float duration);
    public abstract void show();
    public abstract void stop();
}
