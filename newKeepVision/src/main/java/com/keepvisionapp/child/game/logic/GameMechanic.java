package com.keepvisionapp.child.game.logic;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import java.util.Random;

import com.keepvisionapp.child.activities.BadEyes;
import com.keepvisionapp.child.activities.GoodEyes;
import com.keepvisionapp.child.eye_detection.MeasurementDistanceToEyes;
import com.keepvisionapp.child.game.framework.Game;
import com.keepvisionapp.child.game.framework.Input;
import com.keepvisionapp.child.game.framework.impl.GLGame;
import com.keepvisionapp.child.eye_detection.MeasureControllerGame;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.Utils;
import com.keepvisionapp.child.tracking.LightController;

/**
 * Created by AbduLlah on 28.03.2015.
 */
public class GameMechanic implements MeasureControllerGame.IResultMeasurementForGame, LightController.IMeasurementLight {

    private final String TAG = "GameMechanic";
    private final int INITIALLY_START_DISTANCE = -1;
    private MeasureControllerGame measureControllerGame;
    private IGameMechanicForScreen iGameMechanicForScreen;
    public World world;
    public boolean gameOver = false, isBoss = false;
    public GameState state;
    private Game game;
    public float startDistance = INITIALLY_START_DISTANCE, deltaDistance, normVelocity, detltaTimeNextOptotype;
    private Random random = new Random();
    private long lastTimeCreatingOptotype;
    private DataDiagnosticController dataDiagnosticController = new DataDiagnosticController();
    private LightController lightController;
    private DisplayMetrics metrics;

    public int progressOfInit;

    public enum GameState {
        Init,
        Ready,
        Running,
        Paused,
        GameOver
    }

    public interface IGameMechanicForScreen {
        public void drawErrorSwipe();
        public void drawSuccesSwipe(int score, int swipeSide, int index);
        public void createNewOptotypePixmap(Optotype optotype);
        public void failRemoveOptotypePixmap();
    }

    public GameMechanic(Game game, IGameMechanicForScreen iGameMechanicForScreen) {
        this.iGameMechanicForScreen = iGameMechanicForScreen;
        measureControllerGame = new MeasureControllerGame((GLGame) game, this);
        this.game = game;
        metrics = ((GLGame) game).getResources().getDisplayMetrics();
        lightController = new LightController((GLGame) game, this);
        initGame();
    }

    public void initGame() {
        Utils.log(TAG, "initGame");
        world = new World(((GLGame) game).frameBufferWidth, ((GLGame) game).frameBufferHeight, dataDiagnosticController);
        state = GameState.Init;
        try {
            lightController.start();
        } catch (LightController.LightControllerException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        try {
            world.brightOfWindow = Settings.System.getInt(((GLGame) game).getApplicationContext().getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS);
            Utils.log(TAG, "brightOfWindow = " + world.brightOfWindow);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        world.lux = (int) lightController.getCurrentLux();
        world.backColor = Color.WHITE;
        normVelocity = ((float)(((GLGame) game).getWindowManager().getDefaultDisplay().getHeight()) / world.height) * World.VELOCITY_CONST;
    }

    public void startGame() {
        Utils.log(TAG, "startGame");
        state = GameState.Running;
        measureControllerGame.runningGame();
        dataDiagnosticController.setBeginGlobalData(startDistance, System.currentTimeMillis(), world.lux, world.brightOfWindow);
    }

    public void keepOnGame() {
        Utils.log(TAG, "restartGame");
        state = GameState.Running;
        measureControllerGame.runningGame();
    }

    public void update(float deltaTime) {
        if (gameOver || state == GameState.Paused)
            return;
        world.commonTime +=  deltaTime;
        if (world.update(deltaTime)) {
            iGameMechanicForScreen.failRemoveOptotypePixmap();
            wrong();  //TODO - terrible solution!!! but for unit sequence of optotypes it works.
        }
        if (world.optotypeList.size() == 0)
            if (world.countOptotype % 1 == 0) {
                if (world.optotypeList.size() == 0) {
                    isBoss = true;
                    createNewOptotypePixmap(Color.BLACK);
                } else
                    return;
            } else {
                if (world.optotypeList.size() == 0) {
                    createNewOptotypePixmap(Color.BLACK);
                    //createNewOptotypePixmap(randomColor());
                } else {
                    if ((System.currentTimeMillis() - lastTimeCreatingOptotype) / 1000f > detltaTimeNextOptotype) {
                        createNewOptotypePixmap(Color.BLACK);
                        //createNewOptotypePixmap(randomColor());
                    }
                }
            }
    }


    public void swiped(int type) {
        if (world.optotypeList.size() != 0) {
            boolean isSuccess = false;
            switch (type) {
                case Input.TouchEvent.TOUCH_SWIPE_LEFT:
                    if (world.optotypeList.get(0).state == Optotype.LEFT) {
                        isSuccess = true;
                    }
                    else isSuccess = false;
                    break;
                case Input.TouchEvent.TOUCH_SWIPE_DOWN:
                    if (world.optotypeList.get(0).state == Optotype.DOWN) {
                        isSuccess = true;
                    }
                    else isSuccess = false;
                    break;
                case Input.TouchEvent.TOUCH_SWIPE_RIGHT:
                    if (world.optotypeList.get(0).state == Optotype.RIGHT) {
                        isSuccess = true;
                    }
                    else isSuccess = false;
                    break;
                case Input.TouchEvent.TOUCH_SWIPE_UP:
                    if (world.optotypeList.get(0).state == Optotype.UP) {
                        isSuccess = true;
                    }
                    else isSuccess = false;
                    break;
            }
            if (isSuccess) {
                success();
            } else {
                iGameMechanicForScreen.drawErrorSwipe();
                wrong();
            }
        }
    }

    public void pauseGame() {
        Utils.log(TAG, "pauseGame");
        state = GameState.Init;
        measureControllerGame.stopMeasure();
    }

    public void resumeGame() {
        Utils.log(TAG, "resumeGame");
        measureControllerGame.startMeasure();
    }

    public void stopGame(boolean isWin) {
        Utils.log(TAG, "stopGame");
        gameOver = true;
        state = GameState.GameOver;
        Activity activity = (GLGame) game;
        float doz = world.level / (float) (World.COUNT_LEVELS);
        dataDiagnosticController.setEndGlobalData(world.score, doz);
        dataDiagnosticController.pushToServer(activity);
        if (!isWin) {
            Intent toBad = new Intent(activity, BadEyes.class);
            toBad.putExtra(Constants.EYE_TRACKING_RESULT_VALUE, (doz * 100f));
            activity.startActivity(toBad);
        }
        else {
            activity.startActivity(new Intent(activity, GoodEyes.class));
        }
        ((GLGame) game).finish();
    }

    public void disposeGame() {
        measureControllerGame.stopMeasure();
        try {
            lightController.stop();
        } catch (LightController.LightControllerException e) {
            e.printStackTrace();
        }
        measureControllerGame = null;
        world = null;
    }

    private int randomColor() {
        int color = random.nextInt(6);
        switch (color) {
            case 0: color = Color.GREEN;break;
            case 1: color = Color.BLUE;break;
            case 2: color = Color.GRAY;break;
            case 3: color = Color.LTGRAY;break;
            case 4: color = Color.CYAN;break;
            case 5: color = Color.MAGENTA;break;
        }
        return color;
    }

    private void upLevel() {
        world.level++;
    }

    private void downLevel() {
        if (world.level > 1)
            --world.level;
    }

    private void success() {
        int scoreUp = (int) ((((world.optotypeList.get(0).vector.y) * 100)/world.height)/10f) + 1;
        world.score += scoreUp;
        world.combo += 20;
        iGameMechanicForScreen.drawSuccesSwipe(scoreUp, world.optotypeList.get(0).state, world.optotypeList.get(0).index);
        world.removeOptotype(world.optotypeList.get(0).index);
        if (isBoss) {
            world.health = 3;
            if (world.level == World.COUNT_LEVELS) {
                stopGame(true);
                return;
            }
            upLevel();
        }
        isBoss = false;
    }

    private void wrong() {
        if (isBoss) {
            --world.health;
            if (world.health == 0) {
                if (world.optotypeList.size() != 0) //by swipes
                    world.removeOptotype(world.optotypeList.get(0).index);
                stopGame(false);
                return;
            } else
                if (world.health == 1)
                        downLevel();
        } else
            world.combo = 0;
    }

    private void createNewOptotypePixmap(int color) {
        Utils.log(TAG, "level = " + world.level);
        Optotype optotype = new Optotype();
        int state = random.nextInt(4);
        int from, velocity, velocityX = 0, velocityY = 0;
        int startX = 0, startY = world.height;
        //float size = (World.COUNT_LEVELS + 1 - world.level) * startDistance / 7.2f;
        //float size = (float) ((2 * startDistance * Math.tan(((((10 / (float) world.level))) / 60 / 2) / 180 * Math.PI)) * 5);  //1 cm = 37.795276 px; 1 px = 0.026458 cm
        float size = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, (float) ((2 * startDistance * Math.tan((((10 / (float) world.level) / 2) / 60) / 180 * Math.PI)) * 5) * 10, metrics);  //1 cm = 37.795276 px; 1 px = 0.026458 cm
        optotype.color = color;
        optotype.size = size;
        optotype.state = state;
        if (color == Color.BLACK) {
            from = Optotype.FROM_TOP;
            velocity = (int) normVelocity;
            velocityY = -velocity;
            velocityX = 0;
            optotype.saturation = 1;
            startY = world.height + (int) size;
            startX = (int) (size + 0.5) + random.nextInt((int) (world.width - 2 * size));
        } else {
            /*velocity = (int) ((random.nextFloat() * 0.2f + 1) * normVelocity); // 1 - 1.2*/
            velocity = (int) (world.combo * 20 + normVelocity);
            //from = random.nextInt(4);
            size = (int) ((random.nextFloat() * 2.2f + 1) * size); //1 - 3.2
            from = Optotype.FROM_TOP;
            if (from == Optotype.FROM_TOP) {
                startX = random.nextInt((int) (world.width - size));
                startY = (int) -size;
                velocityX = 0;
                velocityY = velocity;
            }
            if (from == Optotype.FROM_LEFT) {
                startX = (int) -size;
                startY = random.nextInt((int) (world.height - size));
                velocityX = velocity;
                velocityY = 0;
            }
            if (from == Optotype.FROM_RIGHT) {
                startX = world.width;
                startY = random.nextInt((int) (world.height - size));
                velocityX = -velocity;
                velocityY = 0;
            }
            if (from == Optotype.FROM_BOTTOM) {
                velocityY = -velocity;
                velocityX = 0;
                startX = random.nextInt((int) (world.width - size));
                startY = world.height;
            }
        }
        optotype.vector.x = startX;
        optotype.vector.y = startY;
        optotype.velocityX = velocityX;
        optotype.velocityY = velocityY;
        optotype.from = from;
        optotype.index = world.countOptotype;
        detltaTimeNextOptotype = ((3 - world.level) / (float) World.COUNT_LEVELS + 0.5f) * (random.nextFloat() * 0.4f + 0.8f);
        Utils.log(TAG, "deltaTimeNextOptotype = " + detltaTimeNextOptotype);
        Utils.log(TAG, " countOptotype = " + world.countOptotype);
        Utils.log(TAG, " size = " + optotype.size);
        lastTimeCreatingOptotype = System.currentTimeMillis();
        //world.optotypeList.add(optotype);
        float respectiveDOZ = world.level / World.COUNT_LEVELS;
        dataDiagnosticController.addOptotypeData(optotype.index, optotype.color, optotype.state, optotype.from, optotype.size, velocity, respectiveDOZ);
        iGameMechanicForScreen.createNewOptotypePixmap(optotype);
        ++world.countOptotype;
    }

    @Override
    public void lightBad(float lux) {
        if (world != null)
            world.lux = (int) lux;
    }

    @Override
    public void lightGood(float lux) {
        if (world != null)
            world.lux = (int) lux;
    }

    @Override
    public void getResultMeasurementForGame(MeasureControllerGame.ResultMeasurementGame result, float value) {
        if (result == MeasureControllerGame.ResultMeasurementGame.INIT) {
            state = GameState.Init;
            this.progressOfInit = (int) value;
        }
        if (result == MeasureControllerGame.ResultMeasurementGame.READY) {
            state = GameState.Ready;
            this.progressOfInit = 0;
            if (this.startDistance == INITIALLY_START_DISTANCE) {//after pause-resume
                this.startDistance = value;
                startGame();
            } else {
                keepOnGame();
            }
        }
        if (result == MeasureControllerGame.ResultMeasurementGame.RUNNING) {
            state = GameState.Running;
        }
        if (result == MeasureControllerGame.ResultMeasurementGame.TO_PAUSE) {
            state = GameState.Paused;
            if (value != MeasurementDistanceToEyes.ERROR_EYES_DETECTION) {
                dataDiagnosticController.setErrorCloseOpt(world.optotypeList.get(0).index, value);
            }
            deltaDistance = value;
        }
    }
}
