package com.keepvisionapp.child.game.logic;

import android.app.Activity;

import com.keepvisionapp.child.tools.Utils;
import com.keepvisionapp.child.tracking.DataKeepVision;
import com.keepvisionapp.child.tracking.PostDataTracking;

/**
 * Created by AbduLlah on 20.04.2015.
 */
public class DataDiagnosticController {
    private PostDataTracking postDataTracking;
    private DataKeepVision.DataDiagnostic dataDiagnostic = new DataKeepVision.DataDiagnostic();

    public static String TAG = "DataDiagnosticController";

    public void pushToServer(final Activity activity) {

        Utils.log(TAG, "GlobalData doz =" + dataDiagnostic.DOZ + "\n score =" +
        dataDiagnostic.score +  "\nlux ="
        + dataDiagnostic.lux + "\nstartTimeString = " + dataDiagnostic.startTime);
        for (int i = 0; i < dataDiagnostic.optotypeDiagnosticList.size(); i++) {
            DataKeepVision.DataOptotypeDiagnostic dataOptotypeDiagnostic = dataDiagnostic.optotypeDiagnosticList.get(i);
            Utils.log(TAG,"index = " + dataOptotypeDiagnostic.index +  "\ndeltaClose = " + dataOptotypeDiagnostic.deltaClose + "\nlifeTime =" +
            dataOptotypeDiagnostic.lifeTime + "\nvelocity" + dataOptotypeDiagnostic.velocity + "\ncolor" + dataOptotypeDiagnostic.color + "\ncountError =" + dataOptotypeDiagnostic.countError +
            "\nsize = " + dataOptotypeDiagnostic.size + "\n colorBack = " + dataOptotypeDiagnostic.colorBack + "\nfrom = " + dataOptotypeDiagnostic.from + "\nstate = " + dataOptotypeDiagnostic.state);
        }
        postDataTracking = new PostDataTracking(PostDataTracking.CODE_EVENT_DIAGNOSTIC, dataDiagnostic, activity);
        postDataTracking.execute(true, true);
    }

    public void setBeginGlobalData(float distanceToEyes,  long startTime, int lux, int brightOfWindow) {
        dataDiagnostic.distanceToEyes = distanceToEyes;
        dataDiagnostic.lux = lux;
        dataDiagnostic.brightOfWindow = brightOfWindow;
        dataDiagnostic.setStartTime(startTime);
    }

    public void setEndGlobalData(int score, float DOZ) {
        dataDiagnostic.DOZ = DOZ;
        dataDiagnostic.score = score;
    }

    public void addOptotypeData(int index, int color, int state, int from, float size, float velocity, float respectiveDOZ) {
        DataKeepVision.DataOptotypeDiagnostic dataOptotypeDiagnostic = null;
        try {
            dataOptotypeDiagnostic = dataDiagnostic.optotypeDiagnosticList.get(index);
        }catch (Exception e) {
            dataOptotypeDiagnostic = new DataKeepVision.DataOptotypeDiagnostic();
            dataDiagnostic.optotypeDiagnosticList.add(dataOptotypeDiagnostic);
        }
        dataOptotypeDiagnostic.setColor(color);
        dataOptotypeDiagnostic.from = from;
        dataOptotypeDiagnostic.index = index;
        dataOptotypeDiagnostic.state = state;
        dataOptotypeDiagnostic.size = size;
        dataOptotypeDiagnostic.velocity = velocity;
        dataOptotypeDiagnostic.respectiveDOZ = respectiveDOZ;
    }

    public void addCountErrorOpt(int index) {
        DataKeepVision.DataOptotypeDiagnostic dataOptotypeDiagnostic = dataDiagnostic.optotypeDiagnosticList.get(index);
        dataOptotypeDiagnostic.countError++;
    }

    public void setErrorCloseOpt(int index, float deltaClose) {
        DataKeepVision.DataOptotypeDiagnostic dataOptotypeDiagnostic = dataDiagnostic.optotypeDiagnosticList.get(index);
        dataOptotypeDiagnostic.deltaClose = (dataOptotypeDiagnostic.deltaClose < deltaClose) ?  deltaClose : dataOptotypeDiagnostic.deltaClose;
    }

    public void setOptotypeFinishData(int index, int colorBack, long lifeTime) {
        DataKeepVision.DataOptotypeDiagnostic dataOptotypeDiagnostic = dataDiagnostic.optotypeDiagnosticList.get(index);
        dataOptotypeDiagnostic.lifeTime = lifeTime;
        dataOptotypeDiagnostic.setColorBack(colorBack);
    }
}
