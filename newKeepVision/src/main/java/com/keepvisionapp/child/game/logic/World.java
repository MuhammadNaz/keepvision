package com.keepvisionapp.child.game.logic;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 29.03.2015.
 */
public class World {

    public final static String TAG = "World";

    public World(int width, int height, DataDiagnosticController dataDiagnosticController) {
        this.width = width;
        this.height = height;
        this.dataDiagnosticController = dataDiagnosticController;
    }
    public int health = 3;
    public float commonTime = 0;
    public int score = 0;
    public List<Optotype> optotypeList = new ArrayList<Optotype>();
    public int level = 1; //1...COUNT_LEVELS, specify to size of optotype -> DOZ
    public int lux;
    public int backColor = Color.WHITE;
    public static final int VELOCITY_CONST = 250;
    public static final int COUNT_LEVELS = 20;
    public int width;
    public int height;
    public int countOptotype, combo;
    public int brightOfWindow = -1;
    private DataDiagnosticController dataDiagnosticController;

    public boolean update(float deltaTime) { //return an boolean which is responsible for invoking "GameMechanic.wrong()"
        boolean isWrong = false;
        for (int i = 0; i < optotypeList.size(); i++) {
            optotypeList.get(i).lifeTime += deltaTime;
            optotypeList.get(i).vector.add(optotypeList.get(i).velocityX * deltaTime, optotypeList.get(i).velocityY * deltaTime);
            if (0 > optotypeList.get(i).vector.y) {
                dataDiagnosticController.addCountErrorOpt(optotypeList.get(i).index);
                removeOptotype(optotypeList.get(i).index);
                isWrong = true;
            }
        }
        return isWrong;
    }

    public void removeOptotype(int index) {
        for (int i = 0; i < optotypeList.size(); i++) {
            if (optotypeList.get(i).index == index) {
                dataDiagnosticController.setOptotypeFinishData(index, backColor, optotypeList.get(i).lifeTime);
                Utils.log(TAG, "REMOVED");
                optotypeList.remove(i);
                return;
            }
        }
        Utils.log(TAG, "NOT_REMOVED");
    }
}
