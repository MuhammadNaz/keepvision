package com.keepvisionapp.child.game.framework.impl;

import javax.microedition.khronos.opengles.GL10;

import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 02.05.2015.
 */
public class GLDrawer {

    public static final String TAG = "GLDrawer";

    public static synchronized void draw(PixmapGL pixmapGL) {
        GL10 gl = pixmapGL.glGraphics.getGL();
        gl.glLoadIdentity();

        pixmapGL.texture.setFilters(GL10.GL_LINEAR, GL10.GL_LINEAR);
        pixmapGL.texture.bind();

        gl.glTranslatef(pixmapGL.x, pixmapGL.y, 0);

        if (pixmapGL.hasRotation) {
            if (pixmapGL.isLog) {
                Utils.log("gl", "draw rotatem px = " + pixmapGL.px + " py = " + pixmapGL.py + " dir = " + pixmapGL.dirRotate);
            }
            gl.glRotatef(pixmapGL.degree, pixmapGL.px, pixmapGL.py, pixmapGL.dirRotate);
        }
        if (pixmapGL.hasScaling) {
            if (pixmapGL.isLog)
                Utils.log("gl", "draw scale");
            gl.glScalef(pixmapGL.scaleX, pixmapGL.scaleY, pixmapGL.scaleZ);
        }
        pixmapGL.vertices.draw(GL10.GL_TRIANGLES, 0, 6);
    }
}
