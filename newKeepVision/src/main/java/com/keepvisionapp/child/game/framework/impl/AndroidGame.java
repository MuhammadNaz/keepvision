package com.keepvisionapp.child.game.framework.impl;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Window;
import android.view.WindowManager;

import com.keepvisionapp.child.User;
import com.keepvisionapp.child.activities.ChildrenList;
import com.keepvisionapp.child.game.framework.Audio;
import com.keepvisionapp.child.game.framework.FileIO;
import com.keepvisionapp.child.game.framework.Game;
import com.keepvisionapp.child.game.framework.Graphics;
import com.keepvisionapp.child.game.framework.Input;
import com.keepvisionapp.child.game.framework.Screen;


public abstract class AndroidGame extends Activity implements Game {
    protected AndroidFastRenderView renderView;
    Graphics graphics;
    Audio audio;
    Input input;
    FileIO fileIO;
    Screen screen;
    WakeLock wakeLock;

    public final int SMARTPHONE_WIDTH = 1280;
    public final int SMARTPHONE_HEIGHT = 720;
    public final int TABLET_WIDTH = 1920;
    public final int TABLET_HEIGHT = 1080;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (User.getInstance().getCurrentChild() == null) {
            startActivity(new Intent(this, ChildrenList.class));
            finish();
            return;
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        boolean isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        //int frameBufferWidth = isLandscape ? 480 : 320;
        //int frameBufferHeight = isLandscape ? 320 : 480;
        int frameBufferWidth, frameBufferHeight;
        /*if (getResources().getBoolean(R.bool.isTablet)) {
            frameBufferWidth = isLandscape ? TABLET_WIDTH : TABLET_HEIGHT;
            frameBufferHeight = isLandscape ? TABLET_HEIGHT : TABLET_WIDTH;
        }
        else {
            frameBufferWidth = isLandscape ? SMARTPHONE_WIDTH : SMARTPHONE_HEIGHT;
            frameBufferHeight = isLandscape ? SMARTPHONE_HEIGHT : SMARTPHONE_WIDTH;
        }*/

      //  if (getWindowManager().getDefaultDisplay().getWidth() < frameBufferWidth && getWindowManager().getDefaultDisplay().getHeight() < frameBufferHeight) {
            frameBufferWidth = getWindowManager().getDefaultDisplay().getWidth();
            frameBufferHeight = getWindowManager().getDefaultDisplay().getHeight();
       // }

        Bitmap frameBuffer = Bitmap.createBitmap(frameBufferWidth,
                frameBufferHeight, Config.ARGB_8888);

        float scaleX = (float) frameBufferWidth
                / getWindowManager().getDefaultDisplay().getWidth();
        float scaleY = (float) frameBufferHeight
                / getWindowManager().getDefaultDisplay().getHeight();

        renderView = new AndroidFastRenderView(this, frameBuffer);
        graphics = new AndroidGraphics(getAssets(), frameBuffer);
        fileIO = new AndroidFileIO(getAssets());
        audio = new AndroidAudio(this);
        input = new AndroidInput(this, renderView, scaleX, scaleY);
        screen = getStartScreen();
        setContentView(renderView);
        
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "GLGame");
    }

    @Override
    public void onResume() {
        super.onResume();
        wakeLock.acquire();
        screen.resume();
        renderView.resume();
    }

    @Override
    public void onPause() {
        super.onPause();
        wakeLock.release();
        renderView.pause();
        screen.pause();
        if (isFinishing())
            screen.dispose();
    }

    @Override
    public Input getInput() {
        return input;
    }

    @Override
    public FileIO getFileIO() {
        return fileIO;
    }

    @Override
    public Graphics getGraphics() {
        return graphics;
    }

    @Override
    public Audio getAudio() {
        return audio;
    }

    @Override
    public void setScreen(Screen screen) {
        if (screen == null)
            throw new IllegalArgumentException("Screen must not be null");

        this.screen.pause();
        this.screen.dispose();
        screen.resume();
        screen.update(0);
        this.screen = screen;
    }
    
    public Screen getCurrentScreen() {
        return screen;
    }
}