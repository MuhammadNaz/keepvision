package com.keepvisionapp.child.game.framework.impl;

import android.view.View.OnTouchListener;

import java.util.List;

import com.keepvisionapp.child.game.framework.Input;

public interface TouchHandler extends OnTouchListener {
    public boolean isTouchDown(int pointer);
    
    public int getTouchX(int pointer);
    
    public int getTouchY(int pointer);
    
    public List<Input.TouchEvent> getTouchEvents();

}
