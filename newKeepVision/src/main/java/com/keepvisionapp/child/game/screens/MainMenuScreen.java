package com.keepvisionapp.child.game.screens;

import android.content.Intent;
import android.widget.Toast;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import com.keepvisionapp.child.User;
import com.keepvisionapp.child.activities.ChildrenList;
import com.keepvisionapp.child.game.framework.Game;
import com.keepvisionapp.child.game.framework.Input;
import com.keepvisionapp.child.game.framework.Screen;
import com.keepvisionapp.child.game.framework.impl.GLDrawer;
import com.keepvisionapp.child.game.framework.impl.GLGame;
import com.keepvisionapp.child.game.framework.impl.GLGraphics;
import com.keepvisionapp.child.game.framework.impl.PixmapGL;
import com.keepvisionapp.child.game.logic.Assets;

/**
 * Created by AbduLlah on 28.03.2015.
 */
public class MainMenuScreen extends Screen {

    Input input;
    GLGraphics glGraphics;

    int w, h;
    User user = User.getInstance();
    PixmapGL pStart, pToChildren;

    public MainMenuScreen(Game game) {
        super(game);
        glGraphics = ((GLGame)game).getGLGraphics();
        input = game.getInput();
        w = ((GLGame)game).frameBufferWidth;
        h = ((GLGame)game).frameBufferHeight;
        pStart = new PixmapGL(glGraphics, game.getGraphics(), Assets.optionsStart);
        pToChildren = new PixmapGL(glGraphics, game.getGraphics(),Assets.optionChildList);

        pStart.setPosition(w/2f, h/2f);
        pToChildren.setPosition(w - w / 4, h - h / 10f);
    }

    @Override
    public void update(float deltaTime) {
        List<Input.TouchEvent> touchEventList = input.getTouchEvents();
        game.getInput().getKeyEvents();
        for (int i = 0; i < touchEventList.size(); i++) {
            Input.TouchEvent touchEvent = touchEventList.get(i);
            if (touchEvent.type == Input.TouchEvent.TOUCH_UP) {
                if (inBounds(touchEvent,pStart.x, pStart.y,  pStart.getWidth(), pStart.getHeight())) {
                    if (user.getCurrentChild() != null)
                        game.setScreen(new GameScreen(game));
                    else {
                        ((GLGame) game).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText((GLGame) game, "Сперва выберите ребенка", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
                if (inBounds(touchEvent,pToChildren.x, pToChildren.y,  pToChildren.getWidth(), pToChildren.getHeight())) {
                    ((GLGame) game).startActivity(new Intent((GLGame) game, ChildrenList.class));
                    ((GLGame) game).finish();
                }
            }
        }
    }

    private boolean inBounds(Input.TouchEvent event, float x, float y, int width, int height) {
        float worldX = ((float) event.x / glGraphics.getWidth()) * w;
        float worldY = ((float) event.y / glGraphics.getHeight()) * h;
        if(worldX > (x - width - 1) && worldX < x + width - 1 &&
                (h - worldY) > (y - height - 1) && ( h - worldY ) < y + height - 1)
            return true;
        else
            return false;

    }

    @Override
    public void present(float deltaTime) {
        GL10 gl = glGraphics.getGL();
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        GLDrawer.draw(pStart);
        GLDrawer.draw(pToChildren);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        GL10 gl = glGraphics.getGL();
        gl.glViewport(0, 0, glGraphics.getWidth(), glGraphics.getHeight());
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrthof(0, w, 0, h, 1, -1);
        gl.glClearColor(1, 1, 1, 0);
        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        pToChildren.reload();
        pStart.reload();
        gl.glMatrixMode(GL10.GL_MODELVIEW);
    }

    @Override
    public void dispose() {
       /* pToChildren.dispose();
        pStart.dispose();*/
    }


}
