package com.keepvisionapp.child.game.screens;


import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import com.keepvisionapp.child.eye_detection.MeasurementDistanceToEyes;
import com.keepvisionapp.child.game.framework.Game;
import com.keepvisionapp.child.game.framework.Graphics;
import com.keepvisionapp.child.game.framework.Input;
import com.keepvisionapp.child.game.framework.Pixmap;
import com.keepvisionapp.child.game.framework.Screen;
import com.keepvisionapp.child.game.framework.impl.AnimationGL;
import com.keepvisionapp.child.game.framework.impl.GLDrawer;
import com.keepvisionapp.child.game.framework.impl.GLGame;
import com.keepvisionapp.child.game.framework.impl.GLGraphics;
import com.keepvisionapp.child.game.framework.impl.PixmapGL;
import com.keepvisionapp.child.game.logic.Assets;
import com.keepvisionapp.child.game.logic.GameMechanic;
import com.keepvisionapp.child.game.logic.Optotype;

/**
 * Created by AbduLlah on 28.03.2015.
 */
public class GameScreen extends Screen implements GameMechanic.IGameMechanicForScreen {

    private final String TAG = "GameScreen";
    public GameMechanic gameMechanic;
    int w, h;
    List<OptotypeOnScreen> optotypesOnScreen = new ArrayList<OptotypeOnScreen>();
    GLGraphics glGraphics;
    Graphics g;

    AnimationGL animLoading, animError, animHand;
    List<AnimationGL> animListSwipeOptotype, animListScoreUp;

    PixmapGL pLoading, pErrorFront, pHand;
    PixmapGL pTxtCheckVision, pTxtMotivation, pTxtDontJerk, pProgressInitUnder, pProgressInitItem;
    PixmapGL pHeart, pTxtScore, pScore, pExplain;
    PixmapGL pTxtPause, pTxtEyeNotDetected, pTxtForth, pTxtCloser, pPhone, pPhoneObv;
    PixmapGL pTxtGameOver;

    PixmapGL[] pNumbs = new PixmapGL[9];
    private Boolean isLoaded = new Boolean(false);

    public GameScreen(Game game) {
        super(game);
        gameMechanic = new GameMechanic(game, this);
        w = ((GLGame)game).frameBufferWidth;
        h = ((GLGame)game).frameBufferHeight;
        g = game.getGraphics();
        glGraphics = ((GLGame) game).getGLGraphics();

        initPixmapsGL();
        initAnimations();
        isLoaded = true;
        synchronized (isLoaded) {
            try {
                isLoaded.notifyAll();
            } catch (Exception e) {

            }
        }

    }

    private void initAnimations() {
        animLoading = new AnimationGL(glGraphics);
        animLoading.create(pLoading, w / 2f, h / 2f);
        animLoading.setRepeat(true);
        animLoading.setRotate(360, 0, 0, PixmapGL.ROTATE_RIGHT);
        animLoading.init(4);
        animLoading.show();

        animError = new AnimationGL(glGraphics);
        animError.create(pErrorFront, w / 2f, h / 2f);
        animError.init(0.05f);
        animError.show();

        animHand = new AnimationGL(glGraphics);
        animHand.create(pHand, 0, 0);
        animHand.setRepeat(true);

        animListScoreUp = new ArrayList<AnimationGL>();
        animListSwipeOptotype = new ArrayList<AnimationGL>();
    }

    private void initPixmapsGL() {
        pLoading = new PixmapGL(glGraphics, g, Assets.loading);
        pErrorFront = new PixmapGL(glGraphics, g, Assets.errorFront);
        pHand = new PixmapGL(glGraphics, g, Assets.hand);

        pTxtCheckVision = new PixmapGL(glGraphics, g, Assets.textCheckVision);
        pTxtMotivation = new PixmapGL(glGraphics, g, Assets.textMotivation);
        pTxtDontJerk = new PixmapGL(glGraphics, g, Assets.textDontJerk);
        pProgressInitUnder = new PixmapGL(glGraphics, g, Assets.progressInitUnder);
        pProgressInitItem = new PixmapGL(glGraphics, g, Assets.progressInitItem);
        pTxtCheckVision.setPosition(w / 2f, h - h / 7f);
        pTxtMotivation.setPosition(w / 2f, h - h / 4f);
        pTxtDontJerk.setPosition(w / 2f, h / 8f);
        pProgressInitUnder.setPosition(w / 2f, h / 2f - Assets.loading.getHeight() - 70);

        pHeart = new PixmapGL(glGraphics, g, Assets.heart);
        pTxtScore = new PixmapGL(glGraphics, g, Assets.textScore);
        pExplain = new PixmapGL(glGraphics, g, Assets.lineForExplain);
        pScore = new PixmapGL(glGraphics, g, g.newPixmapFromText(Integer.toString(gameMechanic.world.score), "exo.otf", w / 17f, android.graphics.Color.BLACK, Graphics.PixmapFormat.ARGB4444));
        pTxtScore.setPosition(w - pTxtScore.getWidth() - pTxtScore.getWidth()/2f, pTxtScore.getHeight());
        pScore.setPosition(pTxtScore.x + pTxtScore.getWidth()/2f + 20, h - pTxtScore.getHeight());

        pTxtPause = new PixmapGL(glGraphics, g, Assets.textPause);
        pTxtEyeNotDetected = new PixmapGL(glGraphics, g, Assets.textEyesNotDetected);
        pTxtForth = new PixmapGL(glGraphics, g, Assets.textForth);
        pTxtCloser = new PixmapGL(glGraphics, g, Assets.textCloser);
        pPhone = new PixmapGL(glGraphics, g, Assets.phone);
        pPhoneObv = new PixmapGL(glGraphics, g, Assets.phone_obv);


        pTxtPause.setPosition(w / 2f, h - h / 4f);
        pTxtEyeNotDetected.setPosition(w / 2f, h / 2f);
        pTxtCloser.setPosition(w / 2f, h - h / 4f);
        pTxtForth.setPosition(w / 2f, h - h / 4f);
        pPhone.setPosition(w / 2f, h / 2f);
        pPhoneObv.setPosition(w / 2f, h / 2f);

        pTxtGameOver = new PixmapGL(glGraphics, g, Assets.textGameOver);
        pTxtGameOver.setPosition(w / 2f, h / 2f);

        for (int i = 0; i < 9; i++) {
            pNumbs[i] = new PixmapGL(glGraphics, g, Assets.numbs[i]);
        }
    }


    @Override
    public void update(float deltaTime) {
        if (gameMechanic.state == GameMechanic.GameState.Init)
            updateInit();
        if (gameMechanic.state == GameMechanic.GameState.Ready)
            updateReady();
        if (gameMechanic.state == GameMechanic.GameState.Running)
            updateRunning(deltaTime);
    }

    private void updateInit() {
    }

    private void updateReady() {
    }

    private void updateRunning(float deltaTime) {
        if (optotypesOnScreen.size() != 0) {
            ArrayList<Input.TouchEvent> touchEvents = (ArrayList<Input.TouchEvent>) game.getInput().getTouchEvents();
            for (int i = 0; i < touchEvents.size(); i++) {
                if (touchEvents.get(i).type == Input.TouchEvent.TOUCH_SWIPE_DOWN
                        || touchEvents.get(i).type == Input.TouchEvent.TOUCH_SWIPE_LEFT
                        || touchEvents.get(i).type == Input.TouchEvent.TOUCH_SWIPE_RIGHT
                        || touchEvents.get(i).type == Input.TouchEvent.TOUCH_SWIPE_UP) {
                    try {
                        gameMechanic.swiped(touchEvents.get(i).type);
                    } catch (Exception e) {}
                }
            }
        }
        gameMechanic.update(deltaTime);
    }

    private boolean inBounds(Input.TouchEvent event, float x, float y, int width, int height) {
        if(event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1)
            return true;
        else
            return false;
    }

    @Override
    public void present(float deltaTime) {
        glGraphics.getGL().glClear(GL10.GL_COLOR_BUFFER_BIT);
        if (gameMechanic.state == GameMechanic.GameState.Init)
            presentInit(deltaTime);
        if (gameMechanic.state == GameMechanic.GameState.Ready)
            presentReady();
        if (gameMechanic.state == GameMechanic.GameState.Running)
            presentRunning(deltaTime);
        if (gameMechanic.state == GameMechanic.GameState.GameOver)
            presentGameOver();
        if (gameMechanic.state == GameMechanic.GameState.Paused)
            presentPause();
    }

    private void presentInit(float deltaTime) {
        animLoading.draw(deltaTime);
        GLDrawer.draw(pTxtCheckVision);
        GLDrawer.draw(pTxtMotivation);
        GLDrawer.draw(pTxtDontJerk);
        GLDrawer.draw(pProgressInitUnder);
        for (int i = 0; i < gameMechanic.progressOfInit; i++) {
            pProgressInitItem.setPosition(pProgressInitUnder.x - (pProgressInitUnder.getWidth() / 2f) + pProgressInitItem.getWidth() / 2f + 10 + i * pProgressInitItem.getWidth(), pProgressInitUnder.y);
            GLDrawer.draw(pProgressInitItem);
        }
    }


    private void presentReady() {
    }

    private void presentRunning(float deltaTime) {
        for (int i = 0; i < optotypesOnScreen.size(); i++) {
            optotypesOnScreen.get(i).setPosition(gameMechanic.world.optotypeList.get(i).vector.x, gameMechanic.world.optotypeList.get(i).vector.y);
            GLDrawer.draw(optotypesOnScreen.get(i));
        }

        animError.draw(deltaTime);
        for (int i = 0; i < animListScoreUp.size(); i++) {
            animListScoreUp.get(i).draw(deltaTime);
        }
        for (int i = 0; i < animListSwipeOptotype.size(); i++) {
            animListSwipeOptotype.get(i).draw(deltaTime);
        }
        if (gameMechanic.isBoss)
            for (int i = 0; i < gameMechanic.world.health; i++) {
                pHeart.setPosition(Assets.heart.getWidth()/2f + Assets.heart.getWidth() * 1.3f * i, Assets.heart.getHeight());
                GLDrawer.draw(pHeart);
            }
        GLDrawer.draw(pTxtScore);
        GLDrawer.draw(pScore);

        if (gameMechanic.world.countOptotype == 1) {
            if (!animHand.isShow) {
                switch (gameMechanic.world.optotypeList.get(0).state) {
                    case Optotype.RIGHT:
                        pExplain.setPosition(w / 2f, h / 5f);
                        animHand.create(pHand, pExplain.x - (pExplain.getWidth()/2f), pExplain.y - (pHand.getHeight()/2f));
                        animHand.setPosition(pHand.x + pExplain.getWidth(), pExplain.y - (pHand.getHeight()/2f));
                        break;
                    case Optotype.LEFT:
                        pExplain.setPosition(w / 2f, h / 5f);
                        pExplain.setRotation(180, PixmapGL.ROTATE_RIGHT);
                        animHand.create(pHand, pExplain.x + (pExplain.getWidth()/2f), pExplain.y - (pHand.getHeight()/2f));
                        animHand.setPosition(pHand.x - pExplain.getWidth(), pExplain.y - (pHand.getHeight()/2f));
                        break;
                    case Optotype.UP:
                        pExplain.setPosition(w / 2f, h / 5f);
                        pExplain.setRotation(90, PixmapGL.ROTATE_LEFT);
                        animHand.create(pHand, pExplain.x + 15, pExplain.y - (pExplain.getWidth()/2f) - (pHand.getHeight()/2f));
                        animHand.setPosition(pExplain.x + 15, pHand.y + pExplain.getWidth());
                        break;
                    case Optotype.DOWN:
                        pExplain.setPosition(w / 2f, h / 5f);
                        pExplain.setRotation(90, PixmapGL.ROTATE_RIGHT);
                        animHand.create(pHand, pExplain.x + 15, pExplain.y + (pExplain.getWidth()/2f) - (pHand.getHeight()/2f));
                        animHand.setPosition(pExplain.x + 15, pHand.y - pExplain.getWidth());
                        break;
                }
                animHand.init(0.8f);
                animHand.show();
            }
            GLDrawer.draw(pExplain);
            animHand.draw(deltaTime);
        }
    }

    private void presentPause() {
        if (gameMechanic.deltaDistance == MeasurementDistanceToEyes.ERROR_EYES_DETECTION) {
            GLDrawer.draw(pTxtPause);
            GLDrawer.draw(pTxtEyeNotDetected);
        } else {
            if (gameMechanic.deltaDistance > 0)
                GLDrawer.draw(pTxtForth);
            else
                GLDrawer.draw(pTxtCloser);
            GLDrawer.draw(pPhoneObv);
            pPhone.setScaling(1 + gameMechanic.deltaDistance / 10, 1 + gameMechanic.deltaDistance / 10, 0);
            GLDrawer.draw(pPhone);
        }
    }

    private float getXOfRedLineAdapt() {
        return gameMechanic.deltaDistance * 10 + w/2f;
    }

    private void presentGameOver() {
        GLDrawer.draw(pTxtGameOver);
    }

    @Override
    public void pause() {
        gameMechanic.pauseGame();
    }

    @Override
    public void resume() {
        if (!isLoaded) {
            synchronized (isLoaded){
                try {
                    isLoaded.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        GL10 gl = glGraphics.getGL();

        gl.glClearColor(1, 1, 1, 0);
        gl.glViewport(0, 0, w, h);
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrthof(0, w, 0, h, 1, -1);
        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        for (int i = 0; i < optotypesOnScreen.size(); i++) {
            optotypesOnScreen.get(i).reload();
        }
        pTxtDontJerk.reload();
        pScore.reload();
        pExplain.reload();
        pTxtScore.reload();
        pTxtCheckVision.reload();
        pTxtPause.reload();
        pTxtEyeNotDetected.reload();
        pTxtForth.reload();
        pTxtCloser.reload();
        pTxtGameOver.reload();
        pTxtGameOver.reload();
        pProgressInitItem.reload();
        pProgressInitUnder.reload();
        pTxtMotivation.reload();
        pLoading.reload();
        pErrorFront.reload();
        pHeart.reload();
        pHand.reload();
        pPhone.reload();
        pPhoneObv.reload();
        for (int i = 0; i < pNumbs.length; i++) {
            pNumbs[i].reload();
        }

        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gameMechanic.resumeGame();
    }

    @Override
    public void dispose() {
        gameMechanic.disposeGame();
        for (int i = 0; i < optotypesOnScreen.size(); i++) {
            optotypesOnScreen.get(i).dispose();
        }
        pScore.dispose();
        pExplain.dispose();
        pTxtScore.dispose();
        pTxtCheckVision.dispose();
        pTxtPause.dispose();
        pTxtEyeNotDetected.dispose();
        pTxtForth.dispose();
        pTxtCloser.dispose();
        pTxtGameOver.dispose();
        pTxtGameOver.dispose();
        pTxtDontJerk.dispose();
        pTxtPause.dispose();
        pTxtMotivation.dispose();
        pLoading.dispose();
        pErrorFront.dispose();
        pHeart.dispose();
        pHand.dispose();
        pPhone.dispose();
        pPhoneObv.dispose();
        for (int i = 0; i < pNumbs.length; i++) {
            pNumbs[i].dispose();
        }

        optotypesOnScreen = null;
        pScore = null;
        pExplain = null;
        pTxtScore = null;
        pTxtCheckVision = null;
        pTxtPause = null;
        pTxtEyeNotDetected = null;
        pTxtForth = null;
        pTxtCloser = null;
        pTxtGameOver = null;
        pTxtGameOver = null;
        pTxtDontJerk = null;
        pTxtPause = null;
        pTxtMotivation = null;
        pLoading = null;
        pErrorFront = null;
        pHeart = null;
        pHand = null;
        pPhone = null;
        pPhoneObv = null;
        for (int i = 0; i < pNumbs.length; i++) {
            pNumbs[i] = null;
        }

        gameMechanic = null;
        g = null;
        glGraphics = null;

        animLoading = null;
        animError = null;
        animHand = null;
        animListSwipeOptotype = null;
        animListScoreUp = null;

        Assets.loading.dispose();
        Assets.errorFront.dispose();
        Assets.buttonStartActive.dispose();
        Assets.buttonStartNotActive.dispose();
        Assets.heart.dispose();
        Assets.textScore.dispose();
        Assets.textGameOver.dispose();
        Assets.textPause.dispose();
        Assets.textDontJerk.dispose();
        Assets.textMotivation.dispose();
        Assets.lineForExplain.dispose();
        Assets.textCheckVision.dispose();
        Assets.textEyesNotDetected.dispose();
        Assets.textCloser.dispose();
        Assets.textForth.dispose();
        Assets.progressInitItem.dispose();
        Assets.progressInitUnder.dispose();
        Assets.hand.dispose();
        Assets.phone.dispose();
        Assets.phone_obv.dispose();
    }

    @Override
    public void drawErrorSwipe() {
        animError.show();
    }

    @Override
    public void drawSuccesSwipe(int score, int swipeSide, int index) {
        PixmapGL pixmapScore = new PixmapGL(glGraphics, g, g.newPixmapFromText("+" + Integer.toString(score), "exo.otf", (int) (w/12f), android.graphics.Color.GREEN, Graphics.PixmapFormat.ARGB4444));
        final AnimationGL animScoreUp = new AnimationGL(glGraphics);
        float x = optotypesOnScreen.get(0).x;
        float y = optotypesOnScreen.get(0).y;
        animScoreUp.create(pixmapScore, x, y);
        //animScoreUp.create(pNumbs[score], x, y);
        animScoreUp.setPosition(x, y + (int) (w / 10f));
        animScoreUp.setIAnimOnStop(new AnimationGL.IAnimOnStop() {
            @Override
            public void onStop(PixmapGL pixmapGL) {
                animListScoreUp.remove(animScoreUp);
                pixmapGL.dispose();
            }
        });
        animScoreUp.init(0.2f);
        animScoreUp.show();
        animListScoreUp.add(animScoreUp);

        final AnimationGL animSwipeOptotype = new AnimationGL(glGraphics);
        animSwipeOptotype.create(optotypesOnScreen.get(0), x, y);
        animSwipeOptotype.setAlpha(1);

        animSwipeOptotype.setIAnimOnStop(new AnimationGL.IAnimOnStop() {
            @Override
            public void onStop(PixmapGL pixmapGL) {
                animListSwipeOptotype.remove(animSwipeOptotype);
                pixmapGL.dispose();
                pixmapGL = null;
            }
        });
        if (swipeSide == Optotype.RIGHT)
            animSwipeOptotype.setPosition(x + (int) (w / 7f), y);
        if (swipeSide == Optotype.LEFT)
            animSwipeOptotype.setPosition(x - (int) (w / 7f), y);
        if (swipeSide == Optotype.UP)
            animSwipeOptotype.setPosition(x, y + (int) (w / 7f));
        if (swipeSide == Optotype.DOWN)
            animSwipeOptotype.setPosition(x, y - (int) (w / 7f));
        animSwipeOptotype.init(0.15f);
        animSwipeOptotype.show();
        animListSwipeOptotype.add(animSwipeOptotype);

        optotypesOnScreen.remove(0);
        if (pScore.texture != null)
            pScore.dispose();
        pScore = new PixmapGL(glGraphics, g, g.newPixmapFromText(Integer.toString(gameMechanic.world.score), "exo.otf", w / 17f, android.graphics.Color.BLACK, Graphics.PixmapFormat.ARGB4444));
        pScore.setPosition(pTxtScore.x + pTxtScore.getWidth()/2f + 20, pTxtScore.getHeight());
    }

    @Override
    public void createNewOptotypePixmap(Optotype optotype) {
        Pixmap pOptotype = g.newPixmap("optotype.png", Graphics.PixmapFormat.ARGB8888);
        OptotypeOnScreen optotypeOnScreen = new OptotypeOnScreen(glGraphics, pOptotype);
        optotypeOnScreen.index = optotype.index;
        optotypeOnScreen.setScaling(optotype.size / pOptotype.getWidth(),optotype.size / pOptotype.getWidth(), optotype.size / pOptotype.getWidth());
        optotypeOnScreen.setPosition(optotype.vector.x, optotype.vector.y);
        switch (optotype.state) {
            case Optotype.RIGHT:
                optotypeOnScreen.setRotation(90, PixmapGL.ROTATE_LEFT);
                break;
            case Optotype.LEFT:
                optotypeOnScreen.setRotation(90, PixmapGL.ROTATE_RIGHT);
                break;
            case Optotype.UP:
                optotypeOnScreen.setRotation(180, PixmapGL.ROTATE_LEFT);
                break;
            case Optotype.DOWN:
                break;
        }
        gameMechanic.world.optotypeList.add(optotype);
        optotypesOnScreen.add(optotypeOnScreen);

        if (animHand.isShow && gameMechanic.world.countOptotype != 1) {
            animHand.stop();
        }
    }

    @Override
    public void failRemoveOptotypePixmap() {
        drawErrorSwipe();
        optotypesOnScreen.remove(0).dispose();
    }



    class OptotypeOnScreen extends PixmapGL {
        public int index;

        public OptotypeOnScreen(GLGraphics glGraphics, Pixmap pixmap) {
            super(glGraphics, g, pixmap);
        }
    }
}
