package com.keepvisionapp.child.game.logic;

import com.keepvisionapp.child.game.framework.Pixmap;

/**
 * Created by AbduLlah on 30.03.2015.
 */
public class Assets {
    public static final String FONT_EXO = "exo.otf";
    public static Pixmap loading;
    public static Pixmap errorFront;
    public static Pixmap textPause, textEyesNotDetected;
    public static Pixmap buttonStartNotActive, buttonStartActive;
    public static Pixmap textDontJerk, textMotivation, textCloser, textForth;
    public static Pixmap optionsStart;
    public static Pixmap optionChildList;
    public static Pixmap heart;
    public static Pixmap textScore;
    public static Pixmap textGameOver;
    public static Pixmap textCheckVision;
    public static Pixmap lineForExplain;
    public static Pixmap progressInitUnder;
    public static Pixmap progressInitItem;
    public static Pixmap hand;
    public static Pixmap phone;
    public static Pixmap phone_obv;
    public static Pixmap[] numbs = new Pixmap[9];
}
