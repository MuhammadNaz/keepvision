package com.keepvisionapp.child.eye_detection;

import com.keepvisionapp.child.R;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.*;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.List;

public class HideCameraTracking implements SurfaceHolder.Callback {

    private final String TAG = "HideCameraTracking";

	private SurfaceHolder mHolder;
	private Camera mCamera;
	private final Context context;
	
	private WindowManager wm;
	private FrameLayout sv_layout;
    private FrameLayout paint_layout;
	public Size previewSize;
	private WindowManager.LayoutParams paramsOfWindowView;
	public int rotate;
    private IntentFilter ifListenOrientationChange;
    private CameraHandlerThread cameraOpenThread = null; //in order to recieve data in previewcallback from another thread
    private EyeTrackingSurfaceView eyeTrackingSurfaceView;
    private PreviewCallback listener;
    private volatile boolean isInit;
    private SurfaceView sv;
    private ICallbackFromCameraState iCallbackFromCameraState;

	public HideCameraTracking(final Context context, final PreviewCallback listener) {
        this.context = context;
        this.listener = listener;
        ((MeasurementDistanceToEyes) listener).setIFaceData(eyeTrackingSurfaceView);
        if (Thread.currentThread().getName().equals(context.getApplicationContext().getMainLooper().getThread().getName()))
            init();
        else {
            Runnable runUiThread = new Runnable() {
                @Override
                public void run() {
                    init();
                }
            };
            Handler handler = new Handler(context.getApplicationContext().getMainLooper());
            handler.post(runUiThread);
        }
	}

    public void setICallbackFromCameraState(ICallbackFromCameraState iCallbackFromCameraState) {
        this.iCallbackFromCameraState = iCallbackFromCameraState;
    }

    public interface ICallbackFromCameraState {
        public void begun();
        public void stopped();
    }

    private void init() {
        paramsOfWindowView = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, PixelFormat.TRANSPARENT);
        wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) context.getApplicationContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.sv_layout, null);
        View paint_layout_view =  inflater.inflate(R.layout.paint_layout, null);
        sv_layout = (FrameLayout) view.findViewById(R.id.sv_layout);
        paint_layout = (FrameLayout) paint_layout_view.findViewById(R.id.paint_layout);
        //eyeTrackingSurfaceView = (EyeTrackingSurfaceView) paint_layout_view.findViewById(R.id.paint_layout);
        //eyeTrackingSurfaceView.setZOrderOnTop(true);
        sv = (SurfaceView) view.findViewById(R.id.surf_view);
        sv.setZOrderOnTop(true);
        mHolder = sv.getHolder();
        //create and start broadcastReceiver for listen orientation change
        ifListenOrientationChange = new IntentFilter();
        ifListenOrientationChange.addAction(Constants.ACTION_ORIENTATION_CHANGE);
        isInit = true;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Utils.log(TAG,"in surfaceDestroyed");
        if (mCamera == null)
            return;
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
        Log.d("TrackingFlow", "Surface destroyed");
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Utils.log(TAG,"in surfaceCreated");
        try {
            newOpenCamera();
            Utils.log(TAG,"thread in surfaceCreated = " + Thread.currentThread().getName());
            mCamera.setPreviewDisplay(holder);
            setRightRotate();
            mCamera.setDisplayOrientation(rotate);
            Parameters parameters = mCamera.getParameters();
            previewSize = parameters.getPreviewSize();
            //settigMinimalPreviewSize(parameters.getSupportedPreviewSizes());
            // previewSize.width = wm.getDefaultDisplay().getHeight();
            // previewSize.height = wm.getDefaultDisplay().getWidth();
            //previewSize = getHighPreviewSize(parameters.getSupportedPreviewSizes(), wm.getDefaultDisplay().getWidth(), wm.getDefaultDisplay().getHeight());
            previewSize.width = 640;
            previewSize.height = 480;
            parameters.setPreviewSize(previewSize.width, previewSize.height);
            parameters.setPictureSize(previewSize.width, previewSize.height);
            if (parameters.getMeteringAreas() != null && parameters.getMeteringAreas().size() != 0) {
                for (int i = 0; i < parameters.getMeteringAreas().size(); i++) {
                    Utils.log(TAG, "supp metearing areas = " + parameters.getMeteringAreas().get(i).rect.toString() + "\n");
                }
            } else Utils.log(TAG, "NOT SUPP METEARING AREAS\n");
            if (parameters.getFocusAreas() != null && parameters.getFocusAreas().size() != 0) {
                for (int i = 0; i < parameters.getFocusAreas().size(); i++) {
                    Utils.log(TAG, "supp focus areas = " + parameters.getFocusAreas().get(i).rect.toString() + "\n");
                }
            } else Utils.log(TAG, "NOT SUPP FOCUS AREAS\n");
            if (parameters.getSupportedFlashModes() != null && parameters.getSupportedFlashModes().size() != 0) {
                for (int i = 0; i < parameters.getSupportedFlashModes().size(); i++) {
                    Utils.log(TAG, "supp flash modes = " + parameters.getSupportedFlashModes().get(i) + "\n");
                }
                Utils.log(TAG, "current flash mode = " + parameters.getFlashMode()+ "\n");
            } else Utils.log(TAG, "NOT SUPP FLASH MODES\n");
            if (parameters.getSupportedFocusModes() != null && parameters.getSupportedFocusModes().size() != 0) {
                for (int i = 0; i < parameters.getSupportedFocusModes().size(); i++) {
                    Utils.log(TAG, "supp focus modes = " + parameters.getSupportedFocusModes().get(i) + "\n");
                }
                Utils.log(TAG, "current focus mode = " + parameters.getFocusMode() + "\n");
            } else Utils.log(TAG, "NOT SUPP FOCUS MODES\n");
            if (parameters.getSupportedSceneModes() != null && parameters.getSupportedSceneModes().size() != 0) {
                for (int i = 0; i < parameters.getSupportedSceneModes().size(); i++) {
                    Utils.log(TAG, "supp scene modes = " + parameters.getSupportedSceneModes().get(i) + "\n");
                }
                Utils.log(TAG, "current scene mode = " + parameters.getSceneMode() + "\n");
            } else Utils.log(TAG, "NOT SUPP SCENE MODES\n");
            if (parameters.getSupportedPictureSizes() != null && parameters.getSupportedPictureSizes().size() != 0) {
                for (int i = 0; i < parameters.getSupportedPictureSizes().size(); i++) {
                    Utils.log(TAG, "supp picture size w = " + parameters.getSupportedPictureSizes().get(i).width + " h = " + parameters.getSupportedPictureSizes().get(i).height + "\n");
                }
                Utils.log(TAG, "current picture size w = " + parameters.getPictureSize().width + " h =" + parameters.getPictureSize().height + "\n");
            } else Utils.log(TAG, "NOT SUPP PICTURE SIZES\n");
            Utils.log(TAG, "Zoom = " + parameters.getZoom() + "\n");
            Utils.log(TAG, "flatten = " + parameters.flatten() + "\n");
            Utils.log(TAG, "FocalLength = " + parameters.getFocalLength() + "\n\n\n");

            Utils.log(TAG,"previewSize = " + previewSize.width + "x" + previewSize.height);

            parameters.setPictureSize(previewSize.width, previewSize.height);
            mCamera.setParameters(parameters);
            mCamera.setPreviewCallback(listener);
            mCamera.startPreview();
            if (iCallbackFromCameraState != null)
                iCallbackFromCameraState.begun();
        }
        catch (Exception e) {
            Utils.log(TAG,"Exception in surfaceCreated " + e.getMessage());
            if (mCamera == null)
                return;
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        Utils.log(TAG,"surfaceChanged called");
    }

    private void startMeasureInUiThread() {
        Utils.log(TAG, "in startMeasure");
        startListenOrientationChange();
        wm.addView(sv_layout, paramsOfWindowView);
        // wm.addView(paint_layout, paramsOfWindowView);
        mHolder.addCallback(this);
    }

	public void startMeasure() {
        while(!isInit){
            Thread.yield();
        };
        if (Thread.currentThread().getName().equals(context.getApplicationContext().getMainLooper().getThread().getName()))
            startMeasureInUiThread();
        else {
            Runnable runUiThread = new Runnable() {
                @Override
                public void run() {
                    startMeasureInUiThread();
                }
            };
            Handler handler = new Handler(context.getApplicationContext().getMainLooper());
            handler.post(runUiThread);
        }
	}

    private void stopMeasureInUiThread() {
        Utils.log(TAG, "in stopMeasure");
        if (mCamera == null)
            return;
        stopListenOrientationChange();
        mCamera.setPreviewCallback(null);
        wm.removeView(sv_layout);
        //wm.removeView(paint_layout);
        mHolder.removeCallback(this);
        mCamera.stopPreview();
        mCamera.release();
        if (iCallbackFromCameraState != null)
            iCallbackFromCameraState.stopped();
        mCamera = null;
    }
	
	public void stopMeasure() {
        if (Thread.currentThread().getName().equals(context.getApplicationContext().getMainLooper().getThread().getName()))
            stopMeasureInUiThread();
        else {
            Runnable runUiThread = new Runnable() {
                @Override
                public void run() {
                    stopMeasureInUiThread();
                }
            };
            Handler handler = new Handler(context.getApplicationContext().getMainLooper());
            handler.post(runUiThread);
        }
    }

    private void settigMinimalPreviewSize(List<Camera.Size> sizes) {
        double deviceRatio = (double) context.getResources().getDisplayMetrics().widthPixels
                / (double) context.getResources().getDisplayMetrics().heightPixels;

        Size bestSize = sizes.get(0);
        double bestRation = (double) bestSize.width / (double) bestSize.height;

        for (Size size : sizes) {
            double sizeRatio = (double) size.width / (double) size.height;

            if (Math.abs(deviceRatio - bestRation) > Math.abs(deviceRatio
                    - sizeRatio)) {
                bestSize = size;
                bestRation = sizeRatio;
            }
        }
        previewSize.height = bestSize.height;
        previewSize.width = bestSize.width;
    }

    private Camera.Size getHighPreviewSize(List<Camera.Size> sizes, int w, int h) {

        final double ASPECT_TOLERANCE = 0.2;
        double targetRatio = (double) w / h;
        if (sizes == null)
            return null;
        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;
        int targetHeight = h;
        for (Camera.Size size : sizes)
        {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff)
            {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null)
        {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff)
                {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }


    private class NoFrontCamera extends Exception {

		public NoFrontCamera() {
            ServiceHandler serviceHandler = new ServiceHandler(Looper.getMainLooper());
            serviceHandler.sendEmptyMessage(0);
        }

        private class ServiceHandler extends Handler {
            public ServiceHandler(Looper looper) {
                super(looper);
            }
            @Override
            public void handleMessage(Message msg) {
                Toast.makeText(context, "You have no front camera", Toast.LENGTH_LONG).show();
            }
        }

    }

    private void setRightRotate() {
        int rotation = wm.getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0: rotate = 90; break;
            case Surface.ROTATION_90: rotate = 0; break;
            case Surface.ROTATION_180: rotate = 270; break;
            case Surface.ROTATION_270: rotate = 180; break;
        }
        Utils.log(TAG, "rotate = " + rotate);
    }

    public void startListenOrientationChange() {
        context.registerReceiver(mBR_OrientationChange, ifListenOrientationChange);
    }

    public void stopListenOrientationChange() {
        context.unregisterReceiver(mBR_OrientationChange);
    }

    public BroadcastReceiver mBR_OrientationChange = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent myIntent) {

            if ( myIntent.getAction().equals( Constants.ACTION_ORIENTATION_CHANGE ) ) {
                Utils.log(TAG,"received->" + Constants.ACTION_ORIENTATION_CHANGE);
                if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                    // it's Landscape
                    Utils.log(TAG, "LANDSCAPE");
                }
                else {
                    Utils.log(TAG, "PORTRAIT");
                }
                stopMeasure();
                startMeasure();
            }
        }
    };
    private class CameraHandlerThread extends HandlerThread {
        Handler mHandler = null;

        CameraHandlerThread() {
            super("CameraHandlerThread");
            start();
            mHandler = new Handler(getLooper());
        }

        synchronized void notifyCameraOpened() {
            notify();
        }

        void openCamera() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    oldOpenCamera();
                    notifyCameraOpened();
                }
            });
            try {
                wait();
            }
            catch (InterruptedException e) {
                Utils.log(TAG,"WAIT wait was interrupted");
            }
        }
    }

    private void oldOpenCamera() {
        try {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            int cameraId = -1;
            int camerasCount = Camera.getNumberOfCameras();
            for (int camIndex = 0; camIndex < camerasCount; camIndex++) {
                Camera.getCameraInfo(camIndex, cameraInfo);
                if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    cameraId = camIndex;
                    break;
                }
            }
            if (cameraId == -1) {
                throw new NoFrontCamera();
            }
            mCamera = Camera.open(cameraId);
        } catch (NoFrontCamera e) {
            Toast.makeText(context, "You have no front camera", Toast.LENGTH_SHORT).show();
            if (mCamera == null)
                return;
            mCamera.release();
            mCamera = null;
        }
        catch (Exception e) {
            Utils.log(TAG,"OPEN failed to open front camera");
        }
    }

    private void newOpenCamera() {
        if (cameraOpenThread == null) {
            cameraOpenThread = new CameraHandlerThread();
        }

        synchronized (cameraOpenThread) {
            cameraOpenThread.openCamera();
        }
    }
}
