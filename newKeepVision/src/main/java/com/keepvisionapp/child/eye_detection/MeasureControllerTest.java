package com.keepvisionapp.child.eye_detection;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import com.keepvisionapp.child.tools.Utils;

public class MeasureControllerTest extends MeasureController {

    public interface IResultMeasurementForTest {
        void getResultMeasurement(ResultMeasurementTest resultMeasurement, float per);
    }

    public final String TAG = "EyeTest";

    public enum ResultMeasurementTest {
        PROCESSING, INIT_TEST_SYSTEM, GOOD, BAD, ERROR;
    }

    private List<Float> bufferValuesForTesting = new ArrayList<Float>();
    public boolean isCaching;
    private IResultMeasurementForTest iResultMeasurement;
    private boolean isFirstTracking;
    private float firstResult, secondResult;

    public MeasureControllerTest(Context context, IResultMeasurementForTest iResultMeasurement) {
        super(context);
        this.iResultMeasurement = iResultMeasurement;
    }

    public void startMeasure(boolean isFirstTracking){
        this.isCaching = true;
        this.isFirstTracking = isFirstTracking;
        bufferValuesForTesting.clear();
        measurementDistanceToEyes.startMeasure();
        Utils.log(TAG, "startMeasure isFirstTracking = " + isFirstTracking);
    }

    public void stopMeasure() {
        isCaching = false;
        Utils.log(TAG, "stopMeasure");
        bufferValuesForTesting.clear();
        measurementDistanceToEyes.stopMeasure();
    }

    @Override
    public void getDistanceToDevice(float distance) {
        if (distance == MeasurementDistanceToEyes.ERROR_EYES_DETECTION) {
            Utils.log(TAG, "face not detected");
            iResultMeasurement.getResultMeasurement(ResultMeasurementTest.ERROR, 0);
        } else {
            iResultMeasurement.getResultMeasurement(ResultMeasurementTest.PROCESSING, distance);
            if (isCaching) {
                if (bufferValuesForTesting.size() < 10) {
                    for (int i = 0; i < bufferValuesForTesting.size(); i++)
                        if (Math.abs(distance - bufferValuesForTesting.get(i)) > 2) {
                            bufferValuesForTesting.clear();
                            Utils.log(TAG, "clearBuffer");
                            return;
                        }
                    bufferValuesForTesting.add(distance);
                } else
                    calculateResult();
            }
            else {
                Utils.log(TAG, "isCaching false");
            }
        }
    }

    private void calculateResult() {
        Utils.log(TAG, "in calculateResult");
        isCaching = false;
        ResultMeasurementTest resultMeasurement;
        float per;
        float sum = 0;
        for (int i = 0; i < bufferValuesForTesting.size(); i++)
            sum +=bufferValuesForTesting.get(i);
        float averageDistance = sum/bufferValuesForTesting.size();
        if (isFirstTracking) {
            firstResult = averageDistance;
            iResultMeasurement.getResultMeasurement(ResultMeasurementTest.INIT_TEST_SYSTEM, firstResult);
        }
        else {
            measurementDistanceToEyes.stopMeasure();
            secondResult = averageDistance;
            if (Math.abs(secondResult - firstResult) < 3) {
                resultMeasurement = ResultMeasurementTest.GOOD;
                per = 100;
            }
            else {
                resultMeasurement = ResultMeasurementTest.BAD;
                float value = firstResult < secondResult ? secondResult : firstResult;
                per = 100 - (Math.abs(firstResult - secondResult) * 100f)/value;
            }
            iResultMeasurement.getResultMeasurement(resultMeasurement, per);
            Utils.log(TAG, "firstResult = " + firstResult + " secondResult = " + secondResult + " per = " + per);
        }
    }
}

