package com.keepvisionapp.child.eye_detection;

import java.util.ArrayList;
import java.util.List;

public class DataEyeDistance {

	private List<Float> distanceEyes = new ArrayList<Float>(); //all distances from traking for finding averageDistance
    private float averageDistance; //distance is operated of system
    public final static int ERROR_VALUE = -1;
    public final static int RATIO_FOR_DISTANCE = 4000;
	public float getAverageDistanceEyes() {
		if (distanceEyes.size() == 0)
			return ERROR_VALUE; //wrong during take averageEyeDistance;
		float sum = 0;
		for (float x : distanceEyes) {
            sum += x;
        }
		averageDistance = sum / distanceEyes.size();
		return averageDistance;
	}
	
	public void addDistance(float eyeDistance) {
		distanceEyes.add(eyeDistance);
	}

	public void setAverageDistance(float averageEyeDistance) {
		this.averageDistance = averageEyeDistance;
	}

	public int size() {
		return distanceEyes.size();
	}
	
	public void removeFirst() {
		if (distanceEyes.size() > 0)
			distanceEyes.remove(0);
	}

    public float getLast() {
        if (distanceEyes.size() == 0)
            return 0;
        return distanceEyes.get(distanceEyes.size() - 1);
    }
}
