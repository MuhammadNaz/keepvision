package com.keepvisionapp.child.eye_detection;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

import com.keepvisionapp.child.R;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.Utils;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Size;




public class MeasurementDistanceToEyes implements Camera.PreviewCallback/*, FREngine.InitFinishedCallback, FREngine.FrameProcessedCallback*/ {

    public interface IMeasurement {
		public void getDistanceToDevice(float distance);
	}

    public static final int ERROR_EYES_DETECTION = 1000000;

    public interface IFaceData {
        public void getFaceCoords(org.opencv.core.Rect face);
        public void getEyeCoords(org.opencv.core.Rect eye_left, org.opencv.core.Rect eye_right);
        public void getIrisCoords(Point iris_left, Point iris_right);
    }

	private DataEyeDistance measureData;
	private HideCameraTracking eyeDetection;
	public IMeasurement iMeasurment;
    private IFaceData iFaceData;
	private float currentAvgDistance = -1;

	private Context context;

    public volatile boolean isMeasuring = false;
    private Size previewSize;
    private DetectController detectController;
    private List<Float> listAvgDistanceEyes = new ArrayList<Float>();


    public MeasurementDistanceToEyes(final Context context, final IMeasurement iMeasurement) {
		measureData = new DataEyeDistance();
		this.iMeasurment = iMeasurement;
		this.context = context;
        detectController = new DetectController();
        //FREngine.getInstance().addInitFinishedCallback(this);
        //
        eyeDetection = new HideCameraTracking(context, this);
	}

    public void setIFaceData(IFaceData iFaceData) {
        this.iFaceData = iFaceData;
    }

	public void startMeasure() {
        /*(FREngine.getInstance()).addInitFinishedCallback(this);
        if (FREngine.getInstance().getStatus() != FREngine.ENGINE_STATUS_INITING) {
            if (FREngine.getInstance().getStatus() != FREngine.ENGINE_STATUS_RUNNING) {
                FREngine.getInstance().startInit();
                Utils.log("startInit FREngine");
            }
        }*/
        if (!isMeasuring) {
            eyeDetection.startMeasure();
            isMeasuring = true;
        }
	}

    public void setICallbackFromCamera(HideCameraTracking.ICallbackFromCameraState iCallbackFromCamera) {
        if (eyeDetection != null)
            eyeDetection.setICallbackFromCameraState(iCallbackFromCamera);
    }
	
	public void stopMeasure() {
        /*if (FREngine.getInstance().getStatus() != FREngine.ENGINE_STATUS_DEINITING) {
           if (FREngine.getInstance().getStatus() == FREngine.ENGINE_STATUS_RUNNING || FREngine.getInstance().getStatus() == FREngine.ENGINE_STATUS_PAUSED) {
               FREngine.getInstance().deinit();
               Utils.log("deinit FREngine");
            }
        }*/
        if (isMeasuring) {
            isMeasuring = false;
            eyeDetection.stopMeasure();
        }
    }

    private void rotationMat(Mat mat, int rotation){
        switch (rotation) {
            case 90:  Core.flip(mat.t(), mat, 0); break;
            case 180: Core.flip(mat.t(), mat, 0); Core.flip(mat.t(), mat, 0); Core.flip(mat.t(), mat, 0); break;
            case 270: Core.flip(mat.t(), mat, 1); Core.flip(mat.t(), mat, 1); break;
        }
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        //convert data to mat
        try {
            Mat mat = new Mat(eyeDetection.previewSize.height, eyeDetection.previewSize.width, CvType.CV_8UC1);
            mat.put(0, 0, data);
            //rotattion mat
            if (eyeDetection.rotate == 0) {
                Utils.log("Менять ничего не нужно");
            }
            else {
                switch (eyeDetection.rotate) {
                    case 90:  Core.flip(mat.t(), mat, 0); break;
                    case 180: Core.flip(mat.t(), mat, 0); Core.flip(mat.t(), mat, 0); Core.flip(mat.t(), mat, 0); break;
                    case 270: Core.flip(mat.t(), mat, 1); Core.flip(mat.t(), mat, 1); break;
                }
            }
                //rotationMat(mat, eyeDetection.getRotate());
            //get distance between eyes
            float eyeDistance = detectController.getDistanceEyes(mat);
            if (eyeDistance != ERROR_EYES_DETECTION) {
                float lastAtListDistance = measureData.getLast();
                measureData.addDistance((eyeDistance + lastAtListDistance)/2);
                measureData.addDistance(eyeDistance);
                while (measureData.size() > Constants.COUNT_FRAMES_THREASHOLD)
                    measureData.removeFirst();
                currentAvgDistance = measureData.getAverageDistanceEyes();
                listAvgDistanceEyes.add(currentAvgDistance);
                if (listAvgDistanceEyes.size() > Constants.COUNT_FRAMES_THREASHOLD)
                    listAvgDistanceEyes.remove(0);
                if (currentAvgDistance != DataEyeDistance.ERROR_VALUE)
                    iMeasurment.getDistanceToDevice(DataEyeDistance.RATIO_FOR_DISTANCE/getAvgAvgDistance());
            }
            else {
                Utils.log("eyeDistance = null");
                iMeasurment.getDistanceToDevice(ERROR_EYES_DETECTION);
            }

        } catch (Throwable e) {
            Utils.log("error in previewFrame");
        }
    }

   /* @Override
    public void onInitFinished(String[] p0) {
        FREngine.getInstance().resume();
        Utils.log("init FREngine finished");
    }

    @Override
    public void onFrameProcessed(long frameId, Face[] faces) {

    }*/

    private float getAvgAvgDistance() {
        if (listAvgDistanceEyes.size() == 0)
            return DataEyeDistance.ERROR_VALUE; //wrong during take averageEyeDistance;
        float sum = 0;
        for (float x : listAvgDistanceEyes) {
            sum += x;
        }
        return sum / listAvgDistanceEyes.size();
    }

    /*class DetectThread implements Runnable {
        private Mat mat;
        public DetectThread(Mat mat) {
            this.mat = mat;
        }
        @Override
        public void run() {

            //rotattion mat
            if (eyeDetection.getRotate() == 0) {
                Utils.log("Менять ничего не нужно");
            }
            else
                rotationMat(mat, eyeDetection.getRotate());
            //get distance between eyes
            double eyeDistance = detectController.getEyeDistance(mat);

            if (eyeDistance != Constants.ERROR_EYES_DETECTION) {
                measureData.addDistance((float) eyeDistance);
                while (measureData.size() > Constants.COUNT_FRAMES_THREASHOLD)
                    measureData.removeFirst();
                currentAvgDistance = measureData.getAverageDistanceEyes();
                if (currentAvgDistance != -1)
                    iMeasurment.getDistanceToDevice(currentAvgDistance);
            }
            else
                Utils.log("eyeDistance = null");
        }
    }*/

    class DetectController {
        private Point eyeLeft, eyeRight;
        private float eyesDistance;
        private Mat templateRightEye, templateLeftEye;
        private CascadeClassifier mNativeDetector;
        private CascadeClassifier mClassifierDetectorEyeLeft;
        private CascadeClassifier mClassifierDetectorEyeRight;
        private BaseLoaderCallback mLoaderCallback;

        public DetectController() {
            mLoaderCallback = new BaseLoaderCallback(context) {
                @Override
                public void onManagerConnected(int status) {
                    switch (status) {
                        case LoaderCallbackInterface.SUCCESS:
                        {
                            Utils.log("OpenCV loaded successfully");
                            // Load native library after(!) OpenCV initialization
                            System.loadLibrary("detection_based_tracker");
                            // load cascade file from application resources
                            mNativeDetector = initCascadeClassifier("lbpcascade_frontaleye.xml", R.raw.lbpcascade_frontaleye);
                            mClassifierDetectorEyeLeft = initCascadeClassifier("eye_left.xml", R.raw.eye_left);
                            mClassifierDetectorEyeRight = initCascadeClassifier("eye_right.xml", R.raw.eye_right);
                        } break;
                        default:
                        {
                            super.onManagerConnected(status);
                        } break;
                    }
                }
            };
            //OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, context, mLoaderCallback);
            OpenCVLoader.initDebug();
            initLoadOpenCV();
        }

        public CascadeClassifier initCascadeClassifier(String filename, int idRes) {
            try {
                // load cascade file from application resources
                InputStream is = context.getResources().openRawResource(idRes);
                File cascadeDir = context.getDir("cascade", Context.MODE_PRIVATE);
                File mCascadeFace = new File(cascadeDir, filename);
                FileOutputStream os = new FileOutputStream(mCascadeFace);
                byte[] buffer = new byte[4096];
                int bytesRead;
                while ((bytesRead = is.read(buffer)) != -1) {
                    os.write(buffer, 0, bytesRead);
                }
                is.close();
                os.close();
                CascadeClassifier mDetector = new CascadeClassifier(mCascadeFace.getAbsolutePath());
                //  mNativeDetector = new DetectionBasedTracker(mCascadeFace.getAbsolutePath(), 0);
                if (mDetector.empty()) {
                    Utils.log("Failed to load " + filename + " cascade classifier");
                    mDetector = null;
                } else
                    Utils.log("Loaded cascade " + filename + " classifier from "
                            + mCascadeFace.getAbsolutePath());
                return  mDetector;
            } catch (IOException e) {
                e.printStackTrace();
                Utils.log("Failed to load cascades. Exception thrown: " + e);
                return null;
            }
        }

        private Rect detectFace(Mat mat) {
            int height = mat.rows();
            float mAbsoluteFaceSize = Math.round(height * 0.2f);
            //  mNativeDetector.setMinFaceSize((int) mAbsoluteFaceSize);
            long t = System.currentTimeMillis();
            MatOfRect faces = new MatOfRect();
            //mNativeDetector.detect(mat, faces);
            mNativeDetector.detectMultiScale(mat, faces, 1.1, 2, 2,
                    new org.opencv.core.Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new org.opencv.core.Size());

            if (faces.toArray().length == 0)
                return null;
            else
                return faces.toArray()[0];
        }

        private boolean detectEyesFast(Mat mat, Rect face) {
            if (face == null)
                return false;
            org.opencv.core.Rect eyearea_right = new org.opencv.core.Rect(face.x + face.width / 16,
                    (int) (face.y + (face.height / 4.5)),
                    (face.width - 2 * face.width / 16) / 2, (int) (face.height / 3.0));
            org.opencv.core.Rect eyearea_left = new org.opencv.core.Rect(face.x + face.width / 16
                    + (face.width - 2 * face.width / 16) / 2,
                    (int) (face.y + (face.height / 4.5)),
                    (face.width - 2 * face.width / 16) / 2, (int) (face.height / 3.0));
            eyeLeft = irisDetect(mat, eyearea_left,false);
            eyeRight = irisDetect(mat, eyearea_right,true);
            if (eyeLeft != null && eyeRight != null){
                Utils.log("in detectEyesFAST WITH FACE");
                return true;
            }
            else
                return false;
        }

        private boolean detectEyesFast(Mat mat) {
            if (eyeLeft == null || eyeRight == null)
                return false;
            float x, y, sideWidth, sideHeight;
            x = getRightCoord(mat, (float) eyeRight.x);
            y = getRightCoord(mat, (float) eyeRight.y);
            sideHeight = getRightSideHeight(mat, (int) eyeRight.y);
            sideWidth = getRightSideWidth(mat, (int) eyeRight.x);
            org.opencv.core.Rect eyearea_right = new org.opencv.core.Rect((int)x,(int) y,(int) sideWidth,(int)sideHeight);

            x = getRightCoord(mat, (float) eyeLeft.x);
            y = getRightCoord(mat, (float) eyeLeft.y);
            sideHeight = getRightSideHeight(mat, (int) eyeLeft.y);
            sideWidth = getRightSideWidth(mat, (int) eyeLeft.x);
            org.opencv.core.Rect eyearea_left = new org.opencv.core.Rect((int)x,(int) y,(int) sideWidth,(int)sideHeight);
           // iFaceData.getEyeCoords(eyearea_left, eyearea_right);
            eyeLeft = irisDetect(mat, eyearea_left,false);
            eyeRight = irisDetect(mat, eyearea_right,true);
            if (eyeLeft != null && eyeRight != null) {
                Utils.log("in detectEyesFAST");
                return true;
            }
            else {
                if (eyeLeft == null)
                    Utils.log("FAST LEFT NULL");
                if (eyeRight == null)
                    Utils.log("FAST RIGHT NULL");
                return false;
            }
        }

        private boolean detectEyesSlow(Mat mat) {
            org.opencv.core.Rect screen = new org.opencv.core.Rect(0, 0, mat.cols(), mat.rows());
            org.opencv.core.Rect eyearea_right = new org.opencv.core.Rect(screen.x, screen.y,
                    (screen.width / 2 - screen.width / 16), (int) (screen.height / 1.5));
            org.opencv.core.Rect eyearea_left = new org.opencv.core.Rect(screen.width / 2 + screen.width / 16, screen.y,
                    (screen.width / 2 - screen.width / 16), (int) (screen.height / 1.5));
            eyeLeft = irisDetect(mat, eyearea_left, false);
            eyeRight = irisDetect(mat, eyearea_right, true);
            if (eyeLeft != null && eyeRight != null) {
                Utils.log("in detectEyesSLOW");
                return true;
            }
            else
                return false;
        }

        private Point irisDetect(Mat mat, Rect eyeArea, boolean isRightEye) {
            Mat mEye = mat.submat(eyeArea);
            MatOfRect eye = new MatOfRect();
            Rect irisarea;
            long t = System.currentTimeMillis();
            //detect eyes
            if (!isRightEye)
                mClassifierDetectorEyeLeft.detectMultiScale(mEye, eye, 1.1, 2, Objdetect.CASCADE_FIND_BIGGEST_OBJECT
                            | Objdetect.CASCADE_SCALE_IMAGE, new org.opencv.core.Size(30, 30),
                    new org.opencv.core.Size());
            else
                mClassifierDetectorEyeRight.detectMultiScale(mEye, eye, 1.1, 2, Objdetect.CASCADE_FIND_BIGGEST_OBJECT
                            | Objdetect.CASCADE_SCALE_IMAGE, new org.opencv.core.Size(30, 30),
                    new org.opencv.core.Size());
            //get irises areas
            if (eye.toArray().length == 0)
                return null;
            else
                irisarea = eye.toArray()[0];

            long eyes_det = (System.currentTimeMillis() - t);
            Utils.log("Eyes detection finished: " + eyes_det);
            Core.MinMaxLocResult mmG;

            irisarea.x = eyeArea.x + irisarea.x;
            irisarea.y = eyeArea.y + irisarea.y;
            eyeArea = new Rect((int) irisarea.tl().x, (int) (irisarea.tl().y + irisarea.height * 0.4), (int) irisarea.width,
                    (int) (irisarea.height * 0.6));

            //mEye = mat.submat(eyeArea);
            mEye = mat.submat(irisarea);
            if (isRightEye)
                templateRightEye = mEye;
            else
                templateLeftEye = mEye;
            /*Mat eyeForSave = mat.submat(irisarea);
            if (isRightEye)
                Utils.saveImageFromMat("eye_right.png", eyeForSave);
            else
                Utils.saveImageFromMat("eye_left.png", eyeForSave);
            mmG = Core.minMaxLoc(mEye);*/
            Point iris = new Point();
            /*iris.x = mmG.minLoc.x + eyeArea.x; //detect iris
            iris.y = mmG.minLoc.y + eyeArea.y;*/
            iris.x = irisarea.x + irisarea.width/2;
            iris.y = irisarea.y + irisarea.height/2;
            return iris;
        }

        public float getDistanceEyes(Mat mat) {
            boolean isSuccess;
            long t = System.currentTimeMillis();
            if(eyesDistance != 0 && detectEyesFast(mat))
                isSuccess = true;
            else
                if(detectEyesFast(mat,  detectFace(mat)))
                    isSuccess = true;
                else
                    if (detectEyesSlow(mat))
                        isSuccess = true;
                    else
                        isSuccess = false;
            Utils.log("Summary time is = " + (System.currentTimeMillis() - t));
            if (isSuccess) {
                eyesDistance = (float) Math.sqrt(Math.pow(eyeLeft.x - eyeRight.x, 2) + Math.pow(eyeLeft.y - eyeRight.y, 2));
                //iFaceData.getIrisCoords(eyeLeft, eyeRight);
                return eyesDistance;
            } else {
                eyesDistance = 0;
                return ERROR_EYES_DETECTION;
            }
        }
        private float getRightCoord(Mat mat, float coordEye) {
            float coord;
            if (coordEye - eyesDistance/2 < 0) {
                coord = 0;
            }
            else {
                coord = coordEye - eyesDistance/2;
            }
            return coord;
        }

        private float getRightSideWidth(Mat mat, float coordEye) {
            float sideWidth;
            if ((coordEye + eyesDistance/2) > mat.cols())
                sideWidth = mat.cols() - coordEye;
            else
                sideWidth = eyesDistance;
            return  sideWidth;
        }

        private  float getRightSideHeight(Mat mat, float coordEye) {
            float sideHeight;
            if ((coordEye + eyesDistance/2) > mat.rows())
                sideHeight = mat.rows() - coordEye;
            else
                sideHeight = eyesDistance;
            return  sideHeight;
        }

        private void initLoadOpenCV() {
            Utils.log("OpenCV loaded successfully");

            // Load native library after(!) OpenCV initialization
            System.loadLibrary("detection_based_tracker");
            mNativeDetector = initCascadeClassifier("lbpcascade_frontaleye.xml", R.raw.lbpcascade_frontaleye);
            mClassifierDetectorEyeLeft = initCascadeClassifier("eye_left.xml", R.raw.eye_left);
            mClassifierDetectorEyeRight = initCascadeClassifier("eye_right.xml", R.raw.eye_right);
        }
    }
}
