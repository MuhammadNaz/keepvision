package com.keepvisionapp.child.eye_detection;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 27.03.2015.
 */
public class MeasureControllerGame extends MeasureController implements HideCameraTracking.ICallbackFromCameraState{

    @Override
    public void begun() {

    }

    @Override
    public void stopped() {

    }

    public interface IResultMeasurementForGame {
        public void getResultMeasurementForGame(ResultMeasurementGame result, float distance);
    }

    public enum ResultMeasurementGame {
        RUNNING, TO_PAUSE, INIT, ERROR, READY;
    }

    private IResultMeasurementForGame iResultMeasurementForGame;
    public static final int DISTANCES_BUFFER_SIZE = 5;
    private List<Float> bufferDistances = new ArrayList<Float>(DISTANCES_BUFFER_SIZE);
    private ResultMeasurementGame resultMeasurementGame;
    private float startDistance;

    public MeasureControllerGame(Context context, IResultMeasurementForGame iResultMeasurementForGame) {
        super(context);
        this.iResultMeasurementForGame = iResultMeasurementForGame;
    }

    public boolean isMeasuring() {
        return measurementDistanceToEyes.isMeasuring;
    }

    public void startMeasure() {
        measurementDistanceToEyes.setICallbackFromCamera(this);
        resultMeasurementGame = ResultMeasurementGame.INIT;
        measurementDistanceToEyes.startMeasure();
    }

    public void stopMeasure() {
        measurementDistanceToEyes.stopMeasure();
    }

    public void runningGame() {
        resultMeasurementGame = ResultMeasurementGame.RUNNING;
    }
    @Override
    public void getDistanceToDevice(float distance) {

        if (resultMeasurementGame == ResultMeasurementGame.INIT) {
            if (distance == MeasurementDistanceToEyes.ERROR_EYES_DETECTION)
                return;
            for (int i = 0; i < bufferDistances.size(); i++)
                if (Math.abs(bufferDistances.get(i) - distance) > 3) {
                    bufferDistances.clear();
                    break;
                }
            bufferDistances.add(distance);
            if (bufferDistances.size() == DISTANCES_BUFFER_SIZE) {
                resultMeasurementGame = ResultMeasurementGame.READY;
                startDistance = getAvgFromBuffer(bufferDistances);
                iResultMeasurementForGame.getResultMeasurementForGame(resultMeasurementGame, startDistance);
                bufferDistances.clear();
                return;
            }
            iResultMeasurementForGame.getResultMeasurementForGame(resultMeasurementGame, bufferDistances.size());
            Utils.log("INIT");
            return;
        }
        if (resultMeasurementGame == ResultMeasurementGame.READY) {
            if (Math.abs(startDistance - distance) > 2 || distance == MeasurementDistanceToEyes.ERROR_EYES_DETECTION) {
                bufferDistances.clear();
                resultMeasurementGame = ResultMeasurementGame.INIT;
                iResultMeasurementForGame.getResultMeasurementForGame(resultMeasurementGame, 0);
            }
            Utils.log("READY");
            return;
        }
        if (resultMeasurementGame == ResultMeasurementGame.RUNNING) {
            if (Math.abs(startDistance - distance) > 3 || distance == MeasurementDistanceToEyes.ERROR_EYES_DETECTION) {
                resultMeasurementGame = ResultMeasurementGame.TO_PAUSE;
                iResultMeasurementForGame.getResultMeasurementForGame(resultMeasurementGame, MeasurementDistanceToEyes.ERROR_EYES_DETECTION);
            }
            Utils.log("RUNNING");
            return;
        }
        if (resultMeasurementGame == ResultMeasurementGame.TO_PAUSE) {
            if (distance == MeasurementDistanceToEyes.ERROR_EYES_DETECTION) {
                resultMeasurementGame = ResultMeasurementGame.TO_PAUSE;
                iResultMeasurementForGame.getResultMeasurementForGame(resultMeasurementGame, MeasurementDistanceToEyes.ERROR_EYES_DETECTION);
                Utils.log("TO_PAUSE_ERROR");
                return;
            }
            if (Math.abs(startDistance - distance) > 2) {
                resultMeasurementGame = ResultMeasurementGame.TO_PAUSE;
                iResultMeasurementForGame.getResultMeasurementForGame(resultMeasurementGame, startDistance - distance);
            }
            else {
                resultMeasurementGame = ResultMeasurementGame.RUNNING;
                iResultMeasurementForGame.getResultMeasurementForGame(resultMeasurementGame, startDistance);
            }
            Utils.log("TO_PAUSE");
            return;
        }
    }


    private float getAvgFromBuffer(List<Float> buffer) {
        float sum = 0;
        for (float x : buffer)
            sum += x;
        return sum / buffer.size();
    }
}
