package com.keepvisionapp.child.eye_detection;

import android.content.Context;

/**
 * Created by AbduLlah on 27.03.2015.
 */
public abstract class MeasureController  implements MeasurementDistanceToEyes.IMeasurement{
    protected MeasurementDistanceToEyes measurementDistanceToEyes;
    public MeasureController(Context context) {
        measurementDistanceToEyes = new MeasurementDistanceToEyes(context, this);
    }
}
