package com.keepvisionapp.child.eye_detection;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import org.opencv.core.Point;
import org.opencv.core.Rect;

import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 04.03.2015.
 */
public class EyeTrackingSurfaceView extends SurfaceView implements SurfaceHolder.Callback, MeasurementDistanceToEyes.IFaceData {

    private Context context;
    private Rect face, eye_left, eye_right;
    private Point iris_left, iris_right;
    private PaintFaceDataThread paintFaceDataThread;
    private Paint paintFace;
    private Paint paintEyes;
    private Paint paintIris;
    private int widthLine;
    private float irisRadius;


    public EyeTrackingSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().setFormat(PixelFormat.TRANSPARENT); //make the surfaceView transparent
        getHolder().addCallback(this);
        this.context = context;
        paintEyes = new Paint();
        paintFace = new Paint();
        paintIris = new Paint();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
            widthLine = w / 30;
        irisRadius = (float) widthLine / 2;
        paintFace.setAntiAlias(true);
        paintEyes.setAntiAlias(true);
        paintIris.setAntiAlias(true);
        paintFace.setColor(Color.GREEN);
        paintEyes.setColor(Color.YELLOW);
        paintIris.setColor(Color.RED);
        paintFace.setStrokeWidth(widthLine);
        paintEyes.setStrokeWidth(widthLine);
        paintFace.setStyle(Paint.Style.STROKE);
        paintEyes.setStyle(Paint.Style.STROKE);
    }


    @Override
    public void getFaceCoords(Rect face) {
        this.face = face;
    }

    @Override
    public void getEyeCoords(Rect eye_left, Rect eye_right) {
        this.eye_left = eye_left;
        this.eye_right = eye_right;
    }

    @Override
    public void getIrisCoords(Point iris_left, Point iris_right) {
        this.iris_left = iris_left;
        this.iris_right = iris_right;
    }

    private void drawFaceData(Canvas canvas) {
        if (canvas != null) {
            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            if (face != null)
                canvas.drawRect((float) face.tl().x,(float) face.tl().y,(float) face.br().x,(float) face.br().y, paintFace);
            if (eye_left != null)
                canvas.drawRect((float) eye_left.tl().x,(float) eye_left.tl().y,(float) eye_left.br().x,(float) eye_left.br().y, paintEyes);
            if (eye_right != null)
                canvas.drawRect((float) eye_right.tl().x,(float) eye_right.tl().y,(float) eye_right.br().x,(float) eye_right.br().y, paintEyes);
            if (iris_right != null)
                canvas.drawCircle((float) iris_right.x,(float) iris_right.y, irisRadius, paintFace);
            if (iris_left != null)
                canvas.drawCircle((float) iris_left.x,(float) iris_left.y, irisRadius, paintIris);
        }
    }

    public void stopDinamicGraph() {
        boolean retry = true;
        if (paintFaceDataThread != null)
            paintFaceDataThread.setStopping(true);
        while (retry) //����������� ��������� ������
        {
            try
            {
                paintFaceDataThread.join();
                retry = false;
            } // end try
            catch (InterruptedException e)
            {
            } // end catch
        }
        paintFaceDataThread = null;
    }

    public void startDinamicGraph() {
        if ((paintFaceDataThread == null || paintFaceDataThread.isStopping())) {
            paintFaceDataThread = new PaintFaceDataThread(getHolder());
            paintFaceDataThread.start();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Utils.log("SURFACE CREATED");
        startDinamicGraph();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stopDinamicGraph();
    }

    class PaintFaceDataThread extends Thread {
        private volatile boolean isStopped = false;
        private SurfaceHolder surfaceHolder;
        public PaintFaceDataThread(SurfaceHolder holder) {
            surfaceHolder = holder;
        }

        public boolean isStopping() {
            return isStopped;
        }

        public void setStopping(boolean isStopping) {
            this.isStopped = isStopping;
        }

        @Override
        public void run() {
            Canvas canvas = null;
            Utils.log("thread paint START");
            while(!isStopped) {
                try {
                    canvas = surfaceHolder.lockCanvas(null);
                    synchronized (surfaceHolder) {
                        drawFaceData(canvas);
                    }
                }
                finally {
                    if (canvas != null)
                        surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
            Utils.log("thread paint STOP");
        }

    }
}
