package com.keepvisionapp.child.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.keepvisionapp.child.R;
import com.keepvisionapp.child.User;
import com.keepvisionapp.child.service.ServiceDataTracking;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.HttpHandler;
import com.keepvisionapp.child.tools.Utils;
import com.keepvisionapp.child.views.DialogWait;

/**
 * Created by AbduLlah on 13.04.2015.
 */
public class LogIn extends Activity{

    Button btnLogin;
    TextView tvForgot, tvRegister;
    EditText etMail, etPassowrd;
    HttpHandler httpHandler = new HttpHandler();
    User user = User.getInstance();
    LogInTask logInTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitvity_login);
        if (user.getAPI_KEY() != null) {
            startActivity(new Intent(LogIn.this, ChildrenList.class));
            finish();
        }

        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvForgot = (TextView) findViewById(R.id.tvForgot);
        tvRegister = (TextView) findViewById(R.id.tvRegister);
        etMail = (EditText) findViewById(R.id.etMail);
        etPassowrd = (EditText) findViewById(R.id.etPassword);
    }

    public void onClkLogin(View v) {
        if (!Utils.isAvailableInternet(this)) {
            Toast.makeText(this, "Internet is not available!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (Utils.isValidEmailAddress(etMail.getText().toString()) && etPassowrd.getText().toString().length() >= 6) {
            logInTask = new LogInTask();
            logInTask.execute();
        } else {
            if (!Utils.isValidEmailAddress(etMail.getText().toString()) && etPassowrd.getText().toString().length() < 6) {
                Toast.makeText(this, "Fill up a correct mail and password should consits more 6 characters", Toast.LENGTH_SHORT).show();
            } else {
                if (!Utils.isValidEmailAddress(etMail.getText().toString()))
                    Toast.makeText(this, "Fill up a correct mail", Toast.LENGTH_SHORT).show();
                if (etPassowrd.getText().toString().length() < 6)
                    Toast.makeText(this, "Password should consits more 6 characters", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void showToastsErrorResponse(final int code) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (code == 400)
                    Toast.makeText(LogIn.this, "email not found", Toast.LENGTH_SHORT).show();
                if (code == 403)
                    Toast.makeText(LogIn.this, "incorrect password", Toast.LENGTH_SHORT).show();
                if (code >= 50 && code < 60)
                    Toast.makeText(LogIn.this, "Server is not available", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!Utils.isAvailableInternet(this))
            Toast.makeText(this, "Internet is not available!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (logInTask != null)
            logInTask.cancel(true);
    }

    public void onClkForgot(View v) {

    }

    public void onClkRegister(View v) {

    }

    private class LogInTask extends AsyncTask<Void, Void, String> {
        AlertDialog dialogWait = new DialogWait(LogIn.this).getDialog();
        List<NameValuePair> params;
        int responseCode;
        private final String GET_API = "/api/v1/actions/get_api_key";
        @Override
        protected void onPreExecute() {
            Utils.log(ServiceDataTracking.TAG, "onPreExecute");
            params = new ArrayList<NameValuePair>();
            params.add(new NameValuePair() {
                @Override
                public String getName() {
                    return "email";
                }

                @Override
                public String getValue() {
                    return etMail.getText().toString();
                }
            });
            params.add(new NameValuePair() {
                @Override
                public String getName() {
                    return "password";
                }

                @Override
                public String getValue() {
                    return etPassowrd.getText().toString();
                }
            });
            if (!dialogWait.isShowing())
                dialogWait.show();
        }

        @Override
        protected String doInBackground(Void... param) {
            Utils.log(ServiceDataTracking.TAG, "doInBackground");
            try {
                return httpHandler.makeServiceCall(Constants.SERVER + GET_API, HttpHandler.GET, params, null);
            } catch (HttpHandler.ResponseException e) {
                showToastsErrorResponse(e.codeException);
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onCancelled() {
            Utils.log(ServiceDataTracking.TAG, "onCancelled");
            if (dialogWait.isShowing())
                dialogWait.dismiss();
        }

        @Override
        protected void onPostExecute(String response) {
            if (dialogWait.isShowing())
                dialogWait.dismiss();
            Utils.log(ServiceDataTracking.TAG, "onPostExecute");
            Utils.log(ServiceDataTracking.TAG, "response = " + response);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    user.setAPI_KEY(jsonObject.getString("api_key"));
                    user.setUserId(jsonObject.getString("user_id"));
                    startActivity(new Intent(LogIn.this, ChildrenList.class));
                    //startActivity(new Intent(LogIn.this, MainActivity.class));
                    //startActivity(new Intent(LogIn.this, KeepVisionGame.class));
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
