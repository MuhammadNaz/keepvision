package com.keepvisionapp.child.activities;

import com.keepvisionapp.child.R;
import com.keepvisionapp.child.User;
import com.keepvisionapp.child.eye_exam.BluetoothActivity;
import com.keepvisionapp.child.game.activities.KeepVisionGame;
import com.keepvisionapp.child.service.ServiceDataTracking;
import com.keepvisionapp.child.tools.KeepVisionAPI;
import com.keepvisionapp.child.tools.ResourcesController;
import com.keepvisionapp.child.tools.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class MainActivity extends Activity {

    User user = User.getInstance();

    @Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);

        if (user.getCurrentChild() == null) {
            startActivity(new Intent(this, ChildrenList.class));
        } else {
            KeepVisionAPI.init(this);
            KeepVisionAPI keepVisionAPI = KeepVisionAPI.getInstance();
            try {
                if (keepVisionAPI.start())
                    Utils.log(ServiceDataTracking.TAG, "Service is not running yet");
                else {
                    Utils.log(ServiceDataTracking.TAG, "Service is running already");
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException();
            }
        }
        ResourcesController.checkFolderStructures(this);
        //FREngine.getInstance().loadConfig(ResourcesController.getFilePathFromRaw(R.raw.viewdle_video));
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

    public void onClcBtnToGame(View v) {
        if (user.getCurrentChild() == null) {
            Toast.makeText(this, "The first you must choose child from children list", Toast.LENGTH_SHORT).show();
            return;
        }
        startActivity(new Intent(this, KeepVisionGame.class));
    }

    public void onClcBtnToEyeExam(View v) {
        startActivity(new Intent(this, BluetoothActivity.class));
    }

    public void onClkBtnChooseChild(View v) {
        startActivity(new Intent(this, ChildrenList.class));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.menu_log_out) {
            User.removeUser();
            startActivity(new Intent(this, LogIn.class));
            finish();
            return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
