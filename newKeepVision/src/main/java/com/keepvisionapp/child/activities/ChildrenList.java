package com.keepvisionapp.child.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.keepvisionapp.child.R;
import com.keepvisionapp.child.User;
import com.keepvisionapp.child.service.ServiceDataTracking;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.FloatingActionButton;
import com.keepvisionapp.child.tools.HttpHandler;
import com.keepvisionapp.child.tools.KeepVisionAPI;
import com.keepvisionapp.child.tools.Utils;
import com.keepvisionapp.child.views.DialogWait;

/**
 * Created by AbduLlah on 13.04.2015.
 */
public class ChildrenList extends Activity {

    Button btnRepeat;
    ListView lvChildren;
    List<User.Child> children = User.getInstance().getChildren();
    FloatingActionButton btnSave;
    AdapterChildren adapterChildren;
    int positionOfChoosenChild = -1;
    HttpHandler httpHandler = new HttpHandler();
    GetChildrenList getChildrenList;
    User user = User.getInstance();
    User.Child priorCurrentChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setTitle("Settings");
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        setContentView(R.layout.activity_children_list);

        if (user.getCurrentChild() != null)
            for (int i = 0; i < children.size(); i++) {
                if (children.get(i).childId.equals(user.getCurrentChild().childId))
                    positionOfChoosenChild = i;
            }

        priorCurrentChild = user.getCurrentChild();

        lvChildren = (ListView) findViewById(R.id.lvChildren);
        btnRepeat = (Button) findViewById(R.id.btnRepeatGetChildren);
        adapterChildren = new AdapterChildren();
        lvChildren.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                positionOfChoosenChild = position;
                Utils.log(ServiceDataTracking.TAG, "Choosen child = " + children.get(positionOfChoosenChild).name);
                priorCurrentChild = children.get(positionOfChoosenChild);
                adapterChildren.notifyDataSetInvalidated();
            }
        });
        lvChildren.setAdapter(adapterChildren);

        btnSave = new FloatingActionButton.Builder(this)
                .withDrawable(getResources().getDrawable(R.drawable.ok))
                .withButtonColor(getResources().getColor(R.color.oragne))
                .withGravity(Gravity.BOTTOM | Gravity.RIGHT)
                .withMargins(0, 0, 16, 16)
                .create();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (priorCurrentChild == null) {
                    Toast.makeText(ChildrenList.this, "Выберите ребенка", Toast.LENGTH_SHORT).show();
                } else {
                    user.setChild(priorCurrentChild);
                    KeepVisionAPI.init(ChildrenList.this);
                    KeepVisionAPI keepVisionAPI = KeepVisionAPI.getInstance();
                    if (keepVisionAPI.isRunning()) {
                        try {
                            keepVisionAPI.stop();
                            keepVisionAPI.start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            keepVisionAPI.start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    finish();
                }
            }
        });
        getChildrenList = new GetChildrenList();
        getChildrenList.execute();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (getChildrenList != null)
            getChildrenList.cancel(true);
    }

    public void onClckRepeateGetChildren(View v) {
        btnRepeat.setVisibility(View.GONE);
        getChildrenList = new GetChildrenList();
        getChildrenList.execute();
    }

    public void showToastsErrorResponse(final int code) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (code == 400)
                    Toast.makeText(ChildrenList.this, "Incorrect argumens: security_token required", Toast.LENGTH_SHORT).show();
                if (code == 403)
                    Toast.makeText(ChildrenList.this, "Authorization failed: incorrect security_token", Toast.LENGTH_SHORT).show();
                if (code == 404)
                    Toast.makeText(ChildrenList.this, "Child not found", Toast.LENGTH_SHORT).show();
                if (code >= 50 && code < 60)
                    Toast.makeText(ChildrenList.this, "Server is not available", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_log_out) {
            if (KeepVisionAPI.getInstance() != null) {
                try {
                    KeepVisionAPI.getInstance().stop();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            User.removeUser();
            startActivity(new Intent(this, LogIn.class));
            finish();
            return true;
        }
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private class GetChildrenList extends AsyncTask<Void, Void, String> {
        AlertDialog dialogWait = new DialogWait(ChildrenList.this).getDialog();
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        private String GET_CHILDREN_API_TEMPLATE = "/api/v1/users/{user_id}/children";
        private String GET_CHILDREN_API;
        int responseCode;
        @Override
        protected void onPreExecute() {
            Utils.log(ServiceDataTracking.TAG, "doPreExecute");
            GET_CHILDREN_API = GET_CHILDREN_API_TEMPLATE.replace("{user_id}", user.getUserId());
            Utils.log(ServiceDataTracking.TAG, "GET_CHILDREN_API = " + GET_CHILDREN_API);
            if (!dialogWait.isShowing())
                dialogWait.show();
            params.add(new NameValuePair() {
                @Override
                public String getName() {
                    return "security_token";
                }

                @Override
                public String getValue() {
                    Utils.log(ServiceDataTracking.TAG, "SHA - 256 = " + Utils.sha256(user.getUserId() + user.getAPI_KEY()));
                    return Utils.sha256(user.getUserId() + user.getAPI_KEY());
                }
            });
        }

        @Override
        protected void onCancelled() {
            Utils.log(ServiceDataTracking.TAG, "onCancelled");
            if (dialogWait.isShowing())
                dialogWait.dismiss();
        }

        @Override
        protected String doInBackground(Void... param) {
            Utils.log(ServiceDataTracking.TAG, "doInBackground");
            try {
                return httpHandler.makeServiceCall(Constants.SERVER + GET_CHILDREN_API, HttpHandler.GET, params, null);
            } catch (HttpHandler.ResponseException e) {
                showToastsErrorResponse(e.codeException);
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String response) {
            Utils.log(ServiceDataTracking.TAG, "onPostExecute");
            Utils.log(ServiceDataTracking.TAG, "response = " + response);
            if (response != null) {
                try {
                    children.clear();
                    JSONArray array = new JSONArray(response);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        User.Child child = new User.Child();
                        child.childId = jsonObject.getString("_id");
                        child.name = jsonObject.getString("name");
                        children.add(child);
                    }
                    User.save();
                    if (user.getCurrentChild() != null)
                        for (int i = 0; i < children.size(); i++) {
                            if (children.get(i).childId.equals(user.getCurrentChild().childId))
                                positionOfChoosenChild = i;
                        }
                    adapterChildren.notifyDataSetInvalidated();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (children.size() == 0)
                    btnRepeat.setVisibility(View.VISIBLE);
            }
            if (dialogWait.isShowing())
                dialogWait.dismiss();
        }
    }

    private class AdapterChildren extends BaseAdapter {

        @Override
        public int getCount() {
            if (children != null)
                return children.size();
            return 0;
        }

        @Override
        public Object getItem(int position) {
            if (children != null)
                return children.get(position);
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.item_list_chlidren, null);
            ((ImageView) convertView.findViewById(R.id.ibItemChildOk)).setVisibility(View.INVISIBLE);
            if (positionOfChoosenChild == position) {
                ((ImageView) convertView.findViewById(R.id.ibItemChildOk)).setVisibility(View.VISIBLE);
            }
            ((TextView) convertView.findViewById(R.id.tvItemChildName)).setText(children.get(position).name);
            return convertView;
        }
    }
}
