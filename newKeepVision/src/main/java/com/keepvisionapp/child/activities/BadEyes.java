package com.keepvisionapp.child.activities;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import com.keepvisionapp.child.R;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.Utils;

public class BadEyes extends Activity {
    TextView tvPer, tvFunt, tvDOZ;
    float DOZ;
    Map<Float, Float> mapOfDOZ = new TreeMap<Float, Float>();
    public final String TAG = "BadEyes";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bad);
        fillMap();
        tvPer = (TextView) findViewById(R.id.tvPer);
        tvFunt = (TextView) findViewById(R.id.tvFunt);
        tvDOZ = (TextView) findViewById(R.id.tvDOZ);
        DOZ = getIntent().getFloatExtra(Constants.EYE_TRACKING_RESULT_VALUE, 0);
        float per = convertToPersValue(DOZ);
        per = new BigDecimal(per).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();
        DOZ = new BigDecimal(DOZ).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue() / 100;

        tvDOZ.setText(String.valueOf(DOZ));
        tvPer.setText(String.valueOf(per) + "%");
        if (DOZ == 0)
            tvFunt.setText("20/Inf");
        else
            tvFunt.setText("20/"+String.valueOf(new BigDecimal(20d/ DOZ).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
    }

    private float convertToPersValue(float doz) { //dinamic ostrota zrenia
        Iterator<Map.Entry<Float, Float>> iterator = mapOfDOZ.entrySet().iterator();
        float firstKey, secondKey = 0;
        float more = 0, less = 0;
        doz = doz/100;
        Utils.log(TAG, "doz = " + doz);
        while (iterator.hasNext()) {
            firstKey = iterator.next().getKey();
            Utils.log(TAG, "firstKey = " + firstKey);
            if (doz <= firstKey) {
                more = firstKey;
                less = secondKey;
                break;
            }
            if (iterator.hasNext()) {
                secondKey = iterator.next().getKey();
                Utils.log(TAG, "secondKey = " + secondKey);
                if (doz <= secondKey) {
                    more = secondKey;
                    less = firstKey;
                    break;
                }
            } else {
                more = firstKey;
                less = secondKey;
            }
        }
        Utils.log(TAG, "more " + more + " less = " + less);
        float moreValue = 0, lessValue = 0;
        if (mapOfDOZ.containsKey(more))
            moreValue = mapOfDOZ.get(more);
        if (mapOfDOZ.containsKey(less))
            lessValue = mapOfDOZ.get(less);
        float percent;
        if (more - less != 0)
            percent = (doz - less) * 100 / (more - less);
        else {
            percent = 100;
        }

        Utils.log(TAG, "percent " + percent);
        float value = percent / 100 * (moreValue - lessValue) + lessValue;
        return value;
    }
    private void fillMap() {
        mapOfDOZ.put(0.1f, 20f);
        mapOfDOZ.put(0.2f, 48.9f);
        mapOfDOZ.put(0.32f, 67.5f);
        mapOfDOZ.put(0.4f, 76.5f);
        mapOfDOZ.put(0.5f, 83.6f);
        mapOfDOZ.put(0.63f, 89.9f);
        mapOfDOZ.put(0.8f, 95.6f);
        mapOfDOZ.put(1f, 100f);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(new Intent(this, MainActivity.class));
    }
}
