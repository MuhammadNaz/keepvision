package com.keepvisionapp.child.service;

import com.keepvisionapp.child.eye_detection.MeasureControllerTest;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.Utils;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class ServiceEyeTest extends Service implements MeasureControllerTest.IResultMeasurementForTest {

	private PendingIntent pendingIntent;
    private volatile boolean isTracking = false;
    private MeasureControllerTest measureController;

    @Override
	public void onCreate() {
		super.onCreate();
	    measureController = new MeasureControllerTest(this, this);
    }

    public void stopTracking() {
        if (measureController.isCaching)
            measureController.stopMeasure();
        stopSelf();
    }

    @Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//ensure an existing one service
		if (startId != 1) { 
			while (startId != 1) {
				--startId;
				stopSelf(startId);
			}
			stopSelf(1);
		}
		this.isTracking = intent.getBooleanExtra(Constants.USER_CONTROL_EYE_TRACKING, false);
        if (this.isTracking) { //user clicked or os itself
            boolean isStopTracking = intent.getBooleanExtra(Constants.SECOND_EYE_TRACKING, true);
            pendingIntent = intent.getParcelableExtra(Constants.EYE_TRACKING_PARAM_PINTENT);
            if (isStopTracking) {
                startMeasure(false);
                //stopSelf();
            }
            else {
                startMeasure(true);
            }
        } else {
            stopTracking();
        }
		return START_NOT_STICKY;
	}


    private void startMeasure(boolean isFirstTracking) {
        measureController.startMeasure(isFirstTracking);
        Utils.sendMsgToActivity(pendingIntent, this, Constants.ON_ACTIVITY_RESULT_EYE_TRACKING, Constants.DIALOG_WAIT_SHOW, true); //show dialog wait
    }

	public void onDestroy() {
		super.onDestroy();
        Utils.log("Service destroyed");
	}

    @Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

    @Override
    public void getResultMeasurement(MeasureControllerTest.ResultMeasurementTest resultMeasurement, float value) {
        if (!(resultMeasurement == MeasureControllerTest.ResultMeasurementTest.PROCESSING || resultMeasurement == MeasureControllerTest.ResultMeasurementTest.ERROR))
            Utils.sendMsgToActivity(pendingIntent, this, Constants.ON_ACTIVITY_RESULT_EYE_TRACKING, Constants.DIALOG_WAIT_SHOW, false);
        Utils.sendMsgToActivity(pendingIntent, this, Constants.ON_ACTIVITY_RESULT_EYE_TRACKING, Constants.EYE_TRACKING_RESULT_ERROR, value, Constants.EYE_TRACKING_RESULT_MEASUREMENT, resultMeasurement.ordinal());
        if (resultMeasurement == MeasureControllerTest.ResultMeasurementTest.GOOD || resultMeasurement == MeasureControllerTest.ResultMeasurementTest.BAD)
            stopSelf();
        /*Intent iResult = new Intent();
        iResult.putExtra(Constants.EYE_TRACKING_RESULT_VALUE, value);
        iResult.putExtra(Constants.EYE_TRACKING_RESULT_MEASUREMENT, resultMeasurement.ordinal());
        try {
            pendingIntent.send(this, Constants.ON_ACTIVITY_RESULT_EYE_TRACKING, iResult);
        } catch (PendingIntent.CanceledException e1) {
            Utils.log("Error for sending distance to activity" + e1.getMessage());
        }*/
    }


}