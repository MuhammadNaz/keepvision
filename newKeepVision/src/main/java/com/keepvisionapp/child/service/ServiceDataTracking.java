package com.keepvisionapp.child.service;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.*;
import android.os.Process;

import com.keepvisionapp.child.R;
import com.keepvisionapp.child.User;
import com.keepvisionapp.child.tools.Utils;
import com.keepvisionapp.child.tracking.TrackController;

/**
 * Created by AbduLlah on 11.04.2015.
 */
public class ServiceDataTracking extends Service  {

    public static final String TAG = "ServiceTimeTracking";

    private ServiceHandler serviceHandler;
    private final int MSG_START_TRACKING = 1;
    public static final String KEY_STOP_SERVICE = "StopService";
    private int myPID;
    private User user = User.getInstance();
    private TrackController trackController;

    private class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_START_TRACKING) {
                trackController = new TrackController(ServiceDataTracking.this);
                try {
                    trackController.start();
                } catch (TrackController.TrackControllerException e) {
                    e.printStackTrace();
                    //throw new RuntimeException();
                }
                Utils.log(TAG, "START DATA TRACKING");
            }
        }
    }

    @Override
    public void onCreate() {
        Utils.log(TAG, "onCreate");
        super.onCreate();

        myPID = Process.myPid();
        HandlerThread handlerThread = new HandlerThread("ServiceTimeTrackingThread", Process.THREAD_PRIORITY_BACKGROUND);
        handlerThread.start();
        serviceHandler = new ServiceHandler(handlerThread.getLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Utils.log(TAG, "onStartCommand");
        //ensure an existing one service
        if (startId != 1) {
            while (startId != 1) {
                --startId;
                stopSelf(startId);
            }
            stopSelf(1);
        }
        /*if (intent != null) {
            if (intent.hasExtra(ServiceDataTracking.KEY_STOP_SERVICE)) {
                Utils.log(TAG, "KEY_STOP_SERVICE -> stopSelf");
                stopSelf();
                return START_NOT_STICKY;
                //Process.killProcess(myPID);
            }
        }*/
        if (user == null || user.getCurrentChild() == null) {
            Utils.log(TAG, "user = null || currentChild = null");
            stopSelf();
            return START_NOT_STICKY;
        } else {
            Message msg = serviceHandler.obtainMessage();
            msg.what = MSG_START_TRACKING;
            serviceHandler.sendMessage(msg);
            startForegroundWithNotification();
            return START_STICKY;
        }
    }

    private void startForegroundWithNotification() {
        Notification notif = new Notification(R.drawable.ic_launcher, "", System.currentTimeMillis());
        notif.flags|=Notification.FLAG_NO_CLEAR;
        /*Intent toMainActivity = new Intent(this, KVActivity.class);
        PendingIntent pi=PendingIntent.getActivity(this, 0,
                toMainActivity, 0);*/
        notif.setLatestEventInfo(this, "KeepVision", "", null);
        startForeground(TrackController.NOTIF_ID, notif);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.log(TAG, "onDestroy");
        if (trackController != null)
            trackController.onDestroy();
    }
}
