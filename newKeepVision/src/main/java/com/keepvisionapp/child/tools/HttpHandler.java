package com.keepvisionapp.child.tools;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.keepvisionapp.child.service.ServiceDataTracking;

public class HttpHandler {
 
    static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;
 
    public HttpHandler() {
 
    }
    public String makeServiceCall(String url, int method) throws ResponseException, IOException {
        return this.makeServiceCall(url, method, null, null);
    }

    public String makeServiceCall(String url, int method, List<NameValuePair> params) throws ResponseException, IOException {
        return this.makeServiceCall(url, method, params, null);
    }

    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     * */
    public String makeServiceCall(String url, int method,
                                  List<NameValuePair> params, String body) throws ResponseException, IOException {

            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;

            // Checking http request method type
            if (method == POST) {

                // adding post params
                if (params != null) {
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += "?" + paramString;
                    //httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                }
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("content-type", "application/json");
                if (body != null) {
                    httpPost.setEntity(new StringEntity(body));
                    Utils.log(ServiceDataTracking.TAG, "body = " + body);
                }
                Utils.log(ServiceDataTracking.TAG, "url = " + httpPost.getRequestLine());
                httpResponse = httpClient.execute(httpPost);

            } else if (method == GET) {
                // appending params to url
                if (params != null) {
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += "?" + paramString;
                }
                HttpGet httpGet = new HttpGet(url);
                httpResponse = httpClient.execute(httpGet);
            }
            Utils.log(ServiceDataTracking.TAG, "Code = " + httpResponse.getStatusLine().getStatusCode());
            if (httpResponse.getStatusLine().getStatusCode() < 200 || httpResponse.getStatusLine().getStatusCode() >= 300) {
                ResponseException exception = new ResponseException();
                exception.codeException = httpResponse.getStatusLine().getStatusCode();
                throw exception;
            }
            httpEntity = httpResponse.getEntity();
            response = EntityUtils.toString(httpEntity);


        return response;

    }

    public class ResponseException extends Exception {
        public int codeException;
    }
}
