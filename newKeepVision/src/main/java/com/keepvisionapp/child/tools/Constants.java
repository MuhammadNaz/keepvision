package com.keepvisionapp.child.tools;

public class Constants {
	public static final int COUNT_FRAMES_THREASHOLD = 5;
	public static final String SECOND_EYE_TRACKING = "IS_STOP_EYE_TRACKING"; //stop eye tracking
	public static final String EYE_TRACKING_PARAM_PINTENT = "PINTENT_FOR_EYE_TRACKING"; //
	public static final String EYE_TRACKING_RESULT_VALUE = "RESULT_OF_EYE_TRACKING";
	public static final String EYE_TRACKING_RESULT_ERROR = "RESULT_OF_EYE_TRACKING";

    public static final String EYE_TRACKING_RESULT_MEASUREMENT = "RESULT_OF_MEASUREMENT";
    public static final String DIALOG_WAIT_SHOW = "DIALOG_WAIT_SHOW";

    public static final String USER_CONTROL_EYE_TRACKING = "USER_CONTROL_EYE_TRACKING"; // user clicked stop or start eye tracking
	public static final int ON_ACTIVITY_RESULT_EYE_TRACKING = 1;
	public static final String ACTION_ORIENTATION_CHANGE = "android.intent.action.CONFIGURATION_CHANGED";
    public static final float NANO_RATIO = 1000000000.0f;
	public enum StateEyeTracking {
        SECOND_TRACKING, START_TRACKING;
	}

    //public static final String SERVER = "http://109.234.39.140:8003"; //1337
    public static final String SERVER = "http://keepvisionapp.com:8003";
    public static final String SERVER_POST = "http://cloudprocess.ru:8003";
    public static final int HANDLE_MSG_ADD_SV = 0;
    public static final int HANDLE_MSG_REMOVE_SV = 1;
    public static final int HANDLE_MSG_SEND_DATA = 2;
}
