package com.keepvisionapp.child.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import com.keepvisionapp.child.R;
public class ResourcesController {
    public static final String FACE_FOLDER = "/sdcard/keepvision/KeepVision";
    public static String pathApp = ""; //init in Application class
    static final String FLD_ERR_MSG = "errMsg";
    public static final String FLD_RESULT = "result";
    private static final Map<Integer, String> UNPACKING_RESOURCES = new HashMap<Integer, String>();

    static {
        UNPACKING_RESOURCES.put(R.raw.viewdle_default, FACE_FOLDER + "/viewdle_default.cfg");
        UNPACKING_RESOURCES.put(R.raw.vdlphotodb, FACE_FOLDER + "/vdlphotodb.sql3");
        UNPACKING_RESOURCES.put(R.raw.viewdle_photo, FACE_FOLDER + "/viewdle_photo.cfg");
        UNPACKING_RESOURCES.put(R.raw.viewdle_video, FACE_FOLDER + "/viewdle_video.cfg");
        UNPACKING_RESOURCES.put(R.raw.viewdle, FACE_FOLDER + "/viewdle.cfg");
        UNPACKING_RESOURCES.put(R.raw.vdlphotoapp, FACE_FOLDER + "/vdlphotoapp.cfg");
        UNPACKING_RESOURCES.put(R.raw.cs0_eyesnosemouth, FACE_FOLDER + "/features/cs0_eyesnosemouth.bin");
        UNPACKING_RESOURCES.put(R.raw.eyes, FACE_FOLDER + "/features/eyes.bin");
        UNPACKING_RESOURCES.put(R.raw.eyes_weak, FACE_FOLDER + "/features/eyes_weak.bin");
        UNPACKING_RESOURCES.put(R.raw.face, FACE_FOLDER + "/features/face.bin");
        UNPACKING_RESOURCES.put(R.raw.letters_ascii, FACE_FOLDER + "/features/letters_ascii.pgm");
        UNPACKING_RESOURCES.put(R.raw.bl_video, FACE_FOLDER + "/features/facelight/bl.bin");
        UNPACKING_RESOURCES.put(R.raw.dfl_video, FACE_FOLDER + "/features/facelight/dfl.bin");
        UNPACKING_RESOURCES.put(R.raw.msl_video, FACE_FOLDER + "/features/facelight/msl.bin");
        UNPACKING_RESOURCES.put(R.raw.sfl_video, FACE_FOLDER + "/features/facelight/sfl.bin");
        UNPACKING_RESOURCES.put(R.raw.tl_video, FACE_FOLDER + "/features/facelight/tl.bin");
        UNPACKING_RESOURCES.put(R.raw.cs0_eyes, FACE_FOLDER + "/cs0_vampire_features/cs0_eyes.bin");
        UNPACKING_RESOURCES.put(R.raw.cs0_eyeswithnose, FACE_FOLDER + "/cs0_vampire_features/cs0_eyeswithnose.bin");
        UNPACKING_RESOURCES.put(R.raw.cs0_mouth, FACE_FOLDER + "/cs0_vampire_features/cs0_mouth.bin");
        UNPACKING_RESOURCES.put(R.raw.cs0_nose, FACE_FOLDER + "/cs0_vampire_features/cs0_nose.bin");
        //UNPACKING_RESOURCES.put(R.raw.exo, FACE_FOLDER + "/exo.otf");
    }

    private static void prepareResourceFiles(final Resources resources, final boolean b) throws IOException {
        prepareResourceFolders(FACE_FOLDER + "/dummy");
        for (final int intValue : UNPACKING_RESOURCES.keySet()) {
            final String s = UNPACKING_RESOURCES.get(intValue);
            if (b || !new File(s).exists()) {
                prepareResourceFolders(s);
                final InputStream openRawResource = resources.openRawResource(intValue);
                final FileOutputStream fileOutputStream = new FileOutputStream(s);
                final byte[] array = new byte[openRawResource.available()];
                openRawResource.read(array);
                fileOutputStream.write(array);
                openRawResource.close();
                fileOutputStream.close();
            }
        }
    }

    private static void prepareResourceFolders(final String s) throws IOException {
        final File file = new File(FACE_FOLDER + "/automatic_named_user");
        if (file.exists()) {
            file.delete();
        }
        final File file2 = new File(s);
        if (!file2.exists() || !file2.isDirectory()) {
            final String[] split = s.split("/");
            String string = "";
            for (int i = 1; i < split.length - 1; ++i) {
                string = String.valueOf(string) + "/" + split[i];
                final File file3 = new File(string);
                if (!file3.isDirectory() && !file3.mkdir()) {
                    throw new IOException("Cannot create folder " + string);
                }
            }
        }
    }

    public static Bundle prepareResources(final Resources resources, final boolean b) {
        final Bundle bundle = new Bundle();
        while (true) {
            Utils.log("prepare Resources");
            try {
               // new File(FACE_FOLDER + "/images/.nomedia").createNewFile();
                try {
                    prepareResourceFiles(resources, b);
                    bundle.putBoolean(FLD_RESULT, true);
                    return bundle;
                }
                catch (IOException ex) {
                    bundle.putBoolean(FLD_RESULT, false);
                    bundle.putString(FLD_ERR_MSG, ex.getMessage());
                    return bundle;
                }
            }
            catch (Exception ex2) {
                Utils.log(ex2.getMessage());
                bundle.putBoolean(FLD_RESULT, false);
                bundle.putString(FLD_ERR_MSG, ex2.getMessage());
                return bundle;
            }
        }
    }

    public static void checkFolderStructures(final Activity activity) {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            final AlertDialog create = new AlertDialog.Builder(activity).create();
            create.setMessage((CharSequence) activity.getString(R.string.no_sd_card_message));
            create.setCancelable(false);
            create.setButton(DialogInterface.BUTTON_NEUTRAL, activity.getString(R.string.close_button_text),new DialogInterface.OnClickListener() {
                public void onClick(final DialogInterface dialogInterface, final int n) {
                    activity.finish();
                }
            });
            create.show();
            return;
        }

       prepareResources(activity.getApplicationContext().getResources(), false);
    }

    public static String getFilePathFromRaw(int rawId) {
        return UNPACKING_RESOURCES.get(rawId);
    }
}
