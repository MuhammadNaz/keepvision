package com.keepvisionapp.child.tools;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.keepvisionapp.child.service.ServiceDataTracking;

/**
 * Created by AbduLlah on 13.04.2015.
 */
public class KeepVisionAPI {
    private static KeepVisionAPI keepVisionAPI;
    private Context context;
    private Intent intent;
    private boolean isInit;

    public static String TAG = "KeepVisionAPI";

    public static final String ACTION_CHECK_RUNNING = "com.keepvisionapp.child.tools.KeepVisionAPI";
    public static void init(Activity activity) {
        keepVisionAPI = new KeepVisionAPI(activity);
        keepVisionAPI.isInit = true;
    }

    public static KeepVisionAPI getInstance() {
        return keepVisionAPI;
    }

    private KeepVisionAPI(Context activity) {
        this.context = activity;
        intent = new Intent(activity, ServiceDataTracking.class);

    }
    public boolean start() throws Exception {
        if (!keepVisionAPI.isInit)
            throw new Exception("You must init API");
        if (!keepVisionAPI.isServiceRunning(keepVisionAPI.context, ServiceDataTracking.class)) {
            keepVisionAPI.context.startService(intent);

            setAlarmCheckRunning(context);

            return true;
        } else
            return false;
    }

    private static void setAlarmCheckRunning(Context context) {
        Intent startIntent = new Intent(ACTION_CHECK_RUNNING);
        PendingIntent startPIntent = PendingIntent.getBroadcast(context, 0, startIntent, 0);
        AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.setRepeating(AlarmManager.RTC, 1 * 60 * 1000, 1 * 60 * 1000, startPIntent);
    }

    public boolean stop() throws Exception {
        if (!keepVisionAPI.isInit)
            throw new Exception("You must init API");
        if (keepVisionAPI.isServiceRunning(keepVisionAPI.context, ServiceDataTracking.class)) {
            /*intent.putExtra(ServiceDataTracking.KEY_STOP_SERVICE, true);
            context.startService(intent);*/
            keepVisionAPI.context.stopService(intent);
            return true;
        } else
            return false;
    }

    private boolean isServiceRunning(Context context, Class serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public boolean isRunning() {
        return keepVisionAPI.isServiceRunning(keepVisionAPI.context, ServiceDataTracking.class);
    }

    public static class CheckRunningReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Utils.log(TAG, "CheckRunningReceiver totalMemory = " + Runtime.getRuntime().totalMemory());
        }
    }
}
