package com.keepvisionapp.child;

import android.database.Observable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.keepvisionapp.child.service.ServiceDataTracking;
import com.keepvisionapp.child.tools.ResourcesController;
import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 13.04.2015.
 */
public class User implements Serializable{
    public static final String FILE_NAME = "/User";

    private static User user = loadUserData();
    private String API_KEY;
    private String UserId;
    private String mail;
    private String firstName;
    private String lastName;
    private Child currentChild;
    private List<Child> children;
    private UserObservable userObservable = new UserObservable();

    public interface IUserChanged {
        void currentChildChanged(Child currentChild);
        void userChanged();
    }

    public static User getInstance() {
        return user;
    }

    public static User loadUserData() {
        user = (User) Utils.loadDataSerializable(ResourcesController.pathApp + FILE_NAME);
        if (user == null) {
            user = new User();
            Utils.saveDataSerializable(user, ResourcesController.pathApp + FILE_NAME);
        }
        return user;
    }

    private User() {
        Utils.log(ServiceDataTracking.TAG, "CREATED NEW USER");
        children = new ArrayList<Child>();
    }

    public void addIUserChanged(IUserChanged iUserChanged) {
        userObservable.registerObserver(iUserChanged);
    }

    public void removeIUserChanged(IUserChanged iUserChanged) {
        userObservable.unregisterObserver(iUserChanged);
    }



    public String getUserId() {
        return user.UserId;
    }

    public void setUserId(String userId) {
        user.UserId = userId;
        Utils.saveDataSerializable(user, ResourcesController.pathApp + FILE_NAME);
    }

    public String getMail() {
        return user.mail;
    }

    public void setMail(String mail) {
        user.mail = mail;
        Utils.saveDataSerializable(user, ResourcesController.pathApp + FILE_NAME);
    }

    public String getFirstName() {
        return user.firstName;
    }

    public void setFirstName(String firstName) {
        user.firstName = firstName;
        Utils.saveDataSerializable(user, ResourcesController.pathApp + FILE_NAME);
    }

    public String getLastName() {
        return user.lastName;
    }

    public static void removeUser() {
        user = new User();
        Utils.saveDataSerializable(user, ResourcesController.pathApp + FILE_NAME);
    }

    public void setLastName(String lastName) {
        user.lastName = lastName;
        Utils.saveDataSerializable(user, ResourcesController.pathApp + FILE_NAME);
    }

    public void setChild(Child child) {
        user.currentChild = child;
        userObservable.notifyCurrentChildChanged(child);
        Utils.saveDataSerializable(user, ResourcesController.pathApp + FILE_NAME);
    }

    public void addChild(Child child) {
        user.children.add(child);
        Utils.saveDataSerializable(user, ResourcesController.pathApp + FILE_NAME);
    }

    public static void save() {
        Utils.saveDataSerializable(user, ResourcesController.pathApp + FILE_NAME);
    }

    public ArrayList<Child> getChildren() {
        return (ArrayList<Child>) children;
    }

    public Child getCurrentChild() {
        return user.currentChild;
    }

    public String getAPI_KEY() {
        return user.API_KEY;
    }

    public void setAPI_KEY(String API_KEY) {
        user.API_KEY = API_KEY;
        Utils.saveDataSerializable(user, ResourcesController.pathApp + FILE_NAME);
        userObservable.notifyUserChanged();
    }

    public class UserObservable extends Observable<IUserChanged> implements Serializable{
        public void notifyCurrentChildChanged(Child currentChild) {
            for (IUserChanged iUserChanged : mObservers) {
                iUserChanged.currentChildChanged(currentChild);
            }
        }
        public void notifyUserChanged() {
            for (IUserChanged iUserChanged : mObservers) {
                iUserChanged.userChanged();
            }
        }
    }

    public static class Child implements Serializable{
        public String childId;
        public String name;
        public int gender;
        public int age;
    }
}
