package com.keepvisionapp.child;

import android.app.Application;
import android.bluetooth.BluetoothSocket;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import com.keepvisionapp.child.eye_exam.ConnectionThread;
import com.keepvisionapp.child.tools.ResourcesController;
import com.keepvisionapp.child.tools.Utils;

public class KVApp extends Application {
    public static String TAG = "KVApp";


    private BluetoothSocket bluetoothSocket;
    private ConnectionThread connectionThread;

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    @Override
    public void onCreate() {
        super.onCreate();
        ResourcesController.pathApp = getApplicationContext().getFilesDir().getPath();
        initGoogleAnalitics();
    }

    private void initGoogleAnalitics() {
        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker("UA-61654820-2");
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);

    }

    public void setBluetoothSocketAndStartConnection(BluetoothSocket bluetoothSocket) {
        Utils.log(TAG, "setBluetoothSocketAndStartConnection");
        this.bluetoothSocket = bluetoothSocket;
        connectionThread = new ConnectionThread(bluetoothSocket);
        connectionThread.start();
    }

    public BluetoothSocket getBluetoothSocket() {
        return bluetoothSocket;
    }

    public ConnectionThread getConnectionThread() {
        return connectionThread;
    }
}
