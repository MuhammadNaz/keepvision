package com.keepvisionapp.child.tracking;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by AbduLlah on 11.04.2015.
 */
public class DataKeepVision implements Serializable {

    public static abstract class Data implements Serializable {}

    public static class DataContinuesTracking extends Data {

    }

    public static class DataTracking extends Data {
        public int lux;
        public long timeRelax;
        public String startTimeString;
        public long startTime;
        public long timeContinueUsing;
        private TimeZone tz = TimeZone.getDefault();
        private DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

        public void setStartTimeString(long startTime) {
            this.startTime = startTime;
            df.setTimeZone(tz);
            this.startTimeString = df.format(new Date(startTime));
        }
    }

    public static class DataDiagnostic extends Data {
        public String startTime;
        public int lux;
        public float DOZ;
        public float distanceToEyes;
        public int score;
        public int brightOfWindow;
        private TimeZone tz = TimeZone.getDefault();
        private DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        public List<DataOptotypeDiagnostic> optotypeDiagnosticList = new ArrayList<DataOptotypeDiagnostic>();
        public void setStartTime(long startTime) {
            df.setTimeZone(tz);
            this.startTime = df.format(new Date(startTime));
        }

    }

    public static class DataOptotypeDiagnostic extends Data {
        public int index;
        public String color;
        public float velocity;
        public int state;
        public int from;
        public float size;
        public long lifeTime = 0; // in milliseconds
        public int countError;
        public float deltaClose;
        public float respectiveDOZ;
        public String colorBack;

        public void setColor(int color) {
            this.color = String.format("#%06X", (0xFFFFFF & color));
        }
        public void setColorBack(int color) {
            colorBack = String.format("#%06X", (0xFFFFFF & color));
        }
    }
}
