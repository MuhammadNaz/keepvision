package com.keepvisionapp.child.tracking;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;

import com.keepvisionapp.child.tools.ResourcesController;
import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 18.04.2015.
 */
public class TrackController implements LockController.ILockState, LightController.IMeasurementLight, SittingController.ISittingController {

    public static String TAG = "TrackController";

    public static int NOTIF_ID = 423;
    private MeasurementIntervalTime measurementTime;
    private ThreadMinute threadMinute;
    private long commonTimeInMin;
    private LockController lockController;
    private LightController lightController;
    private Cacheer cacheer = Cacheer.getInstance();
    private volatile boolean isRunning = false;
    private PostDataTracking postDataTracking;
    private Context context;
    private DataKeepVision.DataTracking dataTracking;
    private boolean isLock;
    private SittingController sittingController = new SittingController(this);

    public TrackController(Context context) {
        threadMinute = new ThreadMinute();
        measurementTime = new MeasurementIntervalTime();
        lockController = new LockController(context, this);
        lightController = new LightController(context, this);
        this.context = context;
    }

    public boolean isRunning() {return isRunning;}


    public void start() throws TrackControllerException {
        Utils.log(TAG, "start track");
        if (!isRunning) {
            try {
                lightController.start();
            } catch (LightController.LightControllerException e) {
                Utils.log(TAG,"startERROR = " +  LightController.LightControllerException.activated);
                e.printStackTrace();
            }
            threadMinute.start();
            dataTracking = new DataKeepVision.DataTracking();
            dataTracking.setStartTimeString(measurementTime.start());
            try {
                lockController.start();
                isLock = lockController.isLock();
            } catch (LockController.LockControllerException e) {
                Utils.log(TAG, "startERROR = " + LockController.LockControllerException.activated);
                e.printStackTrace();
            }
            isRunning = true;
            cacheer.checkCacheAndSend(context);
        } else
            throw new TrackControllerException(TrackControllerException.activated);
    }

    public void stop() throws TrackControllerException {
        Utils.log(TAG, "stop track");
        if (isRunning) {
            try {
                lockController.stop();
            }  catch (LockController.LockControllerException e) {
                Utils.log(TAG, "stopERROR = " + LockController.LockControllerException.notActivated);
                e.printStackTrace();
            }
            try {
                lightController.stop();
            } catch (LightController.LightControllerException e) {
                Utils.log(TAG, "stopERROR = " + LightController.LightControllerException.notActivated);
                e.printStackTrace();
            }
            try {
                measurementTime.stop();
            } catch (MeasurementIntervalTime.MeasureTimeException e) {
                Utils.log(TAG, "stopERROR = " + MeasurementIntervalTime.MeasureTimeException.notActive);
                e.printStackTrace();
            }
            threadMinute.interrupt();
            isRunning = false;
        } else
            throw new TrackControllerException(TrackControllerException.notActivated);
    }

    @Override
    public void lockScreen()  {
        Utils.log(TAG, "lockScreen");
        if (!isLock) {
            try {
                isLock = true;
                dataTracking.lux = (int) lightController.getCurrentLux();
                measurementTime.stop();
                dataTracking.timeContinueUsing = measurementTime.getInterval();
                if (Utils.isAvailableInternet(context)) {
                    postDataTracking = new PostDataTracking(PostDataTracking.CODE_EVENT_TIME_TRACKING, dataTracking, context);
                    postDataTracking.execute(true, false);
                } else {
                    cacheer.addDataCache(PostDataTracking.CODE_EVENT_TIME_TRACKING, dataTracking);
                }
                lightController.stop();
                Utils.log(TAG, "Light stop");
            } catch (LightController.LightControllerException e) {
                Utils.log(TAG, "lockScreenERROR = " + LightController.LightControllerException.notActivated);
                e.printStackTrace();
            } catch (MeasurementIntervalTime.MeasureTimeException e) {
                Utils.log(TAG, "lockScreenERROR = " + MeasurementIntervalTime.MeasureTimeException.notActive);
                e.printStackTrace();
            }
            measurementTime.start();
        }
    }

    @Override
    public void unlockScreen() {
        Utils.log(TAG, "unlockScreen");
        if (isLock) {
            dataTracking = new DataKeepVision.DataTracking();
            try {
                lightController.start();
                dataTracking.lux = (int) lightController.getCurrentLux();
                Utils.log(TAG, "Light start");
            } catch (LightController.LightControllerException e) {
                Utils.log(TAG, "unlockScreenERROR = " + LightController.LightControllerException.activated);
                e.printStackTrace();
            }
            try {
                isLock = false;
                dataTracking.setStartTimeString(measurementTime.getCurrentTime());
                measurementTime.stop();
                dataTracking.timeRelax = measurementTime.getInterval();
                if (Utils.isAvailableInternet(context)) {
                    postDataTracking = new PostDataTracking(PostDataTracking.CODE_EVENT_TIME_TRACKING, dataTracking, context);
                    postDataTracking.execute(false, false);
                } else {
                    Utils.log(TAG, "UNLOCK Failed on sending data");
                }
            } catch (MeasurementIntervalTime.MeasureTimeException e) {
                Utils.log(TAG, "unlockScreenERROR = " + MeasurementIntervalTime.MeasureTimeException.notActive);
                e.printStackTrace();
            }
            measurementTime.start();
        }
    }

    @Override
    public void lightBad(float lux) {
        dataTracking.lux = (int) lux;
    }

    @Override
    public void lightGood(float lux) {
        dataTracking.lux = (int) lux;
    }

    public void onDestroy() {
        try {
            stop();
        } catch (TrackControllerException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
        cacheer.save();
    }


    public void callbackNotifyMinuteOff() {
        ++commonTimeInMin;
        if (!isLock) {
            Utils.log(TAG, "MinuteOff");
            Utils.log(TAG, " lux = " + lightController.getCurrentLux());
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                if (Utils.getProcessInfo(TAG, context).processName != null)
                    Utils.log(TAG, "activity = " + Utils.getProcessInfo(TAG, context).processName);
            } else {
                ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                Utils.log(TAG, "activity = " + cn.toString());
            }

            dataTracking.lux = (int) lightController.getCurrentLux();
            if (dataTracking.startTime != 0)
                dataTracking.timeContinueUsing = measurementTime.getCurrentTime() - dataTracking.startTime;

            if (dataTracking.timeContinueUsing > 3000000) {
                Utils.log(TAG, "BUG BUG BUG BUG BUG BUG BUG BUG BUG BUG BUG BUG BUG BUG BUG BUG");
                Utils.writeString("BUG BUG BUG ", ResourcesController.FACE_FOLDER + "/bag.txt");
            }

            sittingController.checkState(dataTracking);

            if (Utils.isAvailableInternet(context)) {
                postDataTracking = new PostDataTracking(PostDataTracking.CODE_EVENT_TIME_TRACKING, dataTracking, context);
                postDataTracking.execute(false, false);
            } else {
                Utils.log(TAG, "MinuteOff Failed on sending data");
            }
        }
    }

    @Override
    public void good(String text) {

    }

    @Override
    public void bad(String text) {

    }

    @Override
    public void warning(String text) {

    }

    @Override
    public void caution(String text) {

    }

    private void updateNotification(int drawableID, String title, String text) {
        Notification notification = new Notification.Builder(context).setSmallIcon(drawableID)
                .setContentTitle(title).setContentText(text).build();
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIF_ID, notification);
    }

    private class ThreadMinute extends Thread {
        private final long minute = 60 * 1000;

        @Override
        public void run() {
            while (true) {
                try {
                    Thread.sleep(minute);
                    callbackNotifyMinuteOff();
                } catch (InterruptedException e) {
                    Utils.log(TAG, " Interrupted Thread MINUTE NOTIFY!!!");
                    return;
                }
            }
        }
    }

    public static class TrackControllerException extends Exception {
        public static String notActivated = "TrackController You must invoke start before stop";
        public static String activated = "TrackController You got start already";
        public TrackControllerException(String msg) {
            super(msg);
        }
        public TrackControllerException() {}
    }
}
