package com.keepvisionapp.child.tracking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.keepvisionapp.child.User;
import com.keepvisionapp.child.service.ServiceDataTracking;
import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 20.05.2015.
 */
public class AutoStartAfterBoot extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Utils.log(TrackController.TAG, "AutoStart service");
        if (User.getInstance().getCurrentChild() != null) {
            Intent myIntent = new Intent(context, ServiceDataTracking.class);
            context.startService(myIntent);
        }
    }
}
