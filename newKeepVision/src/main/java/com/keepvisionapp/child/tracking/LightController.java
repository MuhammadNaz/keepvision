package com.keepvisionapp.child.tracking;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by AbduLlah on 12.04.2015.
 */
public class LightController implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mPressure;
    private IMeasurementLight iMeasurementLight;
    private boolean isActive;
    private int ERROR_VALUE = -1;
    private float currentLux = ERROR_VALUE;


    public LightController(Context context, IMeasurementLight iMeasurementLight) {
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        mPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        this.iMeasurementLight = iMeasurementLight;
    }

    public void start() throws LightControllerException {
        if (!isActive) {
            isActive = true;
            mSensorManager.registerListener(this, mPressure, SensorManager.SENSOR_DELAY_NORMAL);
        } else
            throw new LightControllerException(LightControllerException.activated);
    }

    public void stop() throws LightControllerException {
        if (isActive) {
            isActive = false;
            mSensorManager.unregisterListener(this);
        } else
            throw new LightControllerException(LightControllerException.notActivated);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if( event.sensor.getType() == Sensor.TYPE_LIGHT)
        {
            if (event.values[0] > 50 && currentLux < 50)
                iMeasurementLight.lightGood(event.values[0]);
            if (event.values[0] < 50 && currentLux > 50)
                iMeasurementLight.lightBad(event.values[0]);
            currentLux = event.values[0];
        }
    }

    public float getCurrentLux() {
        return currentLux;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public interface IMeasurementLight {
        public void lightBad(float lux);
        public void lightGood(float lux);
    }

    public static class LightControllerException extends Exception {
        public static String notActivated = "LightController You must invoke start before stop";
        public static String activated = "LightController You got start already";
        public LightControllerException(String msg) {
            super(msg);
        }
        public LightControllerException() {}
    }
}
