package com.keepvisionapp.child.tracking;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.keepvisionapp.child.User;
import com.keepvisionapp.child.tools.Constants;
import com.keepvisionapp.child.tools.HttpHandler;
import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 18.04.2015.
 */
public class PostDataTracking extends AsyncTask<Boolean, Void, String> {

    public static final String TAG = "PostData";

    /*public static final int CODE_EVENT_CONTINUE = 0;
    public static final int CODE_EVENT_START = 1;
    public static final int CODE_EVENT_STOP = 2;*/
    public static final int CODE_EVENT_TIME_TRACKING = 0;
    public static final int CODE_EVENT_DIAGNOSTIC = 10;

    private List<NameValuePair> params = new ArrayList<NameValuePair>();
    private int responseCode;
    private DataKeepVision.Data data;
    private String jsonStringBody;
    private int eventCode = -1;
    private String POST_EVENT_API_TEMPLATE = "/api/v1/children/{child_id}/events";
    private String POST_EVENT_API;
    private String childId;
    private User user = User.getInstance();
    private HttpHandler httpHandler = new HttpHandler();
    private Cacheer cacheer = Cacheer.getInstance();
    private Context context; //for the Toast msg
    private String finalRequestBody;
    public PostDataTracking(int eventCode, DataKeepVision.Data data, Context context) {
        this.eventCode = eventCode;
        this.data = data;
        this.childId = user.getCurrentChild().childId;
        this.context = context;
    }

    Handler handlerToShowToast = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            Toast.makeText(context, msg.getData().getString("error"), Toast.LENGTH_SHORT).show();
        }
    };

    private void showToast(String error) {
        Message msg = handlerToShowToast.obtainMessage();
        Bundle bundle = new Bundle();
        bundle.putString("error", error);
        msg.setData(bundle);
        handlerToShowToast.sendMessage(msg);
    }

    @Override
    protected void onPreExecute() {
        if (childId == null || data == null || eventCode == -1) {
            cancel(true);
            return;
        }
        Utils.log(TAG, "doPreExecute");
        POST_EVENT_API = POST_EVENT_API_TEMPLATE.replace("{child_id}", childId);
        Utils.log(TAG, "POST_EVENT_API = " + POST_EVENT_API);
        JSONObject jsonMapBody = new JSONObject();
        try {
            jsonMapBody.put("event_code", eventCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject eventOptions = new JSONObject();
        StringBuffer requistBodyBuilder = new StringBuffer();


        try {
            if (eventCode == CODE_EVENT_TIME_TRACKING) {
                Utils.log(TAG, "sending TIME TRACKING");
                eventOptions.put("start", new StringBuffer().append(((DataKeepVision.DataTracking) data).startTimeString));
                eventOptions.put("continue_using",new StringBuffer().append(((DataKeepVision.DataTracking)data).timeContinueUsing));
                eventOptions.put("relax", new StringBuffer().append(((DataKeepVision.DataTracking)data).timeRelax));
                eventOptions.put("lighting_level",new StringBuffer().append(((DataKeepVision.DataTracking)data).lux));
            }
            if (eventCode == CODE_EVENT_DIAGNOSTIC) {
                Utils.log(TAG, "sending TIME DIAGNOSTICS");
                eventOptions.put("date_of_test", new StringBuffer().append(((DataKeepVision.DataDiagnostic) data).startTime));
                eventOptions.put("lighting_level", new StringBuffer().append(((DataKeepVision.DataDiagnostic) data).lux));
                eventOptions.put("acuity", new StringBuffer().append(((DataKeepVision.DataDiagnostic) data).DOZ));
                eventOptions.put("distance", new StringBuffer().append(((DataKeepVision.DataDiagnostic) data).distanceToEyes));
                eventOptions.put("back_bright",new StringBuffer().append(((DataKeepVision.DataDiagnostic) data).brightOfWindow));
                eventOptions.put("score", new StringBuffer().append(((DataKeepVision.DataDiagnostic) data).score));
                JSONArray tests = new JSONArray();
                for (int i = 0; i < ((DataKeepVision.DataDiagnostic) data).optotypeDiagnosticList.size(); i++) {
                    JSONObject obj = new JSONObject();
                    DataKeepVision.DataOptotypeDiagnostic dataOptotypeDiagnostic = ((DataKeepVision.DataDiagnostic) data).optotypeDiagnosticList.get(i);
                    obj.put("serial_number", new StringBuffer().append(dataOptotypeDiagnostic.index));
                    obj.put("ring_size", new StringBuffer().append(dataOptotypeDiagnostic.size));
                    obj.put("ring_color", new StringBuffer().append(dataOptotypeDiagnostic.color));
                    obj.put("ring_velocity", new StringBuffer().append(dataOptotypeDiagnostic.velocity));
                    obj.put("ring_vector", new StringBuffer().append(dataOptotypeDiagnostic.from));
                    obj.put("swipe_time", new StringBuffer().append(dataOptotypeDiagnostic.lifeTime));
                    obj.put("swipe_error", new StringBuffer().append(dataOptotypeDiagnostic.countError));
                    obj.put("distance_error", new StringBuffer().append(dataOptotypeDiagnostic.deltaClose));
                    obj.put("back_color", new StringBuffer().append(dataOptotypeDiagnostic.colorBack));
                    obj.put("acuity", new StringBuffer().append(dataOptotypeDiagnostic.respectiveDOZ));
                    obj.put("state", new StringBuffer().append(dataOptotypeDiagnostic.state));
                    tests.put(obj);
                }
                eventOptions.put("tests", tests);
            }

            jsonMapBody.put("event_options", eventOptions);

            jsonStringBody = jsonMapBody.toString();

            Iterator iterator = jsonMapBody.keys();
            requistBodyBuilder.append(jsonMapBody.getString("event_code"));
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                String value = jsonMapBody.getString(key);
                if (key.equals("event_options")) {
                    JSONObject events = jsonMapBody.getJSONObject("event_options");
                    Iterator iterator2 = events.keys();
                    while (iterator2.hasNext()) {
                        String key2 = (String) iterator2.next();
                        String value2 = events.getString(key2);
                        if (!key2.equals("tests")) {
                            requistBodyBuilder.append(value2);
                        } else {
                            JSONArray arrays = events.getJSONArray("tests");
                            for (int i = 0; i < arrays.length(); i++) {
                                JSONObject obj = arrays.getJSONObject(i);
                                Iterator iterator3 = obj.keys();
                                while (iterator3.hasNext()) {
                                    requistBodyBuilder.append(obj.getString((String) iterator3.next()));
                                }
                            }
                        }
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        finalRequestBody = requistBodyBuilder.toString();
        params.add(new NameValuePair() {
            @Override
            public String getName() {
                return "security_token";
            }

            @Override
            public String getValue() {
                Utils.log(TAG, Utils.sha256(childId + finalRequestBody + user.getAPI_KEY()));
                Utils.log(TAG, "reqParamForHach = " + childId + finalRequestBody + user.getAPI_KEY());
                return Utils.sha256(childId + finalRequestBody + user.getAPI_KEY());
            }
        });
    }

    @Override
    protected String doInBackground(Boolean... booleans) {
        Utils.log(TAG, "doInBackground");
        Utils.log(TAG, "finalREQUESTSTRING_BODY = " + finalRequestBody + "\nbody = " + jsonStringBody);
        String response;
        try {
            response =  httpHandler.makeServiceCall(Constants.SERVER + POST_EVENT_API, HttpHandler.POST, params, jsonStringBody);
            Utils.log(TAG, "SUCCES SEND " + eventCode);
            return response;
        } catch (HttpHandler.ResponseException e) {
            Utils.log(TAG, "ERROR = " + e.codeException);
            if (booleans[1])
                showToast("ERROR = " + e.codeException);
            if (booleans[0]) {
                cacheer.addDataCache(eventCode, data);
            }
        } catch (IOException e) {
            if (booleans[1])
                showToast("Нет доступа к интернету");
            if (booleans[0]) {
                cacheer.addDataCache(eventCode, data);
            }
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onCancelled() {
        Utils.log(TAG, "onCancelled");
        if (data == null)
            Utils.log(TAG, "data = null");
        if (eventCode == -1)
            Utils.log(TAG, "eventCode = -1");
        if (childId == null)
            Utils.log(TAG, "childId = null");
    }

    @Override
    protected void onPostExecute(String response) {
        Utils.log(TAG, "onPostExecute");
        if (response != null) {
            Utils.log(TAG, "response = " + response);
        }
    }
}
