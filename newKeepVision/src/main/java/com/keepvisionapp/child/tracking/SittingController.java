package com.keepvisionapp.child.tracking;


public class SittingController {
    public interface ISittingController {
        void good(String text);
        void bad(String text);
        void warning(String text);
        void caution(String text);
    }

    private ISittingController iSittingController;
    public SittingController(ISittingController iSittingController) {
        this.iSittingController = iSittingController;
    }

    public void checkState(DataKeepVision.DataTracking data) {
        if (data.timeContinueUsing == 0)
            return;
        iSittingController.good("");
    }
}
