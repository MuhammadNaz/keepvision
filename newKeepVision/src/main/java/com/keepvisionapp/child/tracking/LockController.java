package com.keepvisionapp.child.tracking;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.keepvisionapp.child.service.ServiceDataTracking;
import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 18.04.2015.
 */
public class LockController {
    private boolean isLock, isActive;
    private ILockState iLockState;
    private Context context;
    private ScreenStateReceiver screenStateReceiver;

    public static String TAG = "LockController";

    public LockController(Context context, ILockState iLockState) {
        this.context = context;
        this.iLockState = iLockState;
        screenStateReceiver = new ScreenStateReceiver();
    }


    private class ScreenStateReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                Utils.log(TAG, "In Method:  ACTION_SCREEN_OFF");
                iLockState.lockScreen();
                isLock = true;
            } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                Utils.log(TAG, "In Method:  ACTION_SCREEN_ON");

            } else if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
                Utils.log(TAG, "In Method:  ACTION_USER_PRESENT");
                isLock = false;
                iLockState.unlockScreen();
            }
        }
    }

    public boolean isActive() {return isActive;}
    public boolean isLock() {
        return isLock;
    }


    public void start() throws LockControllerException {
        if (!isActive) {
            KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            boolean locked = km.inKeyguardRestrictedInputMode();
            if (locked) {
                Utils.log(ServiceDataTracking.TAG, "LOCKED");
                isLock = true;
            } else {
                Utils.log(ServiceDataTracking.TAG, "UN___LOCKED");
                isLock = false;
            }
            IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            filter.addAction(Intent.ACTION_USER_PRESENT);
            context.registerReceiver(screenStateReceiver, filter);
            isActive = true;
        } else
            throw new LockControllerException(LockControllerException.activated);
    }

    public void stop() throws LockControllerException {
        if (isActive) {
            context.unregisterReceiver(screenStateReceiver);
        } else
            throw new LockControllerException(LockControllerException.notActivated);
    }

    public interface ILockState {
        public void lockScreen();
        public void unlockScreen();
    }

    public static class LockControllerException extends Exception{
        public static String notActivated = "LockController You must invoke start before stop";
        public static String activated = "LockController You got start already";
        public LockControllerException(String msg) {
            super(msg);
        }
        public LockControllerException() {};
    }
}
