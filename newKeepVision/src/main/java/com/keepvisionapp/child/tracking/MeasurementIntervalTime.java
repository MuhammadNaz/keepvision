package com.keepvisionapp.child.tracking;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by AbduLlah on 11.04.2015.
 */
public class MeasurementIntervalTime {
    private long startTime, endTime, interval;
    private volatile boolean isActive;
    private IMeasurementIntervalTime iMeasurementIntervalTime;
    private Calendar calendar = Calendar.getInstance();
    public static String TAG = "MeasurementIntervalTime";

    public long getCurrentTime() {
        //return calendar.getTimeInMillis();
        return new Date().getTime();
    }

    public interface IMeasurementIntervalTime {
        void started(long millis);
        void stopped(long millis, long interval);
    }

    public long start() {
        startTime = getCurrentTime();
        isActive = true;
        if (iMeasurementIntervalTime != null)
            iMeasurementIntervalTime.started(startTime);
        return startTime;
    }

    public long stop() throws MeasureTimeException {
        if (!isActive)
            throw new MeasureTimeException(MeasureTimeException.notActive);
        endTime = getCurrentTime();
        interval = endTime - startTime;
        isActive = false;
        if (iMeasurementIntervalTime != null)
            iMeasurementIntervalTime.stopped(endTime, interval);
        return endTime;
    }

    public void setIMeasurementIntervalTime(IMeasurementIntervalTime iMeasurementIntervalTime) {
        this.iMeasurementIntervalTime = iMeasurementIntervalTime;
    }

    public boolean isActive() {
        return isActive;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public long getInterval() {
        return interval;
    }

    public static class MeasureTimeException extends Exception {
        public static String notActive = "TimeController You must invoke start before stop";
        public static String active = "TimeController You must invoke start before stop";
        public MeasureTimeException(String msg) {
            super(msg);
        }
    }
}
