package com.keepvisionapp.child.tracking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 20.05.2015.
 */
public class ConnectiveChangeReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Utils.log(TrackController.TAG, "ConnectiveChange");
        if (Utils.isAvailableInternet(context))
            Cacheer.getInstance().checkCacheAndSend(context);
    }
}
