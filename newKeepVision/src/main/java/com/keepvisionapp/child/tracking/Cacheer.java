package com.keepvisionapp.child.tracking;

import android.content.Context;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.keepvisionapp.child.User;
import com.keepvisionapp.child.tools.ResourcesController;
import com.keepvisionapp.child.tools.Utils;

/**
 * Created by AbduLlah on 18.04.2015.
 */
public class Cacheer implements User.IUserChanged{

    public static String TAG = "Cacheer";

    private static final Cacheer cacheer = new Cacheer();
    private static final String CATALOG_NAME = "/Cache";
    private User user = User.getInstance();
    private static String PATH_TO_FILE;
    private Map<Integer, ArrayList<DataKeepVision.Data>> cache;
    private String fileName;

    static {
        Utils.log(TAG, "CACHE in static block");
    }

    private Cacheer() {
        Utils.log(TAG, "CREATED NEW CACHER");

        File folder = new File(ResourcesController.pathApp + CATALOG_NAME);
        if (!folder.exists()) {
            boolean success;
            success = folder.mkdir();
            if (success) {
                Utils.log(TAG, "CREATED FOLDER CACHE");
            } else {
                Utils.log(TAG, "ERROR CREATING THE FOLDER");
            }
        }
        if (User.getInstance().getCurrentChild() == null) {
            fileName = "tempChild";
        } else {
            fileName = User.getInstance().getCurrentChild().childId;
        }
        Utils.log(TAG, "fileName = " + fileName);
        PATH_TO_FILE = ResourcesController.pathApp + CATALOG_NAME + "/" + fileName;
        cache = loadCacheData();

        User.getInstance().addIUserChanged(this);
    }

    public void reInit() {
        Utils.log(TAG, "reInit");
        if (User.getInstance().getCurrentChild() == null) {
            cacheer.fileName = "tempChild";
        } else {
            cacheer.fileName = User.getInstance().getCurrentChild().childId;
        }
        Utils.log(TAG, "fileName = " + cacheer.fileName);
        PATH_TO_FILE = ResourcesController.pathApp + CATALOG_NAME + "/" + cacheer.fileName;
        cache = loadCacheData();
    }


    public void addDataCache(int event_code, DataKeepVision.Data object) {
        Utils.log(TAG, "addDataCache, eventCode = " + event_code + " path = " + PATH_TO_FILE);
        if (!cache.containsKey(event_code))
            cache.put(event_code, new ArrayList<DataKeepVision.Data>());
        cache.get(event_code).add(object);
        Utils.saveDataSerializable(cache, PATH_TO_FILE);
    }

    public void save() {
        Utils.saveDataSerializable(cache, PATH_TO_FILE);
    }

    public synchronized void checkCacheAndSend(Context context) {
        Utils.log(TAG, "checkCacheAndSend");


        File fileList = new File(ResourcesController.pathApp + CATALOG_NAME);
        if (fileList != null){
            Utils.log(TAG, "remove wasted files");
            File[] filenames = fileList.listFiles();
            if (filenames == null || filenames.length == 0)
                return;
            for (File tmpf : filenames){
                boolean isRemove = true;
                for (int i = 0; i < user.getChildren().size(); i++) {
                    if (user.getChildren().get(i).childId.equals(tmpf.getName())) {
                        isRemove = false;
                        break;
                    }
                }
                if (isRemove) { //not used anymore
                    Utils.log(TAG, "remove file = " + tmpf.getName());
                    if(!tmpf.delete())
                        Utils.deleteFile(tmpf.getAbsolutePath());
                }
            }
        }

        File cacheFile = new File(PATH_TO_FILE);

        if (cache.size() != 0) {
           /* //fill up ommited time while service was not working

            String childId = cacheFile.getName();
            long lastModifiedTime = cacheFile.lastModified();

            DataTracking.DataStartSession dataRelax = new DataTracking.DataStartSession();
            dataRelax.timeRelax = System.currentTimeMillis() - lastModifiedTime;
            dataRelax.startTimeString = System.currentTimeMillis();
            addDataCache(PostDataTracking.CODE_EVENT_START, dataRelax);*/
            Utils.log(TAG, "cache is not empty");
            String childId = cacheFile.getName();

            ArrayList<Integer> indexesOfEmptyData = new ArrayList<Integer>();
            for (Integer codeEvent : cache.keySet()) {
                ArrayList<DataKeepVision.Data> datas = cache.get(codeEvent);
                if (datas.size() == 0) {
                    indexesOfEmptyData.add(codeEvent);
                    continue;
                }
                if (Utils.isAvailableInternet(context)) {
                    int countError = 0;
                    DataKeepVision.Data data = null;
                    int countPost = datas.size();
                    for (int i = 0; i < countPost; i++) {
                        try {
                            data = datas.remove(0);
                            PostDataTracking postDataTracking = new PostDataTracking(codeEvent, data, context);
                            postDataTracking.execute(true, false);
                            String response = postDataTracking.get();
                            if (response == null) {
                                countError++;
                            } else
                                countError = 0;
                            if (countError > 5)
                                return;
                        } catch (InterruptedException e) {
                            countError++;
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            countError++;
                            e.printStackTrace();
                        }
                    }
                } else Utils.log(TAG, "Internet is not available");
            }
            for (int i = 0; i < indexesOfEmptyData.size(); i++) {
                Utils.log(TAG, "remove codeEvemt = " + indexesOfEmptyData.get(i));
                cache.remove(indexesOfEmptyData.get(i));
            }
        } else {
            Utils.deleteFile(cacheFile.getAbsolutePath());
        }
        save();
    }

    public static Cacheer getInstance() {
        return cacheer;
    }

    private Map<Integer, ArrayList<DataKeepVision.Data>> loadCacheData() {
        cache = (Map<Integer, ArrayList<DataKeepVision.Data>>) Utils.loadDataSerializable(PATH_TO_FILE);
        if (cache == null) {
            cache = new HashMap<Integer, ArrayList<DataKeepVision.Data>>();
            Utils.saveDataSerializable(cache, PATH_TO_FILE);
        }
        return cache;
    }

    @Override
    public void currentChildChanged(User.Child currentChild) {
        reInit();
    }

    @Override
    public void userChanged() {

    }

    public static class CacheerException extends RuntimeException {

        public static final String notFoundCurrentChild = "No chosen current child!";

        public CacheerException(String msg) {
            super(msg);
        }

        public CacheerException() {

        }
    }
}
