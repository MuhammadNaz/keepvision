package com.keepvision.eyeexamfar.eye_exam;
import java.io.Serializable;

/**
 * Created by AbduLlah on 10.05.2015.
 */
public class MsgKV implements Serializable {

    private static final long serialVersionUID = -1344895903;

    public MsgKV(int type) {
        this.type = type;
    }

    public MsgKV() {

    }
    public int type;
    public static final int TYPE_STATE_GAME = 1;
    public static final int TYPE_SWIPE = 2;
    public static final int TYPE_RESULT = 3;
    public static final int TYPE_SHOW_EXPLAIN = 4;

    public String object = "null"; // not null!!!
}
